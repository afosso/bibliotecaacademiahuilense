<?php
    
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/parametro/temasafines.entidad.php';

    class ModeloTemasAfines{
        private $conexion;

        public function ConsultarTemasAfines(){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT * FROM parametro_temasafines");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function GuardarTemasAfines($temasafines){
            try {
                $sql = "INSERT INTO `parametro_temasafines`
                                    (`codigo`,
                                    `descripcion`,
                                    `estado`,
                                    `fechaCreacion`,
                                    `fechaModificacion`,
                                    `idUsuarioCreacion`,
                                    `idUsuarioModificacion`)
                        VALUES (:codigo,
                                :descripcion,
                                :estado,
                                (SELECT NOW()),
                                (SELECT NOW()),
                                :idUsuarioCreacion,
                                :idUsuarioModificacion);";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":codigo",$temasafines->getCodigo(),PDO::PARAM_STR);
                $stmt->bindValue(":descripcion",$temasafines->getDescripcion(),PDO::PARAM_STR);
                $stmt->bindValue(":estado",$temasafines->getEstado(),PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioCreacion",$temasafines->getIdUsuarioCreacion(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$temasafines->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function ModificarTemasAfines($temasafines){
            try {
                $sql = "UPDATE `parametro_temasafines`
                SET `codigo` = :codigo,
                  `descripcion` = :descripcion,
                  `estado` = :estado,
                  `fechaModificacion` = (SELECT NOW()),
                  `idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idTemasAfines` = :idTemasAfines;";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":codigo",$temasafines->getCodigo(),PDO::PARAM_STR);
                $stmt->bindValue(":descripcion",$temasafines->getDescripcion(),PDO::PARAM_STR);
                $stmt->bindValue(":estado",$temasafines->getEstado(),PDO::PARAM_INT);
                $stmt->bindValue(":idTemasAfines",$temasafines->getIdTemasAfines(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$temasafines->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];

            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function EliminarTemasAfines($idTemasAfines){
            try {
                $sql = "DELETE FROM parametro_temasafines WHERE idTemasAfines = :idTemasAfines";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idTemasAfines", $idTemasAfines, PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }
    }

?>