<?php
    
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/parametro/subarea.entidad.php';

    class ModeloSubArea{
        private $conexion;

        public function ConsultarSubArea(){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT sa.*, a.descripcion AS 'descripcionArea' FROM parametro_subarea sa INNER JOIN parametro_area a ON a.idArea = sa.idArea");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function ConsultarSubAreaPorArea($idArea){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT * FROM parametro_subarea WHERE idArea = :idArea");
            $stmt->bindValue(":idArea", $idArea, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function GuardarSubArea($subarea){
            try {
                $sql = "INSERT INTO `parametro_subarea`
                                    (`idArea`,
                                    `codigo`,
                                    `descripcion`,
                                    `estado`,
                                    `fechaCreacion`,
                                    `fechaModificacion`,
                                    `idUsuarioCreacion`,
                                    `idUsuarioModificacion`)
                        VALUES (:idArea,
                                :codigo,
                                :descripcion,
                                :estado,
                                (SELECT NOW()),
                                (SELECT NOW()),
                                :idUsuarioCreacion,
                                :idUsuarioModificacion);";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idArea",$subarea->getIdArea(),PDO::PARAM_INT);
                $stmt->bindValue(":codigo",$subarea->getCodigo(),PDO::PARAM_STR);
                $stmt->bindValue(":descripcion",$subarea->getDescripcion(),PDO::PARAM_STR);
                $stmt->bindValue(":estado",$subarea->getEstado(),PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioCreacion",$subarea->getIdUsuarioCreacion(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$subarea->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function ModificarSubArea($subarea){
            try {
                $sql = "UPDATE `parametro_subarea`
                SET `idArea` = :idArea,
                  `codigo` = :codigo,
                  `descripcion` = :descripcion,
                  `estado` = :estado,
                  `fechaModificacion` = (SELECT NOW()),
                  `idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idSubArea` = :idSubArea;";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idArea",$subarea->getIdArea(),PDO::PARAM_INT);
                $stmt->bindValue(":codigo",$subarea->getCodigo(),PDO::PARAM_STR);
                $stmt->bindValue(":descripcion",$subarea->getDescripcion(),PDO::PARAM_STR);
                $stmt->bindValue(":estado",$subarea->getEstado(),PDO::PARAM_INT);
                $stmt->bindValue(":idSubArea",$subarea->getIdSubArea(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$subarea->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];

            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function EliminarSubArea($idSubArea){
            try {
                $sql = "DELETE FROM parametro_subarea WHERE idSubArea = :idSubArea";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idSubArea", $idSubArea, PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }
    }

?>