<?php
    
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/parametro/tiposervicio.entidad.php';

    class ModeloTipoServicio{
        private $conexion;

        public function ConsultarTipoServicio(){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT * FROM parametro_tiposervicio");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function GuardarTipoServicio($tiposervicio){
            try {
                $sql = "INSERT INTO `parametro_tiposervicio`
                                    (`codigo`,
                                    `descripcion`,
                                    `estado`,
                                    `fechaCreacion`,
                                    `fechaModificacion`,
                                    `idUsuarioCreacion`,
                                    `idUsuarioModificacion`)
                        VALUES (:codigo,
                                :descripcion,
                                :estado,
                                (SELECT NOW()),
                                (SELECT NOW()),
                                :idUsuarioCreacion,
                                :idUsuarioModificacion);";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":codigo",$tiposervicio->getCodigo(),PDO::PARAM_STR);
                $stmt->bindValue(":descripcion",$tiposervicio->getDescripcion(),PDO::PARAM_STR);
                $stmt->bindValue(":estado",$tiposervicio->getEstado(),PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioCreacion",$tiposervicio->getIdUsuarioCreacion(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$tiposervicio->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function ModificarTipoServicio($tiposervicio){
            try {
                $sql = "UPDATE `parametro_tiposervicio`
                SET `codigo` = :codigo,
                  `descripcion` = :descripcion,
                  `estado` = :estado,
                  `fechaModificacion` = (SELECT NOW()),
                  `idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idTipoServicio` = :idTipoServicio;";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":codigo",$tiposervicio->getCodigo(),PDO::PARAM_STR);
                $stmt->bindValue(":descripcion",$tiposervicio->getDescripcion(),PDO::PARAM_STR);
                $stmt->bindValue(":estado",$tiposervicio->getEstado(),PDO::PARAM_INT);
                $stmt->bindValue(":idTipoServicio",$tiposervicio->getIdTipoServicio(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$tiposervicio->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];

            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function EliminarTipoServicio($idTipoServicio){
            try {
                $sql = "DELETE FROM parametro_tiposervicio WHERE idTipoServicio = :idTipoServicio";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idTipoServicio", $idTipoServicio, PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }
    }

?>