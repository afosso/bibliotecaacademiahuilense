<?php
    
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/parametro/tipo.entidad.php';

    class ModeloTipo{
        private $conexion;

        public function ConsultarTipo(){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT * FROM parametro_tipo");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function GuardarTipo($tipo){
            try {
                $sql = "INSERT INTO `parametro_tipo`
                                    (`codigo`,
                                    `descripcion`,
                                    `estado`,
                                    `fechaCreacion`,
                                    `fechaModificacion`,
                                    `idUsuarioCreacion`,
                                    `idUsuarioModificacion`)
                        VALUES (:codigo,
                                :descripcion,
                                :estado,
                                (SELECT NOW()),
                                (SELECT NOW()),
                                :idUsuarioCreacion,
                                :idUsuarioModificacion);";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":codigo",$tipo->getCodigo(),PDO::PARAM_STR);
                $stmt->bindValue(":descripcion",$tipo->getDescripcion(),PDO::PARAM_STR);
                $stmt->bindValue(":estado",$tipo->getEstado(),PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioCreacion",$tipo->getIdUsuarioCreacion(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$tipo->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function ModificarTipo($tipo){
            try {
                $sql = "UPDATE `parametro_tipo`
                SET `codigo` = :codigo,
                  `descripcion` = :descripcion,
                  `estado` = :estado,
                  `fechaModificacion` = (SELECT NOW()),
                  `idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idTipo` = :idTipo;";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":codigo",$tipo->getCodigo(),PDO::PARAM_STR);
                $stmt->bindValue(":descripcion",$tipo->getDescripcion(),PDO::PARAM_STR);
                $stmt->bindValue(":estado",$tipo->getEstado(),PDO::PARAM_INT);
                $stmt->bindValue(":idTipo",$tipo->getIdTipo(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$tipo->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];

            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function EliminarTipo($idTipo){
            try {
                $sql = "DELETE FROM parametro_tipo WHERE idTipo = :idTipo";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idTipo", $idTipo, PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }
    }

?>