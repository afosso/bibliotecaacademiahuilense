<?php
    
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/parametro/area.entidad.php';

    class ModeloArea{
        private $conexion;

        public function ConsultarArea(){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT * FROM parametro_area");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function GuardarArea($area){
            try {
                $sql = "INSERT INTO `parametro_area`
                                    (`codigo`,
                                    `descripcion`,
                                    `estado`,
                                    `fechaCreacion`,
                                    `fechaModificacion`,
                                    `idUsuarioCreacion`,
                                    `idUsuarioModificacion`)
                        VALUES (:codigo,
                                :descripcion,
                                :estado,
                                (SELECT NOW()),
                                (SELECT NOW()),
                                :idUsuarioCreacion,
                                :idUsuarioModificacion);";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":codigo",$area->getCodigo(),PDO::PARAM_STR);
                $stmt->bindValue(":descripcion",$area->getDescripcion(),PDO::PARAM_STR);
                $stmt->bindValue(":estado",$area->getEstado(),PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioCreacion",$area->getIdUsuarioCreacion(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$area->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function ModificarArea($area){
            try {
                $sql = "UPDATE `parametro_area`
                SET `codigo` = :codigo,
                  `descripcion` = :descripcion,
                  `estado` = :estado,
                  `fechaModificacion` = (SELECT NOW()),
                  `idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idArea` = :idArea;";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":codigo",$area->getCodigo(),PDO::PARAM_STR);
                $stmt->bindValue(":descripcion",$area->getDescripcion(),PDO::PARAM_STR);
                $stmt->bindValue(":estado",$area->getEstado(),PDO::PARAM_INT);
                $stmt->bindValue(":idArea",$area->getIdArea(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$area->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];

            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function EliminarArea($idArea){
            try {
                $sql = "DELETE FROM parametro_area WHERE idArea = :idArea";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idArea", $idArea, PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }
    }

?>