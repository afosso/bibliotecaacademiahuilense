<?php

    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/seguridad/rolformulario.entidad.php';

    class ModeloRolFormulario{
        private $conexion;

        public function ConsultarRolFormularioPorIdRol($idRol){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT * FROM seguridad_rolformulario WHERE idRol = :idRol");
            $stmt->bindValue(":idRol", $idRol, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function GuardarRolFormulario($rolFormulario){
            try {
                $sql = "INSERT INTO `seguridad_rolformulario`
                                    (`idRol`,
                                    `idFormulario`)
                        VALUES (:idRol,
                                :idFormulario);";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idRol", $rolFormulario->getIdRol(), PDO::PARAM_INT);
                $stmt->bindValue(":idFormulario", $rolFormulario->getIdFormulario(), PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function EliminarRolFormulario($rolFormulario){
            try {
                $conexion = new Conexion();
                $stmt = $conexion->prepare("DELETE FROM seguridad_rolformulario WHERE idRol = :idRol AND idFormulario = :idFormulario");
                $stmt->bindValue(":idRol", $rolFormulario->getIdRol(), PDO::PARAM_INT);
                $stmt->bindValue(":idFormulario", $rolFormulario->getIdFormulario(), PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }
    }

?>