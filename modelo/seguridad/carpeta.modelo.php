<?php

    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/seguridad/carpeta.entidad.php';
    
    class ModeloCarpeta{
        private $conexion;

        public function ConsultaCarpeta(){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT c.*, cp.descripcion AS 'descripcionCarpetaPadre' FROM seguridad_carpeta c LEFT JOIN seguridad_carpeta cp ON c.idCarpetaPadre = cp.idCarpeta");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function GuardarCarpeta($carpeta){
            try{
                $sql = "INSERT INTO `seguridad_carpeta`
                                    (`idCarpetaPadre`,
                                    `codigo`,
                                    `descripcion`,
                                    `estado`,
                                    `fechaCreacion`,
                                    `fechaModificacion`,
                                    `idUsuarioCreacion`,
                                    `idUsuarioModificacion`)
                        VALUES (:idCarpetaPadre,
                                :codigo,
                                :descripcion,
                                :estado,
                                (SELECT NOW()),
                                (SELECT NOW()),
                                :idUsuarioCreacion,
                                :idUsuarioModificacion);";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $idCarpetaPadre = $carpeta->getIdCarpetaPadre() == '' || $carpeta->getIdCarpetaPadre() == '-1' ? null : $carpeta->getIdCarpetaPadre();

                $stmt->bindValue(":idCarpetaPadre", $idCarpetaPadre, PDO::PARAM_INT);
                $stmt->bindValue(":codigo", $carpeta->getCodigo(), PDO::PARAM_STR);
                $stmt->bindValue(":descripcion", $carpeta->getDescripcion(), PDO::PARAM_STR);
                $stmt->bindValue(":estado", $carpeta->getEstado(), PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioCreacion", $carpeta->getIdUsuarioCreacion(), PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioModificacion", $carpeta->getIdUsuarioModificacion(), PDO::PARAM_INT);

                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];

            }catch(PDOException $error){
                return $error->error_reporting();
            }
        }

        public function ModificarCarpeta($carpeta){
            try{
                $sql = "UPDATE `seguridad_carpeta`
                SET `idCarpetaPadre` = :idCarpetaPadre,
                  `codigo` = :codigo,
                  `descripcion` = :descripcion,
                  `estado` = :estado,
                  `fechaModificacion` = (SELECT NOW()),
                  `idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idCarpeta` = :idCarpeta;";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $idCarpetaPadre = $carpeta->getIdCarpetaPadre() == '' || $carpeta->getIdCarpetaPadre() == "-1" ? null : $carpeta->getIdCarpetaPadre();

                $stmt->bindValue(":idCarpetaPadre", $idCarpetaPadre, PDO::PARAM_INT);
                $stmt->bindValue(":codigo", $carpeta->getCodigo(), PDO::PARAM_STR);
                $stmt->bindValue(":descripcion", $carpeta->getDescripcion(), PDO::PARAM_STR);
                $stmt->bindValue(":estado", $carpeta->getEstado(), PDO::PARAM_INT);
                $stmt->bindValue(":idCarpeta", $carpeta->getIdCarpeta(), PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioModificacion", $carpeta->getIdUsuarioModificacion(), PDO::PARAM_INT);

                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];

            }catch(PDOException $error){
                return $error->error_reporting();
            }
        }

        public function EliminarCarpeta($idCarpeta){
            try{
                $conexion = new Conexion();
                $stmt = $conexion->prepare("DELETE FROM seguridad_carpeta WHERE idCarpeta = :idCarpeta");
                $stmt->bindValue(":idCarpeta", $idCarpeta, PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            }catch(PDOException $error){
                return $error->error_reporting();
            }
        }

    }

?>