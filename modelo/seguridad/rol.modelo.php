<?php

    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/seguridad/rol.entidad.php';

    class ModeloRol{

        private $conexion;

        public function ConsultarRoles(){
            $sql = "SELECT * FROM seguridad_rol";
            $conexion = new Conexion();
            $stmt = $conexion->prepare($sql);
            $stmt->execute();
            $retorno = $stmt->fetchAll(PDO::FETCH_OBJ);
            return $retorno;
        }

        public function GuardarRol($rol){
            try{
                $sql = "INSERT INTO `seguridad_rol`
                                    (`codigo`,
                                    `descripcion`,
                                    `estado`,
                                    `fechaCreacion`,
                                    `fechaModificacion`,
                                    `idUsuarioCreacion`,
                                    `idUsuarioModificacion`)
                        VALUES (:codigo,
                                :descripcion,
                                :estado,
                                (SELECT NOW()),
                                (SELECT NOW()),
                                :idUsuarioCreacion,
                                :idUsuarioModificacion);";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":codigo", $rol->getCodigo(), PDO::PARAM_STR);
                $stmt->bindValue(":descripcion", $rol->getDescripcion(), PDO::PARAM_STR);
                $stmt->bindValue(":estado", $rol->getEstado(), PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioCreacion", $rol->getIdUsuarioCreacion(), PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion", $rol->getIdUsuarioModificacion(), PDO::PARAM_STR);
                if($stmt->execute()){   
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            }catch(PDOException $error){
                return $error->getMessage();
            }
        }

        public function ModificarRol($rol){
            try{
                $sql = "UPDATE `seguridad_rol`
                SET `codigo` = :codigo,
                  `descripcion` = :descripcion,
                  `estado` = :estado,
                  `fechaModificacion` = (SELECT NOW()),
                  `idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idRol` = :idRol;";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":codigo", $rol->getCodigo(), PDO::PARAM_STR);
                $stmt->bindValue(":descripcion", $rol->getDescripcion(), PDO::PARAM_STR);
                $stmt->bindValue(":estado", $rol->getEstado(), PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion", $rol->getIdUsuarioModificacion(), PDO::PARAM_STR);
                $stmt->bindValue(":idRol", $rol->getIdRol(), PDO::PARAM_STR);
                if($stmt->execute()){   
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            }catch(PDOException $error){
                return $error->error_reporting();
            }
        }

        public function EliminarRol($idRol){
            try{
                $conexion = new Conexion();
                $stmt = $conexion->prepare("DELETE FROM seguridad_rol WHERE idRol = $idRol");
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            }catch(PDOException $error){
                return $error->error_reporting();
            }
        }

        

        
    }

?>