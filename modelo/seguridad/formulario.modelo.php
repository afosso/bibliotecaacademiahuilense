<?php

    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/seguridad/formulario.entidad.php';

    class ModeloFormulario{
        private $conexion;

        public function ConsultarFormulario(){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT f.*, c.descripcion AS 'descripcionCarpeta' FROM seguridad_formulario f LEFT JOIN seguridad_carpeta c ON c.idCarpeta = f.idCarpeta");
            $stmt->execute();
            return $stmt->fetchAll(\PDO::FETCH_OBJ);
        }

        public function GuardarFormulario($formulario){
            try {
                $sql = "INSERT INTO `seguridad_formulario`
                                    (`idCarpeta`,
                                    `descripcion`,
                                    `ruta`,
                                    `icono`,
                                    `estado`,
                                    `fechaCreacion`,
                                    `fechaModificacion`,
                                    `idUsuarioCreacion`,
                                    `idUsuarioModificacion`)
                        VALUES (:idCarpeta,
                                :descripcion,
                                :ruta,
                                :icono,
                                :estado,
                                (SELECT NOW()),
                                (SELECT NOW()),
                                :idUsuarioCreacion,
                                :idUsuarioModificacion);";
                            
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idCarpeta", $formulario->getIdCarpeta(), PDO::PARAM_INT);
                $stmt->bindValue(":descripcion", $formulario->getDescripcion(), PDO::PARAM_STR);
                $stmt->bindValue(":ruta", $formulario->getRuta(), PDO::PARAM_STR);
                $stmt->bindValue(":icono", $formulario->getIcono(), PDO::PARAM_STR);
                $stmt->bindValue(":estado", $formulario->getEstado(), PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioCreacion", $formulario->getIdUsuarioCreacion(), PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioModificacion", $formulario->getIdUsuarioModificacion(), PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function ModificarFormulario($formulario){
            try {
                $sql = "UPDATE `seguridad_formulario`
                SET `idCarpeta` = :idCarpeta,
                  `descripcion` = :descripcion,
                  `ruta` = :ruta,
                  `icono` = :icono,
                  `estado` = :estado,
                  `fechaModificacion` = (SELECT NOW()),
                  `idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idFormulario` = :idFormulario;";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idCarpeta", $formulario->getIdCarpeta(), PDO::PARAM_INT);
                $stmt->bindValue(":descripcion", $formulario->getDescripcion(), PDO::PARAM_STR);
                $stmt->bindValue(":ruta", $formulario->getRuta(), PDO::PARAM_STR);
                $stmt->bindValue(":icono", $formulario->getIcono(), PDO::PARAM_STR);
                $stmt->bindValue(":estado", $formulario->getEstado(), PDO::PARAM_STR);
                $stmt->bindValue(":idFormulario", $formulario->getIdFormulario(), PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioModificacion", $formulario->getIdUsuarioModificacion(), PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function EliminarFormulario($idFormulario){
            try {
                $conexion = new Conexion();
                $stmt = $conexion->prepare("DELETE FROM seguridad_formulario WHERE idFormulario = :idFormulario");
                $stmt->bindValue(":idFormulario", $idFormulario, PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }
    }

?>