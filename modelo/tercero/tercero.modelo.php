<?php
    
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/tercero/tercero.entidad.php';

    class ModeloTercero{
        private $conexion;

        public function ConsultarTercero(){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT * FROM tercero_tercero");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function ConsultarTerceroPorId($idTercero){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT * FROM tercero_tercero WHERE idTercero = " . $idTercero);
            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_OBJ);
        }

        public function ConsultarTerceroAvanzada($tercero){
            $whereAnd = "";
            if($tercero->getNit() != null){
                if($whereAnd == ""){
                    $whereAnd = " WHERE ";
                }else{
                    $whereAnd = " AND ";
                }
                $whereAnd .= " nit = '" . $tercero->getNit() . "'";
            }

            if($tercero->getNombres() != null){
                if($whereAnd == ""){
                    $whereAnd = " WHERE ";
                }else{
                    $whereAnd = " AND ";
                }
                $whereAnd .= " nombres = '" . $tercero->getNombres() . "'";
            }

            if($tercero->getApellidos() != null){
                if($whereAnd == ""){
                    $whereAnd = " WHERE ";
                }else{
                    $whereAnd = " AND ";
                }
                $whereAnd .= " apellidos = '" . $tercero->getApellidos() . "'";
            }

            $sql = "SELECT * FROM tercero_tercero " . $whereAnd;
            $conexion = new Conexion();
            $stmt = $conexion->prepare($sql);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function GuardarTercero($tercero){
            try {
                $sql = "INSERT INTO `tercero_tercero`
                                                (`idTipoDocumento`,
                                                `idMunicipioResidencia`,
                                                `nit`,
                                                `nombres`,
                                                `apellidos`,
                                                `genero`,
                                                `direccion`,
                                                `estado`,
                                                `fechaCreacion`,
                                                `fechaModificacion`,
                                                `idUsuarioCreacion`,
                                                `idUsuarioModificacion`)
                                    VALUES (:idTipoDocumento,
                                            :idMunicipioResidencia,
                                            :nit,
                                            :nombres,
                                            :apellidos,
                                            :genero,
                                            :direccion,
                                            :estado,
                                            (SELECT NOW()),
                                            (SELECT NOW()),
                                            :idUsuarioCreacion,
                                            :idUsuarioModificacion);";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idTipoDocumento",$tercero->getIdTipoDocumento(),PDO::PARAM_INT);
                $stmt->bindValue(":idMunicipioResidencia",$tercero->getIdMunicipioResidencia(),PDO::PARAM_INT);
                $stmt->bindValue(":nit",$tercero->getNit(),PDO::PARAM_STR);
                $stmt->bindValue(":nombres",$tercero->getNombres(),PDO::PARAM_STR);
                $stmt->bindValue(":apellidos",$tercero->getApellidos(),PDO::PARAM_STR);
                $stmt->bindValue(":genero",$tercero->getGenero(),PDO::PARAM_STR);
                $stmt->bindValue(":direccion",$tercero->getDireccion(),PDO::PARAM_STR);
                $stmt->bindValue(":estado",$tercero->getEstado(),PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioCreacion",$tercero->getIdUsuarioCreacion(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$tercero->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function ModificarTercero($tercero){
            try {
                $sql = "UPDATE `tercero_tercero`
                SET `idTipoDocumento` = :idTipoDocumento,
                  `idMunicipioResidencia` = :idMunicipioResidencia,
                  `nit` = :nit,
                  `nombres` = :nombres,
                  `apellidos` = :apellidos,
                  `genero` = :genero,
                  `direccion` = :direccion,
                  `estado` = :estado,
                  `fechaModificacion` = (SELECT NOW()),
                  `idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idTercero` = :idTercero;";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idTipoDocumento",$tercero->getIdTipoDocumento(),PDO::PARAM_INT);
                $stmt->bindValue(":idMunicipioResidencia",$tercero->getIdMunicipioResidencia(),PDO::PARAM_INT);
                $stmt->bindValue(":nit",$tercero->getNit(),PDO::PARAM_STR);
                $stmt->bindValue(":nombres",$tercero->getNombres(),PDO::PARAM_STR);
                $stmt->bindValue(":apellidos",$tercero->getApellidos(),PDO::PARAM_STR);
                $stmt->bindValue(":genero",$tercero->getGenero(),PDO::PARAM_STR);
                $stmt->bindValue(":direccion",$tercero->getDireccion(),PDO::PARAM_STR);
                $stmt->bindValue(":estado",$tercero->getEstado(),PDO::PARAM_INT);
                $stmt->bindValue(":idTercero",$tercero->getIdTercero(),PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioModificacion",$tercero->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];

            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function EliminarTercero($idTercero){
            try {
                $sql = "DELETE FROM tercero_tercero WHERE idTercero = :idTercero";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idTercero", $idTercero, PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }
    }

?>