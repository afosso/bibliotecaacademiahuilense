<?php

    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/ubicacion/pais.entidad.php';

    class ModeloPais{
        private $conexion;

        public function ConsultarPais(){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT * FROM ubicacion_pais");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function GuardarPais($pais){
            try {
                $sql = "INSERT INTO `ubicacion_pais`
                                    (`codigoDane`,
                                    `descripcion`,
                                    `estado`,
                                    `fechaCreacion`,
                                    `fechaModificacion`,
                                    `idUsuarioCreacion`,
                                    `idUsuarioModificacion`)
                        VALUES (:codigoDane,
                                :descripcion,
                                :estado,
                                (SELECT NOW()),
                                (SELECT NOW()),
                                :idUsuarioCreacion,
                                :idUsuarioModificacion);";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);

                $stmt->bindValue(":codigoDane", $pais->getCodigoDane(), PDO::PARAM_STR);
                $stmt->bindValue(":descripcion", $pais->getDescripcion(), PDO::PARAM_STR);
                $stmt->bindValue(":estado", $pais->getEstado(), PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioCreacion", $pais->getIdUsuarioCreacion(), PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion", $pais->getIdUsuarioModificacion(), PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function ModificarPais($pais){
            try {
                $sql = "UPDATE `ubicacion_pais`
                SET `codigoDane` = :codigoDane,
                  `descripcion` = :descripcion,
                  `estado` = :estado,
                  `fechaModificacion` = (SELECT NOW()),
                  `idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idPais` = :idPais;";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);

                $stmt->bindValue(":codigoDane", $pais->getCodigoDane(), PDO::PARAM_STR);
                $stmt->bindValue(":descripcion", $pais->getDescripcion(), PDO::PARAM_STR);
                $stmt->bindValue(":estado", $pais->getEstado(), PDO::PARAM_INT);
                $stmt->bindValue(":idPais", $pais->getIdPais(), PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion", $pais->getIdUsuarioModificacion(), PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function EliminarPais($idPais){
            try {
                $sql = "DELETE FROM ubicacion_pais WHERE idPais = :idPais";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idPais", $pais->getIdPais(), PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }
    }

?>