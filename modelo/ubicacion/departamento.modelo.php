<?php

    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/ubicacion/departamento.entidad.php';

    class ModeloDepartamento{
        private $conexion;

        public function ConsultarDepartamento(){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT d.*, p.descripcion AS 'descripcionPais' FROM ubicacion_departamento d INNER JOIN ubicacion_pais p ON p.idPais = d.idPais");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function GuardarDepartamento($departamento){
            try {
                $sql = "INSERT INTO `ubicacion_departamento`
                                    (`idPais`,
                                    `codigoDane`,
                                    `descripcion`,
                                    `estado`,
                                    `fechaCreacion`,
                                    `fechaModificacion`,
                                    `idUsuarioCreacion`,
                                    `idUsuarioModificacion`)
                        VALUES (:idPais,
                                :codigoDane,
                                :descripcion,
                                :estado,
                                (SELECT NOW()),
                                (SELECT NOW()),
                                :idUsuarioCreacion,
                                :idUsuarioModificacion);";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);

                $stmt->bindValue(":idPais", $departamento->getIdPais(), PDO::PARAM_STR);
                $stmt->bindValue(":codigoDane", $departamento->getCodigoDane(), PDO::PARAM_STR);
                $stmt->bindValue(":descripcion", $departamento->getDescripcion(), PDO::PARAM_STR);
                $stmt->bindValue(":estado", $departamento->getEstado(), PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioCreacion", $departamento->getIdUsuarioCreacion(), PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion", $departamento->getIdUsuarioModificacion(), PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function ModificarDepartamento($departamento){
            try {
                $sql = "UPDATE `ubicacion_departamento`
                SET `idPais` = :idPais,
                  `codigoDane` = :codigoDane,
                  `descripcion` = :descripcion,
                  `estado` = :estado,
                  `fechaModificacion` = (SELECT NOW()),
                  `idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idDepartamento` = :idDepartamento;";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);

                $stmt->bindValue(":idPais", $departamento->getIdPais(), PDO::PARAM_STR);
                $stmt->bindValue(":codigoDane", $departamento->getCodigoDane(), PDO::PARAM_STR);
                $stmt->bindValue(":descripcion", $departamento->getDescripcion(), PDO::PARAM_STR);
                $stmt->bindValue(":estado", $departamento->getEstado(), PDO::PARAM_INT);
                $stmt->bindValue(":idDepartamento", $departamento->getIdDepartamento(), PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion", $departamento->getIdUsuarioModificacion(), PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function EliminarDepartamento($idDepartamento){
            try {
                $sql = "DELETE FROM ubicacion_departamento WHERE idDepartamento = :idDepartamento";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idDepartamento", $departamento->getIdDepartamento(), PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }
    }

?>