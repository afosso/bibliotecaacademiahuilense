<?php

    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/biblioteca/bibliotecadetalle.entidad.php';

    class ModeloBibliotecaDetalle{
        private $conexion;

        public function ConsultarDetallePorIdCabecera($idCabecera){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT * FROM biblioteca_bibliotecadetalle WHERE idBibliotecaCabecera = :idBibliotecaCabecera");
            $stmt->bindValue(":idBibliotecaCabecera", $idCabecera, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function ConsultarDetallePorId($idDetalle){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT * FROM biblioteca_bibliotecadetalle WHERE idBibliotecaDetalle = :idBibliotecaDetalle");
            $stmt->bindValue(":idBibliotecaDetalle", $idDetalle, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_OBJ);
        }

        public function GuardarDetalle($detalle){
            try {
                $sql = "INSERT INTO `biblioteca_bibliotecadetalle`
                                (`idBibliotecaCabecera`,
                                `descripcion`,
                                `numeroPagina`,
                                `nivel`,
                                `fechaCreacion`,
                                `fechaModificacion`,
                                `idUsuarioCreacion`,
                                `idUsuarioModificacion`)
                    VALUES (:idBibliotecaCabecera,
                            :descripcion,
                            :numeroPagina,
                            null,
                            (SELECT NOW()),
                            (SELECT NOW()),
                            :idUsuarioCreacion,
                            :idUsuarioModificacion);";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idBibliotecaCabecera", $detalle->getIdBibliotecaCabecera(), PDO::PARAM_INT);
                $stmt->bindValue(":descripcion", $detalle->getDescripcion(), PDO::PARAM_STR);
                $stmt->bindValue(":numeroPagina", $detalle->getNumeroPagina(), PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioCreacion", $detalle->getIdUsuarioCreacion(), PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioModificacion", $detalle->getIdUsuarioModificacion(), PDO::PARAM_INT);

                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function ModificarDetalle($detalle){
            try {
                $sql = "UPDATE `biblioteca_bibliotecadetalle`
                SET `descripcion` = :descripcion,
                  `numeroPagina` = :numeroPagina,
                  `fechaModificacion` = (SELECT NOW()),
                  `idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idBibliotecaDetalle` = :idBibliotecaDetalle;";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":descripcion", $detalle->getDescripcion(), PDO::PARAM_STR);
                $stmt->bindValue(":numeroPagina", $detalle->getNumeroPagina(), PDO::PARAM_INT);
                $stmt->bindValue(":idBibliotecaDetalle", $detalle->getIdBibliotecaDetalle(), PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioModificacion", $detalle->getIdUsuarioModificacion(), PDO::PARAM_INT);

                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function EliminaDetalle($idEliminar){
            try {
                $sql = "DELETE FROM biblioteca_bibliotecadetalle WHERE idBibliotecaDetalle = :idEliminar";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idEliminar", $idEliminar, PDO::PARAM_INT);

                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }
    }

?>