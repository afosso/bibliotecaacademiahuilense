<?php
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/biblioteca/editorial.entidad.php';

    class ModeloPrestamos{
        private $conexion;

        public function ConsultarPrestamos(){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT 
                                        p.*,
                                        bc.`descripcion` AS 'libro',
                                        CONCAT(t.`nombres`, ' ', t.`apellidos`) AS 'tercero',
                                        ts.`descripcion` AS 'tipoServicio'
                                    FROM `biblioteca_prestamos` p
                                    INNER JOIN `biblioteca_bibliotecacabecera` bc ON bc.`idBibliotecaCabecera` = p.`idBibliotecaCabecera`
                                    INNER JOIN `tercero_tercero` t ON t.`idTercero` = p.`idTercero`
                                    INNER JOIN `parametro_tiposervicio` ts ON ts.`idTipoServicio` = p.`idTipoServicio`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function ConsultarPrestamosPendientes(){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT 
                                        p.*,
                                        bc.`descripcion` AS 'libro',
                                        bc.codigo AS 'codigoLibro',
                                        CONCAT(t.`nombres`, ' ', t.`apellidos`) AS 'tercero',
                                        t.telefono,
                                        t.celular,
                                        t.correoElectronico,
                                        ts.`descripcion` AS 'tipoServicio',
                                        bc.numeroPaginas
                                    FROM `biblioteca_prestamos` p
                                    INNER JOIN `biblioteca_bibliotecacabecera` bc ON bc.`idBibliotecaCabecera` = p.`idBibliotecaCabecera`
                                    INNER JOIN `tercero_tercero` t ON t.`idTercero` = p.`idTercero`
                                    INNER JOIN `parametro_tiposervicio` ts ON ts.`idTipoServicio` = p.`idTipoServicio`
                                    WHERE fechaEntrada IS NULL
                                    ");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function GuardarPrestamos($prestamo){
            try {
                $sql = "INSERT INTO `biblioteca_prestamos`(`idBibliotecaCabecera`,`idTercero`,`idTipoServicio`,`fechaSalida`,`fechaVencimiento`,`fechaEntrada`,`numeroEjemplar`,`fechaCreacion`,`fechaModificacion`,`idUsuarioCreacion`,`idUsuarioModificacion`)
                        VALUES (:idBibliotecaCabecera,:idTercero,:idTipoServicio,:fechaSalida,:fechaVencimiento,NULL,:numeroEjemplar,(SELECT NOW()),(SELECT NOW()),:idUsuarioCreacion,:idUsuarioModificacion);";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idBibliotecaCabecera", $prestamo->getIdBibliotecaCabecera(), PDO::PARAM_INT);
                $stmt->bindValue(":idTercero", $prestamo->getIdTercero(), PDO::PARAM_INT);
                $stmt->bindValue(":idTipoServicio", $prestamo->getIdTipoServicio(), PDO::PARAM_INT);
                $stmt->bindValue(":fechaSalida", $prestamo->getFechaSalida(), PDO::PARAM_STR);
                $stmt->bindValue(":fechaVencimiento", $prestamo->getFechaVencimiento(), PDO::PARAM_STR);
                $stmt->bindValue(":numeroEjemplar", $prestamo->getNumeroEjemplar(), PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioCreacion", $prestamo->getIdUsuarioCreacion(), PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioModificacion", $prestamo->getIdUsuarioModificacion(), PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return "error";
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }


        public function ModificarPrestamos($prestamo){
            try {
                $sql = "UPDATE `biblioteca_prestamos`
                SET `fechaEntrada` = :fecha,
                  `fechaModificacion` = (SELECT NOW()),
                  `idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idPrestamo` = :idPrestamo;";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idPrestamo", $prestamo->getIdPrestamo(), PDO::PARAM_INT);
                $stmt->bindValue(":fecha", $prestamo->getFechaEntrada(), PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion", $prestamo->getIdUsuarioModificacion(), PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return "error";
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function EliminarPrestamo($idPrestamo){
            try {
                $sql = "DELETE FROM biblioteca_prestamos WHERE idPrestamo = :idPrestamo";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idPrestamo", $idPrestamo, PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return "ERROR";
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

    }


?>