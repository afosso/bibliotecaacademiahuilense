<?php

    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/biblioteca/bibliotecacabecera.entidad.php';

    class ModeloBibliotecaCabecera{
        private $conexion;

        public function ConsultarCabecera(){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT * FROM biblioteca_bibliotecacabecera");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function ConsultarCabeceraPorId($idCabecera){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT * FROM biblioteca_bibliotecacabecera WHERE idBibliotecaCabecera = :idBibliotecaCabecera");
            $stmt->bindValue(":idBibliotecaCabecera", $idCabecera, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_OBJ);
        }

        public function ConsultarCabeceraAvanzada($cabecera){
            $whereAnd = "";

            if($cabecera->getDescripcion() != null){
                if($whereAnd == ""){
                    $whereAnd .= " WHERE ";
                }else{
                    $whereAnd .= " AND ";
                }
                $whereAnd .= " bc.descripcion LIKE '%" . $cabecera->getDescripcion() . "%'";
            }

            if($cabecera->getAutor() != null){
                if($whereAnd == ""){
                    $whereAnd .= " WHERE ";
                }else{
                    $whereAnd .= " AND ";
                }
                $whereAnd .= " bc.autor LIKE '%" . $cabecera->getAutor() . "%'";
            }

            if($cabecera->getEjercicio() != null){
                if($whereAnd == ""){
                    $whereAnd .= " WHERE ";
                }else{
                    $whereAnd .= " AND ";
                }
                $whereAnd .= " bc.ejercicio = " . $cabecera->getEjercicio();
            }

            if($cabecera->getPalabrasClaves() != null){
                if($whereAnd == ""){
                    $whereAnd .= " WHERE ";
                }else{
                    $whereAnd .= " AND ";
                }
                $whereAnd .= " (bc.palabrasClaves LIKE '%" . $cabecera->getPalabrasClaves() . "%' OR bd.descripcion LIKE '%" . $cabecera->getPalabrasClaves() . "%' )";
            }
            
            

            $sql = "SELECT DISTINCT bc.* FROM biblioteca_bibliotecacabecera bc INNER JOIN biblioteca_bibliotecadetalle bd ON bd.idBibliotecaCabecera = bc.idBibliotecaCabecera  " . $whereAnd;
            $conexion = new Conexion();
            $stmt = $conexion->prepare($sql);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function GuardarCabeceraYDetalle($cabecera){
            try {
                $sql = "INSERT INTO `biblioteca_bibliotecacabecera`
                                    (`idTipo`,`idSubArea`,`idTemasAfines`,`idEditorial`,`idMunicipio`,`codigo`,`descripcion`,`autor`,`ejercicio`,`numeroPaginas`,`numeroEjemplar`,`tipoAdquisicion`,`descripcionAdquisicion`,`fechaIngreso`,`precio`,`fechaCreacion`,`fechaModificacion`,`idUsuarioCreacion`,`idUsuarioModificacion`)
                        VALUES (:idTipo,:idSubArea,:idTemasAfines,:idEditorial,:idMunicipio,:codigo,:descripcion,:autor,:ejercicio,:numeroPaginas,:numeroEjemplar,:tipoAdquisicion,:descripcionAdquisicion,:fechaIngreso,:precio,(SELECT NOW()),(SELECT NOW()),:idUsuarioCreacion,:idUsuarioModificacion);";

                $conexion = new Conexion();

                $idSubArea = $cabecera->getIdSubArea() == "-1" || $cabecera->getIdSubArea() == "" ? null : $cabecera->getIdSubArea();
                $idTemasAfines = $cabecera->getIdTemasAfines() == "-1" || $cabecera->getIdTemasAfines() == "" ? null : $cabecera->getIdTemasAfines();
                $idEditorial = $cabecera->getIdEditorial() == "-1" || $cabecera->getIdEditorial() == "" ? null : $cabecera->getIdEditorial();
                $ejercicio = $cabecera->getEjercicio() == null || $cabecera->getEjercicio() == "" ? null : $cabecera->getEjercicio();
                $tipoAdquisicion = $cabecera->getTipoAdquisicion() == "-1" || $cabecera->getTipoAdquisicion() == "" ? null : $cabecera->getTipoAdquisicion();
                $descripcionAdquisicion = $cabecera->getDescripcionAdquisicion() == null || $cabecera->getDescripcionAdquisicion() == "" ? null : $cabecera->getDescripcionAdquisicion();

                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idTipo", $cabecera->getIdTipo(), PDO::PARAM_INT);
                $stmt->bindValue(":idSubArea", $idSubArea, PDO::PARAM_INT);
                $stmt->bindValue(":idTemasAfines", $idTemasAfines, PDO::PARAM_INT);
                $stmt->bindValue(":idEditorial", $idEditorial, PDO::PARAM_INT);
                $stmt->bindValue(":idMunicipio", $cabecera->getIdMunicipio(), PDO::PARAM_INT);
                $stmt->bindValue(":codigo", $cabecera->getCodigo(), PDO::PARAM_STR);
                $stmt->bindValue(":descripcion", $cabecera->getDescripcion(), PDO::PARAM_STR);
                $stmt->bindValue(":autor", $cabecera->getAutor(), PDO::PARAM_STR);
                $stmt->bindValue(":ejercicio", $ejercicio, PDO::PARAM_STR);
                $stmt->bindValue(":numeroPaginas", $cabecera->getNumeroPaginas(), PDO::PARAM_STR);
                $stmt->bindValue(":numeroEjemplar", $cabecera->getNumeroEjemplar(), PDO::PARAM_STR);
                $stmt->bindValue(":tipoAdquisicion", $tipoAdquisicion, PDO::PARAM_STR);
                $stmt->bindValue(":descripcionAdquisicion", $descripcionAdquisicion, PDO::PARAM_STR);
                $stmt->bindValue(":fechaIngreso", $cabecera->getFechaIngreso(), PDO::PARAM_STR);
                $stmt->bindValue(":precio", $cabecera->getPrecio(), PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioCreacion", $cabecera->getIdUsuarioCreacion(), PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion", $cabecera->getIdUsuarioModificacion(), PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return "ERROR";
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function ModificarCabecera($cabecera){
            try {
                $sql = "UPDATE `biblioteca_bibliotecacabecera`
                SET `idTipo` = :idTipo,`idSubArea` = :idSubArea,`idTemasAfines` = :idTemasAfines,`idEditorial` = :idEditorial,`idMunicipio` = :idMunicipio,`codigo` = :codigo,`descripcion` = :descripcion,`autor` = :autor,`ejercicio` = :ejercicio,`numeroPaginas` = :numeroPaginas,`numeroEjemplar` = :numeroEjemplar,`tipoAdquisicion` = :tipoAdquisicion,`descripcionAdquisicion` = :descripcionAdquisicion,`fechaIngreso` = :fechaIngreso,`precio` = :precio,`fechaModificacion` = (SELECT NOW()),`idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idBibliotecaCabecera` = :idBibliotecaCabecera;";

                $conexion = new Conexion();

                $idSubArea = $cabecera->getIdSubArea() == "-1" || $cabecera->getIdSubArea() == "" ? null : $cabecera->getIdSubArea();
                $idTemasAfines = $cabecera->getIdTemasAfines() == "-1" || $cabecera->getIdTemasAfines() == "" ? null : $cabecera->getIdTemasAfines();
                $idEditorial = $cabecera->getIdEditorial() == "-1" || $cabecera->getIdEditorial() == "" ? null : $cabecera->getIdEditorial();
                $ejercicio = $cabecera->getEjercicio() == null || $cabecera->getEjercicio() == "" ? null : $cabecera->getEjercicio();
                $tipoAdquisicion = $cabecera->getTipoAdquisicion() == "-1" || $cabecera->getTipoAdquisicion() == "" ? null : $cabecera->getTipoAdquisicion();
                $descripcionAdquisicion = $cabecera->getDescripcionAdquisicion() == null || $cabecera->getDescripcionAdquisicion() == "" ? null : $cabecera->getDescripcionAdquisicion();

                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idBibliotecaCabecera", $cabecera->getIdBibliotecaCabecera(), PDO::PARAM_INT);
                $stmt->bindValue(":idTipo", $cabecera->getIdTipo(), PDO::PARAM_INT);
                $stmt->bindValue(":idSubArea", $idSubArea, PDO::PARAM_INT);
                $stmt->bindValue(":idTemasAfines", $idTemasAfines, PDO::PARAM_INT);
                $stmt->bindValue(":idEditorial", $idEditorial, PDO::PARAM_INT);
                $stmt->bindValue(":idMunicipio", $cabecera->getIdMunicipio(), PDO::PARAM_INT);
                $stmt->bindValue(":codigo", $cabecera->getCodigo(), PDO::PARAM_STR);
                $stmt->bindValue(":descripcion", $cabecera->getDescripcion(), PDO::PARAM_STR);
                $stmt->bindValue(":autor", $cabecera->getAutor(), PDO::PARAM_STR);
                $stmt->bindValue(":ejercicio", $ejercicio, PDO::PARAM_STR);
                $stmt->bindValue(":numeroPaginas", $cabecera->getNumeroPaginas(), PDO::PARAM_STR);
                $stmt->bindValue(":numeroEjemplar", $cabecera->getNumeroEjemplar(), PDO::PARAM_STR);
                $stmt->bindValue(":tipoAdquisicion", $tipoAdquisicion, PDO::PARAM_STR);
                $stmt->bindValue(":descripcionAdquisicion", $descripcionAdquisicion, PDO::PARAM_STR);
                $stmt->bindValue(":fechaIngreso", $cabecera->getFechaIngreso(), PDO::PARAM_STR);
                $stmt->bindValue(":precio", $cabecera->getPrecio(), PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion", $cabecera->getIdUsuarioModificacion(), PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return "ERROR";
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function EliminarCabecera($idCabecera){
            try {
                
                $conexion = new Conexion();
                $conexion->beginTransaction();

                $stmt = $conexion->prepare("DELETE FROM biblioteca_bibliotecadetalle WHERE idBibliotecaCabecera = :idBibliotecaCabecera");
                $stmt->bindValue(":idBibliotecaCabecera", $idCabecera, PDO::PARAM_INT);
                if($stmt->execute()){
                    $stmt = $conexion->prepare("DELETE FROM biblioteca_bibliotecacabecera WHERE idBibliotecaCabecera = :idBibliotecaCabecera");
                    $stmt->bindValue(":idBibliotecaCabecera", $idCabecera, PDO::PARAM_INT);
                    if(!$stmt->execute()){
                        $conexion->rollBack();
                        return "ERROR";
                    }
                    $conexion->commit();
                    return "OK";
                }
                $conexion->rollBack();
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }
    }

?>