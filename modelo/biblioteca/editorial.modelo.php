<?php
    
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entorno/conexion.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/biblioteca/editorial.entidad.php';

    class ModeloEditorial{
        private $conexion;

        public function ConsultarEditorial(){
            $conexion = new Conexion();
            $stmt = $conexion->prepare("SELECT * FROM biblioteca_editorial");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function GuardarEditorial($editorial){
            try {
                $sql = "INSERT INTO `biblioteca_editorial`
                                    (`codigo`,
                                    `descripcion`,
                                    `estado`,
                                    `fechaCreacion`,
                                    `fechaModificacion`,
                                    `idUsuarioCreacion`,
                                    `idUsuarioModificacion`)
                        VALUES (:codigo,
                                :descripcion,
                                :estado,
                                (SELECT NOW()),
                                (SELECT NOW()),
                                :idUsuarioCreacion,
                                :idUsuarioModificacion);";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":codigo",$editorial->getCodigo(),PDO::PARAM_STR);
                $stmt->bindValue(":descripcion",$editorial->getDescripcion(),PDO::PARAM_STR);
                $stmt->bindValue(":estado",$editorial->getEstado(),PDO::PARAM_INT);
                $stmt->bindValue(":idUsuarioCreacion",$editorial->getIdUsuarioCreacion(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$editorial->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function ModificarEditorial($editorial){
            try {
                $sql = "UPDATE `biblioteca_editorial`
                SET `codigo` = :codigo,
                  `descripcion` = :descripcion,
                  `estado` = :estado,
                  `fechaModificacion` = (SELECT NOW()),
                  `idUsuarioModificacion` = :idUsuarioModificacion
                WHERE `idEditorial` = :idEditorial;";

                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":codigo",$editorial->getCodigo(),PDO::PARAM_STR);
                $stmt->bindValue(":descripcion",$editorial->getDescripcion(),PDO::PARAM_STR);
                $stmt->bindValue(":estado",$editorial->getEstado(),PDO::PARAM_INT);
                $stmt->bindValue(":idEditorial",$editorial->getIdEditorial(),PDO::PARAM_STR);
                $stmt->bindValue(":idUsuarioModificacion",$editorial->getIdUsuarioModificacion(),PDO::PARAM_STR);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];

            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }

        public function EliminarEditorial($idEditorial){
            try {
                $sql = "DELETE FROM biblioteca_editorial WHERE idEditorial = :idEditorial";
                $conexion = new Conexion();
                $stmt = $conexion->prepare($sql);
                $stmt->bindValue(":idEditorial", $idEditorial, PDO::PARAM_INT);
                if($stmt->execute()){
                    return "OK";
                }
                return $stmt->errorInfo()[2];
            } catch (PDOException $error) {
                return $error->error_reporting();
            }
        }
    }

?>