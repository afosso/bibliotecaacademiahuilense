<?php

    class BibliotecaCabercera{
        private $idBibliotecaCabecera;
        private $idTipo;
        private $idSubArea;
        private $idTemasAfines;
        private $idEditorial;
        private $idMunicipio;
        private $codigo;
        private $descripcion;
        private $autor;
        private $ejercicio;
        private $numeroPaginas;
        private $numeroEjemplar;
        private $tipoAdquisicion;
        private $descripcionAdquisicion;
        private $fechaIngreso;
        private $precio;
        private $palabrasClaves;
        private $idUsuarioCreacion;
        private $idUsuarioModificacion;

        /**
         * Get the value of idBibliotecaCabecera
         */ 
        public function getIdBibliotecaCabecera()
        {
                return $this->idBibliotecaCabecera;
        }

        /**
         * Set the value of idBibliotecaCabecera
         *
         * @return  self
         */ 
        public function setIdBibliotecaCabecera($idBibliotecaCabecera)
        {
                $this->idBibliotecaCabecera = $idBibliotecaCabecera;

                return $this;
        }

        /**
         * Get the value of idTipo
         */ 
        public function getIdTipo()
        {
                return $this->idTipo;
        }

        /**
         * Set the value of idTipo
         *
         * @return  self
         */ 
        public function setIdTipo($idTipo)
        {
                $this->idTipo = $idTipo;

                return $this;
        }

        /**
         * Get the value of idSubArea
         */ 
        public function getIdSubArea()
        {
                return $this->idSubArea;
        }

        /**
         * Set the value of idSubArea
         *
         * @return  self
         */ 
        public function setIdSubArea($idSubArea)
        {
                $this->idSubArea = $idSubArea;

                return $this;
        }

        /**
         * Get the value of idTemasAfines
         */ 
        public function getIdTemasAfines()
        {
                return $this->idTemasAfines;
        }

        /**
         * Set the value of idTemasAfines
         *
         * @return  self
         */ 
        public function setIdTemasAfines($idTemasAfines)
        {
                $this->idTemasAfines = $idTemasAfines;

                return $this;
        }

        /**
         * Get the value of idEditorial
         */ 
        public function getIdEditorial()
        {
                return $this->idEditorial;
        }

        /**
         * Set the value of idEditorial
         *
         * @return  self
         */ 
        public function setIdEditorial($idEditorial)
        {
                $this->idEditorial = $idEditorial;

                return $this;
        }

        /**
         * Get the value of idMunicipio
         */ 
        public function getIdMunicipio()
        {
                return $this->idMunicipio;
        }

        /**
         * Set the value of idMunicipio
         *
         * @return  self
         */ 
        public function setIdMunicipio($idMunicipio)
        {
                $this->idMunicipio = $idMunicipio;

                return $this;
        }

        /**
         * Get the value of codigo
         */ 
        public function getCodigo()
        {
                return $this->codigo;
        }

        /**
         * Set the value of codigo
         *
         * @return  self
         */ 
        public function setCodigo($codigo)
        {
                $this->codigo = $codigo;

                return $this;
        }

        /**
         * Get the value of descripcion
         */ 
        public function getDescripcion()
        {
                return $this->descripcion;
        }

        /**
         * Set the value of descripcion
         *
         * @return  self
         */ 
        public function setDescripcion($descripcion)
        {
                $this->descripcion = $descripcion;

                return $this;
        }

        /**
         * Get the value of autor
         */ 
        public function getAutor()
        {
                return $this->autor;
        }

        /**
         * Set the value of autor
         *
         * @return  self
         */ 
        public function setAutor($autor)
        {
                $this->autor = $autor;

                return $this;
        }

        /**
         * Get the value of ejercicio
         */ 
        public function getEjercicio()
        {
                return $this->ejercicio;
        }

        /**
         * Set the value of ejercicio
         *
         * @return  self
         */ 
        public function setEjercicio($ejercicio)
        {
                $this->ejercicio = $ejercicio;

                return $this;
        }

        /**
         * Get the value of numeroPaginas
         */ 
        public function getNumeroPaginas()
        {
                return $this->numeroPaginas;
        }

        /**
         * Set the value of numeroPaginas
         *
         * @return  self
         */ 
        public function setNumeroPaginas($numeroPaginas)
        {
                $this->numeroPaginas = $numeroPaginas;

                return $this;
        }

        /**
         * Get the value of numeroEjemplar
         */ 
        public function getNumeroEjemplar()
        {
                return $this->numeroEjemplar;
        }

        /**
         * Set the value of numeroEjemplar
         *
         * @return  self
         */ 
        public function setNumeroEjemplar($numeroEjemplar)
        {
                $this->numeroEjemplar = $numeroEjemplar;

                return $this;
        }

        /**
         * Get the value of tipoAdquisicion
         */ 
        public function getTipoAdquisicion()
        {
                return $this->tipoAdquisicion;
        }

        /**
         * Set the value of tipoAdquisicion
         *
         * @return  self
         */ 
        public function setTipoAdquisicion($tipoAdquisicion)
        {
                $this->tipoAdquisicion = $tipoAdquisicion;

                return $this;
        }

        /**
         * Get the value of descripcionAdquisicion
         */ 
        public function getDescripcionAdquisicion()
        {
                return $this->descripcionAdquisicion;
        }

        /**
         * Set the value of descripcionAdquisicion
         *
         * @return  self
         */ 
        public function setDescripcionAdquisicion($descripcionAdquisicion)
        {
                $this->descripcionAdquisicion = $descripcionAdquisicion;

                return $this;
        }

        /**
         * Get the value of fechaIngreso
         */ 
        public function getFechaIngreso()
        {
                return $this->fechaIngreso;
        }

        /**
         * Set the value of fechaIngreso
         *
         * @return  self
         */ 
        public function setFechaIngreso($fechaIngreso)
        {
                $this->fechaIngreso = $fechaIngreso;

                return $this;
        }

        /**
         * Get the value of precio
         */ 
        public function getPrecio()
        {
                return $this->precio;
        }

        /**
         * Set the value of precio
         *
         * @return  self
         */ 
        public function setPrecio($precio)
        {
                $this->precio = $precio;

                return $this;
        }

        /**
         * Get the value of idUsuarioCreacion
         */ 
        public function getIdUsuarioCreacion()
        {
                return $this->idUsuarioCreacion;
        }

        /**
         * Set the value of idUsuarioCreacion
         *
         * @return  self
         */ 
        public function setIdUsuarioCreacion($idUsuarioCreacion)
        {
                $this->idUsuarioCreacion = $idUsuarioCreacion;

                return $this;
        }

        /**
         * Get the value of idUsuarioModificacion
         */ 
        public function getIdUsuarioModificacion()
        {
                return $this->idUsuarioModificacion;
        }

        /**
         * Set the value of idUsuarioModificacion
         *
         * @return  self
         */ 
        public function setIdUsuarioModificacion($idUsuarioModificacion)
        {
                $this->idUsuarioModificacion = $idUsuarioModificacion;

                return $this;
        }

        /**
         * Get the value of palabrasClaves
         */ 
        public function getPalabrasClaves()
        {
                return $this->palabrasClaves;
        }

        /**
         * Set the value of palabrasClaves
         *
         * @return  self
         */ 
        public function setPalabrasClaves($palabrasClaves)
        {
                $this->palabrasClaves = $palabrasClaves;

                return $this;
        }
    }

?>