<?php

    class BibliotecaDetalle{
        private $idBibliotecaDetalle;
        private $idBibliotecaCabecera;
        private $descripcion;
        private $numeroPagina;
        private $nivel;
        private $fechaCreacion;
        private $fechaModificacion;
        private $idUsuarioCreacion;
        private $idUsuarioModificacion;

        /**
         * Get the value of idBibliotecaDetalle
         */ 
        public function getIdBibliotecaDetalle()
        {
                return $this->idBibliotecaDetalle;
        }

        /**
         * Set the value of idBibliotecaDetalle
         *
         * @return  self
         */ 
        public function setIdBibliotecaDetalle($idBibliotecaDetalle)
        {
                $this->idBibliotecaDetalle = $idBibliotecaDetalle;

                return $this;
        }

        /**
         * Get the value of idBibliotecaCabecera
         */ 
        public function getIdBibliotecaCabecera()
        {
                return $this->idBibliotecaCabecera;
        }

        /**
         * Set the value of idBibliotecaCabecera
         *
         * @return  self
         */ 
        public function setIdBibliotecaCabecera($idBibliotecaCabecera)
        {
                $this->idBibliotecaCabecera = $idBibliotecaCabecera;

                return $this;
        }

        /**
         * Get the value of descripcion
         */ 
        public function getDescripcion()
        {
                return $this->descripcion;
        }

        /**
         * Set the value of descripcion
         *
         * @return  self
         */ 
        public function setDescripcion($descripcion)
        {
                $this->descripcion = $descripcion;

                return $this;
        }

        /**
         * Get the value of numeroPagina
         */ 
        public function getNumeroPagina()
        {
                return $this->numeroPagina;
        }

        /**
         * Set the value of numeroPagina
         *
         * @return  self
         */ 
        public function setNumeroPagina($numeroPagina)
        {
                $this->numeroPagina = $numeroPagina;

                return $this;
        }

        /**
         * Get the value of nivel
         */ 
        public function getNivel()
        {
                return $this->nivel;
        }

        /**
         * Set the value of nivel
         *
         * @return  self
         */ 
        public function setNivel($nivel)
        {
                $this->nivel = $nivel;

                return $this;
        }

        /**
         * Get the value of fechaCreacion
         */ 
        public function getFechaCreacion()
        {
                return $this->fechaCreacion;
        }

        /**
         * Set the value of fechaCreacion
         *
         * @return  self
         */ 
        public function setFechaCreacion($fechaCreacion)
        {
                $this->fechaCreacion = $fechaCreacion;

                return $this;
        }

        /**
         * Get the value of fechaModificacion
         */ 
        public function getFechaModificacion()
        {
                return $this->fechaModificacion;
        }

        /**
         * Set the value of fechaModificacion
         *
         * @return  self
         */ 
        public function setFechaModificacion($fechaModificacion)
        {
                $this->fechaModificacion = $fechaModificacion;

                return $this;
        }

        /**
         * Get the value of idUsuarioCreacion
         */ 
        public function getIdUsuarioCreacion()
        {
                return $this->idUsuarioCreacion;
        }

        /**
         * Set the value of idUsuarioCreacion
         *
         * @return  self
         */ 
        public function setIdUsuarioCreacion($idUsuarioCreacion)
        {
                $this->idUsuarioCreacion = $idUsuarioCreacion;

                return $this;
        }

        /**
         * Get the value of idUsuarioModificacion
         */ 
        public function getIdUsuarioModificacion()
        {
                return $this->idUsuarioModificacion;
        }

        /**
         * Set the value of idUsuarioModificacion
         *
         * @return  self
         */ 
        public function setIdUsuarioModificacion($idUsuarioModificacion)
        {
                $this->idUsuarioModificacion = $idUsuarioModificacion;

                return $this;
        }
    }

?>