<?php

    class Prestamos{
        private $idPrestamo;
        private $idBibliotecaCabecera;
        private $idTercero;
        private $idTipoServicio;
        private $fechaSalida;
        private $fechaVencimiento;
        private $fechaEntrada;
        private $numeroEjemplar;
        private $fechaCreacion;
        private $fechaModificacion;
        private $idUsuarioCreacion;
        private $idUsuarioModificacion;

        /**
         * Get the value of idPrestamo
         */ 
        public function getIdPrestamo()
        {
                return $this->idPrestamo;
        }

        /**
         * Set the value of idPrestamo
         *
         * @return  self
         */ 
        public function setIdPrestamo($idPrestamo)
        {
                $this->idPrestamo = $idPrestamo;

                return $this;
        }

        /**
         * Get the value of idBibliotecaCabecera
         */ 
        public function getIdBibliotecaCabecera()
        {
                return $this->idBibliotecaCabecera;
        }

        /**
         * Set the value of idBibliotecaCabecera
         *
         * @return  self
         */ 
        public function setIdBibliotecaCabecera($idBibliotecaCabecera)
        {
                $this->idBibliotecaCabecera = $idBibliotecaCabecera;

                return $this;
        }

        /**
         * Get the value of idTercero
         */ 
        public function getIdTercero()
        {
                return $this->idTercero;
        }

        /**
         * Set the value of idTercero
         *
         * @return  self
         */ 
        public function setIdTercero($idTercero)
        {
                $this->idTercero = $idTercero;

                return $this;
        }

        /**
         * Get the value of idTipoServicio
         */ 
        public function getIdTipoServicio()
        {
                return $this->idTipoServicio;
        }

        /**
         * Set the value of idTipoServicio
         *
         * @return  self
         */ 
        public function setIdTipoServicio($idTipoServicio)
        {
                $this->idTipoServicio = $idTipoServicio;

                return $this;
        }

        /**
         * Get the value of fechaSalida
         */ 
        public function getFechaSalida()
        {
                return $this->fechaSalida;
        }

        /**
         * Set the value of fechaSalida
         *
         * @return  self
         */ 
        public function setFechaSalida($fechaSalida)
        {
                $this->fechaSalida = $fechaSalida;

                return $this;
        }

        /**
         * Get the value of fechaVencimiento
         */ 
        public function getFechaVencimiento()
        {
                return $this->fechaVencimiento;
        }

        /**
         * Set the value of fechaVencimiento
         *
         * @return  self
         */ 
        public function setFechaVencimiento($fechaVencimiento)
        {
                $this->fechaVencimiento = $fechaVencimiento;

                return $this;
        }

        /**
         * Get the value of fechaEntrada
         */ 
        public function getFechaEntrada()
        {
                return $this->fechaEntrada;
        }

        /**
         * Set the value of fechaEntrada
         *
         * @return  self
         */ 
        public function setFechaEntrada($fechaEntrada)
        {
                $this->fechaEntrada = $fechaEntrada;

                return $this;
        }

        /**
         * Get the value of numeroEjemplar
         */ 
        public function getNumeroEjemplar()
        {
                return $this->numeroEjemplar;
        }

        /**
         * Set the value of numeroEjemplar
         *
         * @return  self
         */ 
        public function setNumeroEjemplar($numeroEjemplar)
        {
                $this->numeroEjemplar = $numeroEjemplar;

                return $this;
        }

        /**
         * Get the value of fechaCreacion
         */ 
        public function getFechaCreacion()
        {
                return $this->fechaCreacion;
        }

        /**
         * Set the value of fechaCreacion
         *
         * @return  self
         */ 
        public function setFechaCreacion($fechaCreacion)
        {
                $this->fechaCreacion = $fechaCreacion;

                return $this;
        }

        /**
         * Get the value of fechaModificacion
         */ 
        public function getFechaModificacion()
        {
                return $this->fechaModificacion;
        }

        /**
         * Set the value of fechaModificacion
         *
         * @return  self
         */ 
        public function setFechaModificacion($fechaModificacion)
        {
                $this->fechaModificacion = $fechaModificacion;

                return $this;
        }

        /**
         * Get the value of idUsuarioCreacion
         */ 
        public function getIdUsuarioCreacion()
        {
                return $this->idUsuarioCreacion;
        }

        /**
         * Set the value of idUsuarioCreacion
         *
         * @return  self
         */ 
        public function setIdUsuarioCreacion($idUsuarioCreacion)
        {
                $this->idUsuarioCreacion = $idUsuarioCreacion;

                return $this;
        }

        /**
         * Get the value of idUsuarioModificacion
         */ 
        public function getIdUsuarioModificacion()
        {
                return $this->idUsuarioModificacion;
        }

        /**
         * Set the value of idUsuarioModificacion
         *
         * @return  self
         */ 
        public function setIdUsuarioModificacion($idUsuarioModificacion)
        {
                $this->idUsuarioModificacion = $idUsuarioModificacion;

                return $this;
        }
    }

?>