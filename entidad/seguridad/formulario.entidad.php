<?php

    class Formulario{
        private $idFormulario;
        private $idCarpeta;
        private $descripcion;
        private $ruta;
        private $icono;
        private $estado;
        private $fechaCreacion;
        private $fechaModificacion;
        private $idUsuarioCreacion;
        private $idUsuarioModificacion;
        


        /**
         * Get the value of idFormulario
         */ 
        public function getIdFormulario()
        {
                return $this->idFormulario;
        }

        /**
         * Set the value of idFormulario
         *
         * @return  self
         */ 
        public function setIdFormulario($idFormulario)
        {
                $this->idFormulario = $idFormulario;

                return $this;
        }

        /**
         * Get the value of idCarpeta
         */ 
        public function getIdCarpeta()
        {
                return $this->idCarpeta;
        }

        /**
         * Set the value of idCarpeta
         *
         * @return  self
         */ 
        public function setIdCarpeta($idCarpeta)
        {
                $this->idCarpeta = $idCarpeta;

                return $this;
        }

        /**
         * Get the value of descripcion
         */ 
        public function getDescripcion()
        {
                return $this->descripcion;
        }

        /**
         * Set the value of descripcion
         *
         * @return  self
         */ 
        public function setDescripcion($descripcion)
        {
                $this->descripcion = $descripcion;

                return $this;
        }

        /**
         * Get the value of ruta
         */ 
        public function getRuta()
        {
                return $this->ruta;
        }

        /**
         * Set the value of ruta
         *
         * @return  self
         */ 
        public function setRuta($ruta)
        {
                $this->ruta = $ruta;

                return $this;
        }

        /**
         * Get the value of icono
         */ 
        public function getIcono()
        {
                return $this->icono;
        }

        /**
         * Set the value of icono
         *
         * @return  self
         */ 
        public function setIcono($icono)
        {
                $this->icono = $icono;

                return $this;
        }

        /**
         * Get the value of estado
         */ 
        public function getEstado()
        {
                return $this->estado;
        }

        /**
         * Set the value of estado
         *
         * @return  self
         */ 
        public function setEstado($estado)
        {
                $this->estado = $estado;

                return $this;
        }

        /**
         * Get the value of fechaCreacion
         */ 
        public function getFechaCreacion()
        {
                return $this->fechaCreacion;
        }

        /**
         * Set the value of fechaCreacion
         *
         * @return  self
         */ 
        public function setFechaCreacion($fechaCreacion)
        {
                $this->fechaCreacion = $fechaCreacion;

                return $this;
        }

        /**
         * Get the value of fechaModificacion
         */ 
        public function getFechaModificacion()
        {
                return $this->fechaModificacion;
        }

        /**
         * Set the value of fechaModificacion
         *
         * @return  self
         */ 
        public function setFechaModificacion($fechaModificacion)
        {
                $this->fechaModificacion = $fechaModificacion;

                return $this;
        }

        /**
         * Get the value of idUsuarioCreacion
         */ 
        public function getIdUsuarioCreacion()
        {
                return $this->idUsuarioCreacion;
        }

        /**
         * Set the value of idUsuarioCreacion
         *
         * @return  self
         */ 
        public function setIdUsuarioCreacion($idUsuarioCreacion)
        {
                $this->idUsuarioCreacion = $idUsuarioCreacion;

                return $this;
        }

        /**
         * Get the value of idUsuarioModificacion
         */ 
        public function getIdUsuarioModificacion()
        {
                return $this->idUsuarioModificacion;
        }

        /**
         * Set the value of idUsuarioModificacion
         *
         * @return  self
         */ 
        public function setIdUsuarioModificacion($idUsuarioModificacion)
        {
                $this->idUsuarioModificacion = $idUsuarioModificacion;

                return $this;
        }
    }

?>