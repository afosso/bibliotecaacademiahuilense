<?php

    class RolUsuario{
        private $idRolUsuario;
        private $idRol;
        private $idUsuario;

        /**
         * Get the value of idRolUsuario
         */ 
        public function getIdRolUsuario()
        {
                return $this->idRolUsuario;
        }

        /**
         * Set the value of idRolUsuario
         *
         * @return  self
         */ 
        public function setIdRolUsuario($idRolUsuario)
        {
                $this->idRolUsuario = $idRolUsuario;

                return $this;
        }

        /**
         * Get the value of idRol
         */ 
        public function getIdRol()
        {
                return $this->idRol;
        }

        /**
         * Set the value of idRol
         *
         * @return  self
         */ 
        public function setIdRol($idRol)
        {
                $this->idRol = $idRol;

                return $this;
        }

        /**
         * Get the value of idUsuario
         */ 
        public function getIdUsuario()
        {
                return $this->idUsuario;
        }

        /**
         * Set the value of idUsuario
         *
         * @return  self
         */ 
        public function setIdUsuario($idUsuario)
        {
                $this->idUsuario = $idUsuario;

                return $this;
        }
    }

?>