<?php

    class RolFormulario{
        private $idRolFormulario;
        private $idRol;
        private $idFormulario;

        

        /**
         * Get the value of idRolFormulario
         */ 
        public function getIdRolFormulario()
        {
                return $this->idRolFormulario;
        }

        /**
         * Set the value of idRolFormulario
         *
         * @return  self
         */ 
        public function setIdRolFormulario($idRolFormulario)
        {
                $this->idRolFormulario = $idRolFormulario;

                return $this;
        }

        /**
         * Get the value of idRol
         */ 
        public function getIdRol()
        {
                return $this->idRol;
        }

        /**
         * Set the value of idRol
         *
         * @return  self
         */ 
        public function setIdRol($idRol)
        {
                $this->idRol = $idRol;

                return $this;
        }

        /**
         * Get the value of idFormulario
         */ 
        public function getIdFormulario()
        {
                return $this->idFormulario;
        }

        /**
         * Set the value of idFormulario
         *
         * @return  self
         */ 
        public function setIdFormulario($idFormulario)
        {
                $this->idFormulario = $idFormulario;

                return $this;
        }
    }

?>