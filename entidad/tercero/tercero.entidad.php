<?php

    class Tercero{
        private $idTercero;
        private $idTipoDocumento;
        private $idMunicipioResidencia;
        private $nit;
        private $nombres;
        private $apellidos;
        private $genero;
        private $direccion;
        private $estado;
        private $fechaCreacion;
        private $fechaModificacion;
        private $idUsuarioCreacion;
        private $idUsuarioModificacion;

        /**
         * Get the value of idTercero
         */ 
        public function getIdTercero()
        {
                return $this->idTercero;
        }

        /**
         * Set the value of idTercero
         *
         * @return  self
         */ 
        public function setIdTercero($idTercero)
        {
                $this->idTercero = $idTercero;

                return $this;
        }

        /**
         * Get the value of idTipoDocumento
         */ 
        public function getIdTipoDocumento()
        {
                return $this->idTipoDocumento;
        }

        /**
         * Set the value of idTipoDocumento
         *
         * @return  self
         */ 
        public function setIdTipoDocumento($idTipoDocumento)
        {
                $this->idTipoDocumento = $idTipoDocumento;

                return $this;
        }

        /**
         * Get the value of idMunicipioResidencia
         */ 
        public function getIdMunicipioResidencia()
        {
                return $this->idMunicipioResidencia;
        }

        /**
         * Set the value of idMunicipioResidencia
         *
         * @return  self
         */ 
        public function setIdMunicipioResidencia($idMunicipioResidencia)
        {
                $this->idMunicipioResidencia = $idMunicipioResidencia;

                return $this;
        }

        /**
         * Get the value of nit
         */ 
        public function getNit()
        {
                return $this->nit;
        }

        /**
         * Set the value of nit
         *
         * @return  self
         */ 
        public function setNit($nit)
        {
                $this->nit = $nit;

                return $this;
        }

        /**
         * Get the value of nombres
         */ 
        public function getNombres()
        {
                return $this->nombres;
        }

        /**
         * Set the value of nombres
         *
         * @return  self
         */ 
        public function setNombres($nombres)
        {
                $this->nombres = $nombres;

                return $this;
        }

        /**
         * Get the value of apellidos
         */ 
        public function getApellidos()
        {
                return $this->apellidos;
        }

        /**
         * Set the value of apellidos
         *
         * @return  self
         */ 
        public function setApellidos($apellidos)
        {
                $this->apellidos = $apellidos;

                return $this;
        }

        /**
         * Get the value of genero
         */ 
        public function getGenero()
        {
                return $this->genero;
        }

        /**
         * Set the value of genero
         *
         * @return  self
         */ 
        public function setGenero($genero)
        {
                $this->genero = $genero;

                return $this;
        }

        /**
         * Get the value of direccion
         */ 
        public function getDireccion()
        {
                return $this->direccion;
        }

        /**
         * Set the value of direccion
         *
         * @return  self
         */ 
        public function setDireccion($direccion)
        {
                $this->direccion = $direccion;

                return $this;
        }

        /**
         * Get the value of estado
         */ 
        public function getEstado()
        {
                return $this->estado;
        }

        /**
         * Set the value of estado
         *
         * @return  self
         */ 
        public function setEstado($estado)
        {
                $this->estado = $estado;

                return $this;
        }

        /**
         * Get the value of fechaCreacion
         */ 
        public function getFechaCreacion()
        {
                return $this->fechaCreacion;
        }

        /**
         * Set the value of fechaCreacion
         *
         * @return  self
         */ 
        public function setFechaCreacion($fechaCreacion)
        {
                $this->fechaCreacion = $fechaCreacion;

                return $this;
        }

        /**
         * Get the value of fechaModificacion
         */ 
        public function getFechaModificacion()
        {
                return $this->fechaModificacion;
        }

        /**
         * Set the value of fechaModificacion
         *
         * @return  self
         */ 
        public function setFechaModificacion($fechaModificacion)
        {
                $this->fechaModificacion = $fechaModificacion;

                return $this;
        }

        /**
         * Get the value of idUsuarioCreacion
         */ 
        public function getIdUsuarioCreacion()
        {
                return $this->idUsuarioCreacion;
        }

        /**
         * Set the value of idUsuarioCreacion
         *
         * @return  self
         */ 
        public function setIdUsuarioCreacion($idUsuarioCreacion)
        {
                $this->idUsuarioCreacion = $idUsuarioCreacion;

                return $this;
        }

        /**
         * Get the value of idUsuarioModificacion
         */ 
        public function getIdUsuarioModificacion()
        {
                return $this->idUsuarioModificacion;
        }

        /**
         * Set the value of idUsuarioModificacion
         *
         * @return  self
         */ 
        public function setIdUsuarioModificacion($idUsuarioModificacion)
        {
                $this->idUsuarioModificacion = $idUsuarioModificacion;

                return $this;
        }
    }

?>