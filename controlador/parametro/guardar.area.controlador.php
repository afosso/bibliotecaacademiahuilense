<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/parametro/area.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/parametro/area.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $area = new Area();
        $area->setCodigo($_POST["txtCodigo"]);
        $area->setDescripcion($_POST["txtDescripcion"]);
        $area->setEstado($_POST["ddlEstado"]);
        $area->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $area->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($area->getCodigo() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo código";
            echo json_encode($respuesta);
            return;
        }

        if($area->getDescripcion() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo descripción";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($area->getDescripcion(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "La descripción no debe tener carácteres especiales";
            echo json_encode($respuesta);
            return;
        }

        $modeloArea = new ModeloArea();
        $respuestaModelo = $modeloArea->GuardarArea($area);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>