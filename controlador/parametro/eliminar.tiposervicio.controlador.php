<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/parametro/tiposervicio.modelo.php';

    $idTipoServicio = $_POST["idTipoServicio"];
    if(filter_var($idTipoServicio, FILTER_VALIDATE_INT)){
        $modeloTipoServicio = new ModeloTipoServicio();
        $respuesta = $modeloTipoServicio->EliminarTipoServicio($idTipoServicio);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>