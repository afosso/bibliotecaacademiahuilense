<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/parametro/temasafines.modelo.php';

    $idTemasAfines = $_POST["idTemasAfines"];
    if(filter_var($idTemasAfines, FILTER_VALIDATE_INT)){
        $modeloTemasAfines = new ModeloTemasAfines();
        $respuesta = $modeloTemasAfines->EliminarTemasAfines($idTemasAfines);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>