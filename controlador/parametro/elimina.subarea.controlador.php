<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/parametro/subarea.modelo.php';

    $idSubArea = $_POST["idSubArea"];
    if(filter_var($idSubArea, FILTER_VALIDATE_INT)){
        $modeloSubArea = new ModeloSubArea();
        $respuesta = $modeloSubArea->EliminarSubArea($idSubArea);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>