<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/parametro/area.modelo.php';

    $idArea = $_POST["idArea"];
    if(filter_var($idArea, FILTER_VALIDATE_INT)){
        $modeloArea = new ModeloArea();
        $respuesta = $modeloArea->EliminarArea($idArea);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>