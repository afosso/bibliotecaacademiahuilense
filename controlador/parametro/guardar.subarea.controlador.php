<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/parametro/subarea.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/parametro/subarea.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $subarea = new SubArea();
        $subarea->setIdArea($_POST["ddlArea"]);
        $subarea->setCodigo($_POST["txtCodigo"]);
        $subarea->setDescripcion($_POST["txtDescripcion"]);
        $subarea->setEstado($_POST["ddlEstado"]);
        $subarea->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $subarea->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($subarea->getIdArea() == "-1"){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe seleccionar un area";
            echo json_encode($respuesta);
            return;
        }

        if($subarea->getCodigo() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo código";
            echo json_encode($respuesta);
            return;
        }

        if($subarea->getDescripcion() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo descripción";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($subarea->getDescripcion(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "La descripción no debe tener carácteres especiales";
            echo json_encode($respuesta);
            return;
        }

        $modeloArea = new ModeloSubArea();
        $respuestaModelo = $modeloArea->GuardarSubArea($subarea);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>