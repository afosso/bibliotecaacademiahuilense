<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/parametro/tipo.modelo.php';

    $idTipo = $_POST["idTipo"];
    if(filter_var($idTipo, FILTER_VALIDATE_INT)){
        $modeloTipo = new ModeloTipo();
        $respuesta = $modeloTipo->EliminarTipo($idTipo);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>