<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/parametro/tipo.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/parametro/tipo.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $tipo = new Tipo();
        $tipo->setCodigo($_POST["txtCodigo"]);
        $tipo->setDescripcion($_POST["txtDescripcion"]);
        $tipo->setEstado($_POST["ddlEstado"]);
        $tipo->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $tipo->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($tipo->getCodigo() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo código";
            echo json_encode($respuesta);
            return;
        }

        if($tipo->getDescripcion() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo descripción";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($tipo->getDescripcion(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "La descripción no debe tener carácteres especiales";
            echo json_encode($respuesta);
            return;
        }

        $modeloTipo = new ModeloTipo();
        $respuestaModelo = $modeloTipo->GuardarTipo($tipo);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>