<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/parametro/temasafines.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/parametro/temasafines.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $temasafines = new TemasAfines();
        $temasafines->setCodigo($_POST["txtCodigo"]);
        $temasafines->setDescripcion($_POST["txtDescripcion"]);
        $temasafines->setEstado($_POST["ddlEstado"]);
        $temasafines->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $temasafines->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($temasafines->getCodigo() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo código";
            echo json_encode($respuesta);
            return;
        }

        if($temasafines->getDescripcion() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo descripción";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($temasafines->getDescripcion(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "La descripción no debe tener carácteres especiales";
            echo json_encode($respuesta);
            return;
        }

        $modeloTemasAfines = new ModeloTemasAfines();
        $respuestaModelo = $modeloTemasAfines->GuardarTemasAfines($temasafines);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>