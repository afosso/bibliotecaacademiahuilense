<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/parametro/tiposervicio.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/parametro/tiposervicio.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $tiposervicio = new TipoServicio();
        $tiposervicio->setIdTipoServicio($_POST["hiddenIdTipoServicio"]);
        $tiposervicio->setCodigo($_POST["txtCodigo"]);
        $tiposervicio->setDescripcion($_POST["txtDescripcion"]);
        $tiposervicio->setEstado($_POST["ddlEstado"]);
        $tiposervicio->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $tiposervicio->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($tiposervicio->getCodigo() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo código";
            echo json_encode($respuesta);
            return;
        }

        if($tiposervicio->getDescripcion() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo descripción";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($tiposervicio->getDescripcion(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "La descripción no debe tener carácteres especiales";
            echo json_encode($respuesta);
            return;
        }

        $modeloTipoServicio = new ModeloTipoServicio();
        $respuestaModelo = $modeloTipoServicio->ModificarTipoServicio($tiposervicio);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>