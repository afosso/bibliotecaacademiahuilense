<?php

    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/parametro/subarea.modelo.php';

    if(count($_POST) > 0){
        $modeloSubArea = new ModeloSubArea();
        echo json_encode($modeloSubArea->ConsultarSubAreaPorArea($_POST["idArea"]));
    }else{
        $modeloSubArea = new ModeloSubArea();
        echo json_encode($modeloSubArea->ConsultarSubArea());
    }
    
?>