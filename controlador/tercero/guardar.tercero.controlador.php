<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/tercero/tercero.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/tercero/tercero.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $tercero = new Tercero();
        $tercero->setIdTipoDocumento($_POST["ddlTipoDocumento"]);
        $tercero->setIdMunicipioResidencia($_POST["ddlMunicipio"]);
        $tercero->setNit($_POST["txtNit"]);
        $tercero->setNombres($_POST["txtNombres"]);
        $tercero->setApellidos($_POST["txtApellidos"]);
        $tercero->setGenero($_POST["ddlGenero"]);
        $tercero->setDireccion($_POST["txtDireccion"]);
        $tercero->setEstado($_POST["ddlEstado"]);
        $tercero->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $tercero->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($tercero->getIdTipoDocumento() == "-1"){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe seleccionar un tipo de documento";
            echo json_encode($respuesta);
            return;
        }

        if($tercero->getIdMunicipioResidencia() == "-1"){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe seleccionar un municipio de residencia";
            echo json_encode($respuesta);
            return;
        }

        if($tercero->getNit() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe digitar un número de identificación";
            echo json_encode($respuesta);
            return;
        }

        if($tercero->getNombres() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe digitar un nombre";
            echo json_encode($respuesta);
            return;
        }

        if($tercero->getApellidos() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe digitar un apellido";
            echo json_encode($respuesta);
            return;
        }

        if($tercero->getGenero() == "-1"){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe seleccionar un género";
            echo json_encode($respuesta);
            return;
        }

        if($tercero->getDireccion() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe digitar una dirección";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($tercero->getIdTipoDocumento(), FILTER_VALIDATE_INT)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "El tipo de documento seleccionado no es de tipo entero";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($tercero->getIdMunicipioResidencia(), FILTER_VALIDATE_INT)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "El municipio seleccionado no es de tipo entero";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($tercero->getNit(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "El NIT digitado posee carácteres especiales";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($tercero->getNombres(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "El Nombre digitado posee carácteres especiales";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($tercero->getApellidos(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "El Apellido digitado posee carácteres especiales";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($tercero->getGenero(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "El Genero digitado posee carácteres especiales";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($tercero->getDireccion(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "La dirección digitada posee carácteres especiales";
            echo json_encode($respuesta);
            return;
        }   

        $modeloTercero = new ModeloTercero();
        $respuestaModelo = $modeloTercero->GuardarTercero($tercero);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>