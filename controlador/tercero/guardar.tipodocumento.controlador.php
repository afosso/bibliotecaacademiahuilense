<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/tercero/tipodocumento.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/tercero/tipodocumento.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $tipoDocumento = new TipoDocumento();
        $tipoDocumento->setCodigo($_POST["txtCodigo"]);
        $tipoDocumento->setDescripcion($_POST["txtDescripcion"]);
        $tipoDocumento->setEstado($_POST["ddlEstado"]);
        $tipoDocumento->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $tipoDocumento->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($tipoDocumento->getCodigo() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo código";
            echo json_encode($respuesta);
            return;
        }

        if($tipoDocumento->getDescripcion() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo descripción";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($tipoDocumento->getDescripcion(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "La descripción no debe tener carácteres especiales";
            echo json_encode($respuesta);
            return;
        }

        $modeloTipoDocumento = new ModeloTipoDocumento();
        $respuestaModelo = $modeloTipoDocumento->GuardarTipoDocumento($tipoDocumento);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>