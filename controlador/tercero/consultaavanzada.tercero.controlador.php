<?php

    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/tercero/tercero.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/tercero/tercero.entidad.php';

    $tercero = new Tercero();
    if(isset($_POST["txtNumeroIdentificacion"])){
        $tercero->setNit($_POST["txtNumeroIdentificacion"]);
    }

    if(isset($_POST["txtNombres"])){
        $tercero->setNombres($_POST["txtNombres"]);
    }

    if(isset($_POST["txtApellidos"])){
        $tercero->setApellidos($_POST["txtApellidos"]);
    }

    $modeloTercero = new ModeloTercero();
    echo json_encode($modeloTercero->ConsultarTerceroAvanzada($tercero));

?>