<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/tercero/tipodocumento.modelo.php';

    $idTipoDocumento = $_POST["idTipoDocumento"];
    if(filter_var($idTipoDocumento, FILTER_VALIDATE_INT)){
        $modeloTipoDocumento = new ModeloTipoDocumento();
        $respuesta = $modeloTipoDocumento->EliminarTipoDocumento($idTipoDocumento);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>