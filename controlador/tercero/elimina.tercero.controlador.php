<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/tercero/tercero.modelo.php';

    $idTercero = $_POST["idTercero"];
    if(filter_var($idTercero, FILTER_VALIDATE_INT)){
        $modeloTercero = new ModeloTercero();
        $respuesta = $modeloTercero->EliminarTercero($idTercero);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>