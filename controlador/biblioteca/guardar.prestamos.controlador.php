<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/biblioteca/prestamos.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/biblioteca/prestamos.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $prestamo = new Prestamos();
        $prestamo->setIdBibliotecaCabecera($_POST["hiddeIdBiblioteca"]);
        $prestamo->setIdTercero($_POST["hiddeIdTercero"]);
        $prestamo->setIdTipoServicio($_POST["ddlTipoServicio"]);
        $prestamo->setFechaSalida($_POST["txtFechaSalida"]);
        $prestamo->setFechaVencimiento($_POST["txtFechaVencimiento"]);
        $prestamo->setNumeroEjemplar($_POST["txtNumeroEjemplar"]);
        $prestamo->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $prestamo->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($prestamo->getIdBibliotecaCabecera() == "-1"){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe seleccionar el libro para el prestamo";
            echo json_encode($respuesta);
            return;
        }

        if($prestamo->getIdTercero() == "-1"){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe seleccionar una persona para realizar el prestamo";
            echo json_encode($respuesta);
            return;
        }

        if($prestamo->getIdTipoServicio() == "-1"){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe seleccionar el tipo de servicio";
            echo json_encode($respuesta);
            return;
        }

        if($prestamo->getFechaSalida() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe seleccionar una fecha de salida";
            echo json_encode($respuesta);
            return;
        }

        if($prestamo->getFechaVencimiento() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe seleccionar una fecha de vencimiento para el prestamo";
            echo json_encode($respuesta);
            return;
        }

        if($prestamo->getNumeroEjemplar() == "" || (int)$prestamo->getNumeroEjemplar() <= 0){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar la cantidad de ejemplares en el prestamo";
            echo json_encode($respuesta);
            return;
        }

        $modeloPrestamos = new ModeloPrestamos();
        $respuestaModelo = $modeloPrestamos->GuardarPrestamos($prestamo);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>