<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/biblioteca/prestamos.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/biblioteca/prestamos.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $prestamo = new Prestamos();
        $prestamo->setIdPrestamo($_POST["idPrestamo"]);
        $prestamo->setFechaEntrada($_POST["fecha"]);
        $prestamo->setIdUsuarioModificacion($_SESSION["idUsuario"]);

        $modeloPrestamos = new ModeloPrestamos();
        $respuestaModelo = $modeloPrestamos->ModificarPrestamos($prestamo);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>