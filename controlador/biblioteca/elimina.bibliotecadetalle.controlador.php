<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/biblioteca/bibliotecadetalle.modelo.php';

    $idBibliotecaDetalle = $_POST["idBibliotecaDetalle"];
    if(filter_var($idBibliotecaDetalle, FILTER_VALIDATE_INT)){
        $modeloBibliotecaDetalle = new ModeloBibliotecaDetalle();
        $respuesta = $modeloBibliotecaDetalle->EliminaDetalle($idBibliotecaDetalle);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>