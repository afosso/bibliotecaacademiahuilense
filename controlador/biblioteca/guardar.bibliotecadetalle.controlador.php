<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/biblioteca/bibliotecadetalle.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/biblioteca/bibliotecadetalle.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $bibliotecaDetalle = new BibliotecaDetalle();
        $bibliotecaDetalle->setIdBibliotecaCabecera($_POST["hiddenIdCabecera"]);
        $bibliotecaDetalle->setDescripcion($_POST["txtDescripcionPortada"]);
        $bibliotecaDetalle->setNumeroPagina($_POST["txtNumeroPagina"]);
        $bibliotecaDetalle->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $bibliotecaDetalle->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($bibliotecaDetalle->getDescripcion() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe digitar una descripción";
            echo json_encode($respuesta);
            return;
        }

        if($bibliotecaDetalle->getNumeroPagina() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe digitar un número de página";
            echo json_encode($respuesta);
            return;
        }

        $modeloBiblioteca = new ModeloBibliotecaDetalle();
        $respuestaModelo = $modeloBiblioteca->GuardarDetalle($bibliotecaDetalle);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>