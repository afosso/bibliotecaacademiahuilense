<?php

    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/biblioteca/bibliotecadetalle.modelo.php';

    if(isset($_POST["idDetalle"])){
        $modeloBibliotecaDetalle = new ModeloBibliotecaDetalle();
        echo json_encode($modeloBibliotecaDetalle->ConsultarDetallePorId($_POST["idDetalle"]));
    }else{
        $modeloBibliotecaDetalle = new ModeloBibliotecaDetalle();
        echo json_encode($modeloBibliotecaDetalle->ConsultarDetallePorIdCabecera($_POST["idCabecera"]));
    }

?>