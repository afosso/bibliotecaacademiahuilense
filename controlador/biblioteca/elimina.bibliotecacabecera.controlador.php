<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/biblioteca/bibliotecacabecera.modelo.php';

    $idBibliotecaCabecera = $_POST["idBibliotecaCabecera"];
    if(filter_var($idBibliotecaCabecera, FILTER_VALIDATE_INT)){
        $modeloBibliotecaCabecera = new ModeloBibliotecaCabecera();
        $respuesta = $modeloBibliotecaCabecera->EliminarCabecera($idBibliotecaCabecera);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>