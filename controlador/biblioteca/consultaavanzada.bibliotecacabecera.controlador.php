<?php

    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/biblioteca/bibliotecacabecera.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/biblioteca/bibliotecacabecera.entidad.php';

    $cabecera = new BibliotecaCabercera();
    if(isset($_POST["ddlTipo"])){
        $cabecera->setIdTipo($_POST["ddlTipo"]);
    }

    if(isset($_POST["ddlTemasAfines"])){
        $cabecera->setIdTemasAfines($_POST["ddlTemasAfines"]);
    }

    if(isset($_POST["txtCodigo"])){
        $cabecera->setCodigo($_POST["txtCodigo"]);
    }

    if(isset($_POST["txtDescripcion"])){
        $cabecera->setDescripcion($_POST["txtDescripcion"]);
    }

    if(isset($_POST["txtAutor"])){
        $cabecera->setAutor($_POST["txtAutor"]);
    }

    if(isset($_POST["txtAnio"])){
        $cabecera->setEjercicio($_POST["txtAnio"]);
    }

    if(isset($_POST["txtPalabrasClaves"])){
        $cabecera->setEjercicio($_POST["txtPalabrasClaves"]);
    }

    $modeloBibliotecaCabecera = new ModeloBibliotecaCabecera();
    echo json_encode($modeloBibliotecaCabecera->ConsultarCabeceraAvanzada($cabecera));

?>