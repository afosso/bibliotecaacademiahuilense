<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/biblioteca/prestamos.modelo.php';

    $idPrestamo = $_POST["idPrestamo"];
    if(filter_var($idPrestamo, FILTER_VALIDATE_INT)){
        $modeloPrestamos = new ModeloPrestamos();
        $respuesta = $modeloPrestamos->EliminarPrestamo($idPrestamo);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>