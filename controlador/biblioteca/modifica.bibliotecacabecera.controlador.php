<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/biblioteca/bibliotecacabecera.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/biblioteca/bibliotecacabecera.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $bibliotecaCabecera = new BibliotecaCabercera();
        $bibliotecaCabecera->setIdBibliotecaCabecera($_POST["hiddenIdCabecera"]);
        $bibliotecaCabecera->setIdTipo($_POST["ddlTipo"]);
        isset($_POST["ddlSubArea"]) ? $bibliotecaCabecera->setIdSubArea($_POST["ddlSubArea"]) : $bibliotecaCabecera->setIdSubArea("");
        $bibliotecaCabecera->setIdTemasAfines($_POST["ddlTemasAfines"]);
        $bibliotecaCabecera->setIdEditorial($_POST["ddlEditorial"]);
        $bibliotecaCabecera->setIdMunicipio($_POST["ddlMunicipio"]);
        $bibliotecaCabecera->setCodigo($_POST["txtCodigo"]);
        $bibliotecaCabecera->setDescripcion($_POST["txtDescripcion"]);
        $bibliotecaCabecera->setAutor($_POST["txtAutor"]);
        $bibliotecaCabecera->setEjercicio($_POST["txtEjercicio"]);
        $bibliotecaCabecera->setNumeroPaginas($_POST["txtNumeroPaginas"]);
        $bibliotecaCabecera->setNumeroEjemplar($_POST["txtNumeroEjemplares"]);
        $bibliotecaCabecera->setTipoAdquisicion($_POST["ddlTipoAdquisicion"]);
        $bibliotecaCabecera->setDescripcionAdquisicion($_POST["txtDescripcionAdquisicion"]);
        $bibliotecaCabecera->setFechaIngreso($_POST["txtFechaIngreso"]);
        $bibliotecaCabecera->setPrecio($_POST["txtPrecio"]);
        $bibliotecaCabecera->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $bibliotecaCabecera->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($bibliotecaCabecera->getIdTipo() == "-1"){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe seleccionar un tipo";
            echo json_encode($respuesta);
            return;
        }

        if($bibliotecaCabecera->getIdMunicipio() == "-1"){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe seleccionar un municipio";
            echo json_encode($respuesta);
            return;
        }

        if($bibliotecaCabecera->getCodigo() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe digitar un código";
            echo json_encode($respuesta);
            return;
        }

        if($bibliotecaCabecera->getDescripcion() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe digitar una descripcion";
            echo json_encode($respuesta);
            return;
        }

        if($bibliotecaCabecera->getAutor() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe digitar un autor";
            echo json_encode($respuesta);
            return;
        }

        if($bibliotecaCabecera->getNumeroPaginas() == "" || (Int)$bibliotecaCabecera->getNumeroPaginas() <= 0){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar el número de páginas";
            echo json_encode($respuesta);
            return;
        }

        if($bibliotecaCabecera->getNumeroEjemplar() == "" || (Int)$bibliotecaCabecera->getNumeroEjemplar() <= 0){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar el número de ejemplares";
            echo json_encode($respuesta);
            return;
        }

        if($bibliotecaCabecera->getFechaIngreso() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe seleccionar una fecha";
            echo json_encode($respuesta);
            return;
        }

        if($bibliotecaCabecera->getPrecio() == "" || (Int)$bibliotecaCabecera->getPrecio() < 0){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe indicar el precio del libro";
            echo json_encode($respuesta);
            return;
        }

        $modeloBiblioteca = new ModeloBibliotecaCabecera();
        $respuestaModelo = $modeloBiblioteca->ModificarCabecera($bibliotecaCabecera);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>