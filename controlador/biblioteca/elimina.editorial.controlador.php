<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/biblioteca/editorial.modelo.php';

    $idEditorial = $_POST["idEditorial"];
    if(filter_var($idEditorial, FILTER_VALIDATE_INT)){
        $modeloEditorial = new ModeloEditorial();
        $respuesta = $modeloEditorial->EliminarEditorial($idEditorial);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>