<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/ubicacion/pais.modelo.php';

    $idPais = $_POST["idPais"];
    if(filter_var($idPais, FILTER_VALIDATE_INT)){
        $modeloPais = new ModeloPais();
        $respuesta = $modeloPais->EliminarPais($idPais);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>