<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/ubicacion/municipio.modelo.php';

    $idMunicipio = $_POST["idMunicipio"];
    if(filter_var($idMunicipio, FILTER_VALIDATE_INT)){
        $modeloMunicipio = new ModeloMunicipio();
        $respuesta = $modeloMunicipio->EliminarMunicipio($idMunicipio);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>