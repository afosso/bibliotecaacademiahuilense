<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/ubicacion/departamento.modelo.php';

    $idDepartamento = $_POST["idDepartamento"];
    if(filter_var($idDepartamento, FILTER_VALIDATE_INT)){
        $modeloDepartamento = new ModeloDepartamento();
        $respuesta = $modeloDepartamento->EliminarDepartamento($idDepartamento);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>