<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/ubicacion/departamento.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/ubicacion/departamento.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $departamento = new Departamento();
        $departamento->setIdPais($_POST["ddlPais"]);
        $departamento->setCodigoDane($_POST["txtCodigoDane"]);
        $departamento->setDescripcion($_POST["txtDescripcion"]);
        $departamento->setEstado($_POST["ddlEstado"]);
        $departamento->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $departamento->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($departamento->getIdPais() == "-1"){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe seleccionar un PAIS";
            echo json_encode($respuesta);
            return;
        }

        if($departamento->getCodigoDane() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar el codigo DANE";
            echo json_encode($respuesta);
            return;
        }

        if($departamento->getDescripcion() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo descripción";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($departamento->getCodigoDane(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "El codigo DANE no debe contener valores especiales";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($departamento->getDescripcion(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "La descripción no debe tener carácteres especiales";
            echo json_encode($respuesta);
            return;
        }

        $modeloDepartamento = new ModeloDepartamento();
        $respuestaModelo = $modeloDepartamento->GuardarDepartamento($departamento);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>