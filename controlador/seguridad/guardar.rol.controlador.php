<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/seguridad/rol.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/seguridad/rol.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $rol = new Rol();
        $rol->setCodigo($_POST["txtCodigo"]);
        $rol->setDescripcion($_POST["txtDescripcion"]);
        $rol->setEstado($_POST["ddlEstado"]);
        $rol->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $rol->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($rol->getCodigo() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo nombre de usuario";
            echo json_encode($respuesta);
            return;
        }

        if($rol->getDescripcion() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo descripción del usuario";
            echo json_encode($respuesta);
            return;
        }

        if(!filter_var($rol->getDescripcion(), FILTER_SANITIZE_STRING)){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "La descripción no debe tener carácteres especiales";
            echo json_encode($respuesta);
            return;
        }

        $modeloRol = new ModeloRol();
        $respuestaModelo = $modeloRol->GuardarRol($rol);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>