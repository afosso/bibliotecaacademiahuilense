<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/seguridad/usuario.modelo.php';

    if(isset($_POST)){
        $usuario = $_POST["txtUsuario"];
        $contrasenia = $_POST["txtContrasenia"];

        if($usuario != "" && $contrasenia != ""){

            $modeloUsuario = new ModeloUsuario();
            $resultado = $modeloUsuario->IniciarSesion($usuario);

            if($resultado != null){
                if($resultado->password != md5($contrasenia)){
                    echo "La contraseña es incorrecta";
                    return;
                }
                if($resultado->fechaActivacion > date('Y-m-d')){
                    echo "El usuario no se encuentra activo";
                    return;
                }

                if($resultado->fechaExpiracion < date('Y-m-d')){
                    echo "El usuario no se encuentra activo";
                    return;
                }

                $_SESSION["iniciarSesion"] = "ok";
                $_SESSION["fotoUser"] = $resultado->fotoUrl;
                $_SESSION["nombreUsuario"] = $resultado->descripcion;
                $_SESSION["administrador"] = $resultado->administrador;
                $_SESSION["idUsuario"] = $resultado->idUsuario;
                
                echo "OK";
            }else{
                echo "No se encontró usuario";
            }
        }else{
            echo "No se ingresaron los datos completos, por favor verifique.";
        }
    }

?>