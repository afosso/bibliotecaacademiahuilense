<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/seguridad/rol.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/seguridad/rol.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $rol = new Rol();
        $rol->setIdRol($_POST["hiddenIdRol"]);
        $rol->setCodigo($_POST["txtCodigo"]);
        $rol->setDescripcion($_POST["txtDescripcion"]);
        $rol->setEstado($_POST["ddlEstado"]);
        $rol->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $rol->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($rol->getCodigo() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo código";
            echo json_encode($respuesta);
            return;
        }

        if($rol->getDescripcion() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo descripción";
            echo json_encode($respuesta);
            return;
        }

        $modeloRol = new ModeloRol();
        $respuestaModelo = $modeloRol->ModificarRol($rol);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>