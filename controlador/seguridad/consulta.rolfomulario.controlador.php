<?php

    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/seguridad/rolformulario.modelo.php';

    $idRol = $_POST["idRol"];

    if(filter_var($idRol, FILTER_VALIDATE_INT)){
        $modeloRolFormulario = new ModeloRolFormulario();
        echo json_encode($modeloRolFormulario->ConsultarRolFormularioPorIdRol($idRol));
    }
?>