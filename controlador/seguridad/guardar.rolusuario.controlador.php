<?php

    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/seguridad/rolusuario.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/seguridad/rolusuario.entidad.php';

    $rolUsuario = new RolUsuario();
    $rolUsuario->setIdRol($_POST["idRol"]);
    $rolUsuario->setIdUsuario($_POST["idUsuario"]);
    $valor = $_POST["valor"];

    $modeloRolUsuario = new ModeloRolUsuario();
    $respuesta = "error";
    if($valor == "true"){
        $respuesta = $modeloRolUsuario->GuardarRolUsuario($rolUsuario);
    }else{
        $respuesta = $modeloRolUsuario->EliminarRolUsuario($rolUsuario);
    }

    echo json_encode($respuesta);
?>