<?php

    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/seguridad/rolformulario.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/seguridad/rolformulario.entidad.php';

    $rolFormulario = new RolFormulario();
    $rolFormulario->setIdRol($_POST["idRol"]);
    $rolFormulario->setIdFormulario($_POST["idFormulario"]);
    $valor = $_POST["valor"];

    $modeloRolFormulario = new ModeloRolFormulario();
    $respuesta = "error";
    if($valor == "true"){
        $respuesta = $modeloRolFormulario->GuardarRolFormulario($rolFormulario);
    }else{
        $respuesta = $modeloRolFormulario->EliminarRolFormulario($rolFormulario);
    }

    echo json_encode($respuesta);
?>