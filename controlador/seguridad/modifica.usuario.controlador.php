<?php

    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/seguridad/usuario.modelo.php';
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/entidad/seguridad/usuario.entidad.php';

    $respuesta = array(
        "status" => "",
        "mensaje" => ""
    );

    if(isset($_POST)){
        $usuario = new Usuario();
        $usuario->setIdUsuario($_POST["idUsuario"]);
        $usuario->setCodigo($_POST["NombreUsuario"]);
        $usuario->setDescripcion($_POST["DescripcionUsuario"]);
        $usuario->setPassword(md5($_POST["Contrasenia"]));
        $usuario->setFechaActivacion($_POST["FechaActivacion"]);
        $usuario->setFechaExpiracion($_POST["FechaExpiracion"]);
        $usuario->setAdministrador($_POST["Administrador"]);
        $usuario->setFotoUrl($_POST["Imagen"]);
        $usuario->setIdUsuarioModificacion($_SESSION["idUsuario"]);
        $usuario->setIdUsuarioCreacion($_SESSION["idUsuario"]);

        if($usuario->getCodigo() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo nombre de usuario";
            echo json_encode($respuesta);
            return;
        }

        if($usuario->getDescripcion() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo descripción del usuario";
            echo json_encode($respuesta);
            return;
        }

        if($usuario->getPassword() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo contraseña";
            echo json_encode($respuesta);
            return;
        }

        if($usuario->getFechaActivacion() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo Fecha de Activación";
            echo json_encode($respuesta);
            return;
        }

        if($usuario->getFechaExpiracion() == ""){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "Debe ingresar un valor al campo fecha de expiración";
            echo json_encode($respuesta);
            return;
        }

        if($usuario->getFechaActivacion() > $usuario->getFechaExpiracion()){
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = "La fecha de activación no debe ser mayor a la fecha de expiración";
            echo json_encode($respuesta);
            return;
        }

        $modeloUsuario = new ModeloUsuario();
        $respuestaModelo = $modeloUsuario->ModificarUsuario($usuario);
        if($respuestaModelo == "OK"){
            $respuesta["status"] = "OK";
            echo json_encode($respuesta);
        }else{
            $respuesta["status"] = "Error";
            $respuesta["mensaje"] = $respuestaModelo;
            echo json_encode($respuesta);
        }
    }

?>