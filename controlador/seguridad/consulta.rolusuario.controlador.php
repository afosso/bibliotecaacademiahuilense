<?php

    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/seguridad/rolusuario.modelo.php';

    $idUsuario = $_POST["idUsuario"];

    if(filter_var($idUsuario, FILTER_VALIDATE_INT)){
        $modeloRolUsuario = new ModeloRolUsuario();
        echo json_encode($modeloRolUsuario->ConsultarRolUsuarioPorIdUsuario($idUsuario));
    }
?>