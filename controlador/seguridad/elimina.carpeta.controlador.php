<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/seguridad/carpeta.modelo.php';

    $idCarpeta = $_POST["idCarpeta"];
    if(filter_var($idCarpeta, FILTER_VALIDATE_INT)){
        $modeloCarpeta = new ModeloCarpeta();
        $respuesta = $modeloCarpeta->EliminarCarpeta($idCarpeta);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>