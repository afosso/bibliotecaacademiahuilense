<?php
    session_start();
    require_once str_replace("\\", "/", dirname(__DIR__, 2)) . '/modelo/seguridad/formulario.modelo.php';

    $idFormulario = $_POST["idFormulario"];
    if(filter_var($idFormulario, FILTER_VALIDATE_INT)){
        $modeloFormulario = new ModeloFormulario();
        $respuesta = $modeloFormulario->EliminarFormulario($idFormulario);
        echo json_encode($respuesta);
    }else{
        echo json_encode("El valor asignado no es numerico");
    }


?>