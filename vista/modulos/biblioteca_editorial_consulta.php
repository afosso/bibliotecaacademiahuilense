<script src="<?php echo $url; ?>/js/biblioteca/editorial_consulta.js"></script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Tablero
        <small>Panel de control</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="active">Tablero</a></li>
      </ol>
    </section>

    <section class="content">
        <form method="POST" id="form">
          
          <div class="box box-primary">
              <div class="box-header with-border">
                <h4 class="box-title">Lista de editoriales</h4>
              </div>
              <div class="box-body">
                <table id="datos" class="table table-bordered table-striped dt-responsive tabla">
                </table>
              </div>
              <div class="box-footer">
              </div>
          </div>
        </form>
    </section>

  </div>