
<?php 
    require_once str_replace("\\", "/", dirname(__DIR__,2)) . '/entorno/conexion.php';
    $idBibliotecaCabecera = $_GET["parametro"]; 
    $resultado;
    if(!filter_var($idBibliotecaCabecera, FILTER_VALIDATE_INT)){
        die("No juegues con los valores");
    }else{
        $sql = "SELECT bc.*, m.idDepartamento, sa.idArea FROM 
            biblioteca_bibliotecacabecera bc
            INNER JOIN ubicacion_municipio m ON m.idMunicipio = bc.idMunicipio
            LEFT JOIN parametro_subarea sa ON sa.idSubArea = bc.idSubArea
            WHERE bc.idBibliotecaCabecera = $idBibliotecaCabecera";
        $conexion = new Conexion();
        $stmt = $conexion->prepare($sql);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_OBJ);
    }

?>
<script src="<?php echo $url; ?>/js/biblioteca/bibliotecacabecera_modifica.js"></script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Tablero
        <small>Panel de control</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="active">Tablero</a></li>
      </ol>
    </section>

    <section class="content">
        <?php if($resultado == null){die("<div class='alert alert-info'>No existen datos con el valor asignado<div>");} ?>
      <form method="POST" id="form">
          <h4 class="box-title">Actualizar Libro</h4>
          <div class="box box-primary">
                <div class="box-header with-border">
                    <button class="btn btn-primary modificar" type="submit"><i class="fa fa-edit"></i> Modificar</button>
                </div>
                <div class="box-body">
                    <input type="hidden" name="hiddenIdCabecera" id="hiddenIdCabecera" value="<?php echo $resultado->idBibliotecaCabecera; ?>">

                    <div class="row">
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="ddlTipo" class="control-label">Tipo</label>
                            <select name="ddlTipo" id="ddlTipo" class="form-control">
                                <option value="-1">--Seleccione una opción--</option>
                                <?php
                                    $stmt = $conexion->prepare("SELECT * FROM parametro_tipo");
                                    $stmt->execute();
                                    $tipo = $stmt->fetchAll(PDO::FETCH_OBJ);
                                    foreach ($tipo as $key => $value) {
                                        $seleccionado = "";
                                        if($value->idTipo == $resultado->idTipo){
                                            $seleccionado = "selected=''";
                                        }else{
                                            $seleccionado = "";
                                        }

                                        echo '<option value="' . $value->idTipo . '" ' . $seleccionado . '>' . $value->descripcion . '</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="ddlArea" class="control-label">Area</label>
                            <select name="ddlArea" id="ddlArea" class="form-control" onchange="ConsultarSubAreaPorArea();">
                                <option value="-1">--Seleccione una opción--</option>
                                <?php
                                    $stmt = $conexion->prepare("SELECT * FROM parametro_area");
                                    $stmt->execute();
                                    $area = $stmt->fetchAll(PDO::FETCH_OBJ);
                                    foreach ($area as $key => $value) {
                                        $seleccionado = "";
                                        if($value->idArea == $resultado->idArea){
                                            $seleccionado = "selected=''";
                                        }else{
                                            $seleccionado = "";
                                        }

                                        echo '<option value="' . $value->idArea . '" ' . $seleccionado . '>' . $value->descripcion . '</option>';
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="ddlSubArea" class="control-label">Sub Area</label>
                            <select name="ddlSubArea" id="ddlSubArea" class="form-control">
                                <option value="-1">--Seleccione una opción--</option>
                                <?php
                                    $stmt = $conexion->prepare("SELECT * FROM parametro_subarea WHERE idArea = :idArea");
                                    $stmt->bindValue(":idArea", $resultado->idArea, PDO::PARAM_INT);
                                    $stmt->execute();
                                    $subarea = $stmt->fetchAll(PDO::FETCH_OBJ);
                                    foreach ($subarea as $key => $value) {
                                        $seleccionado = "";
                                        if($value->idSubArea == $resultado->idSubArea){
                                            $seleccionado = "selected=''";
                                        }else{
                                            $seleccionado = "";
                                        }

                                        echo '<option value="' . $value->idSubArea . '" ' . $seleccionado . '>' . $value->descripcion . '</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="ddlTemasAfines" class="control-label">Temas Afines</label>
                            <select name="ddlTemasAfines" id="ddlTemasAfines" class="form-control">
                                <option value="-1">--Seleccione una opción--</option>
                                <?php
                                    $stmt = $conexion->prepare("SELECT * FROM parametro_temasafines");
                                    $stmt->execute();
                                    $temasAfiones = $stmt->fetchAll(PDO::FETCH_OBJ);
                                    foreach ($temasAfiones as $key => $value) {
                                        $seleccionado = "";
                                        if($value->idTemasAfines == $resultado->idTemasAfines){
                                            $seleccionado = "selected=''";
                                        }else{
                                            $seleccionado = "";
                                        }

                                        echo '<option value="' . $value->idTemasAfines . '" ' . $seleccionado . '>' . $value->descripcion . '</option>';
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="ddlEditorial" class="control-label">Editorial</label>
                            <select name="ddlEditorial" id="ddlEditorial" class="form-control">
                                <option value="-1">--Seleccione una opción--</option>
                                <?php
                                    $stmt = $conexion->prepare("SELECT * FROM biblioteca_editorial");
                                    $stmt->execute();
                                    $editorial = $stmt->fetchAll(PDO::FETCH_OBJ);
                                    foreach ($editorial as $key => $value) {
                                        $seleccionado = "";
                                        if($value->idEditorial == $resultado->idEditorial){
                                            $seleccionado = "selected=''";
                                        }else{
                                            $seleccionado = "";
                                        }

                                        echo '<option value="' . $value->idEditorial . '" ' . $seleccionado . '>' . $value->descripcion . '</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    </div> 

                    <div class="row">
                        <div class="form-group  col-sm-6 col-lg-6">
                            <label for="ddlDepartamento" class="control-label">Departamento</label>
                            <select name="ddlDepartamento" id="ddlDepartamento" class="form-control" onchange="ConsultarMunicipio();">
                                <option value="-1">--Seleccione una opción--</option>
                                <?php
                                    $stmt = $conexion->prepare("SELECT * FROM ubicacion_departamento");
                                    $stmt->execute();
                                    $departamento = $stmt->fetchAll(PDO::FETCH_OBJ);
                                    foreach ($departamento as $key => $value) {
                                        $seleccionado = "";
                                        if($value->idDepartamento == $resultado->idDepartamento){
                                            $seleccionado = "selected=''";
                                        }else{
                                            $seleccionado = "";
                                        }

                                        echo '<option value="' . $value->idDepartamento . '" ' . $seleccionado . '>' . $value->descripcion . '</option>';
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="ddlMunicipio" class="control-label">Municipio</label>
                            <select name="ddlMunicipio" id="ddlMunicipio" class="form-control">
                                <option value="-1">--Seleccione una opción--</option>
                                <?php
                                    $stmt = $conexion->prepare("SELECT * FROM ubicacion_municipio WHERE idDepartamento = :idDepartamento");
                                    $stmt->bindValue(":idDepartamento", $resultado->idDepartamento, PDO::PARAM_INT);
                                    $stmt->execute();
                                    $municipio = $stmt->fetchAll(PDO::FETCH_OBJ);
                                    foreach ($municipio as $key => $value) {
                                        $seleccionado = "";
                                        if($value->idMunicipio == $resultado->idMunicipio){
                                            $seleccionado = "selected=''";
                                        }else{
                                            $seleccionado = "";
                                        }

                                        echo '<option value="' . $value->idMunicipio . '" ' . $seleccionado . '>' . $value->descripcion . '</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="txtCodigo" class="control-label">Código</label>
                            <input type="text" name="txtCodigo" id="txtCodigo" class="form-control" required value="<?php echo $resultado->codigo; ?>">
                        </div>
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="txtDescripcion" class="control-label">Descripción</label>
                            <input type="text" name="txtDescripcion" id="txtDescripcion" class="form-control" required value="<?php echo $resultado->descripcion ?>">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="txtAutor" class="control-label">Autor</label>
                            <input type="text" name="txtAutor" id="txtAutor" class="form-control" required value="<?php echo $resultado->autor; ?>">
                        </div>
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="txtEjercicio" class="control-label">Año</label>
                            <input type="number" name="txtEjercicio" id="txtEjercicio" class="form-control" value="<?php echo $resultado->ejercicio; ?>">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="txtNumeroPaginas" class="control-label">Número Páginas</label>
                            <input type="number" name="txtNumeroPaginas" id="txtNumeroPaginas" class="form-control" required value="<?php echo $resultado->numeroPaginas; ?>">
                        </div>
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="txtNumeroEjemplares" class="control-label">Número Ejemplares</label>
                            <input type="number" name="txtNumeroEjemplares" id="txtNumeroEjemplares" class="form-control" required value="<?php echo $resultado->numeroEjemplar ?>">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="ddlTipoAdquisicion" class="control-label">Tipo de Adquisición</label>
                            <select name="ddlTipoAdquisicion" id="ddlTipoAdquisicion" class="form-control">
                                <option value="-1">--Seleccione una opción--</option>
                                <option value="Comprado A" <?php if($resultado->tipoAdquisicion == 'Comprado A'){ echo "selected=''"; } ?>>Comprado A</option>
                                <option value="Donado Por" <?php if($resultado->tipoAdquisicion == 'Donado Por'){ echo "selected=''"; } ?>>Donado Por</option>
                            </select>
                        </div>
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="txtDescripcionAdquisicion" class="control-label">Descripción Adquisición</label>
                            <input type="text" name="txtDescripcionAdquisicion" id="txtDescripcionAdquisicion" class="form-control" value="<?php echo $resultado->descripcionAdquisicion; ?>">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="txtFechaIngreso" class="control-label">Fecha de Ingreso</label>
                            <input type="text" name="txtFechaIngreso" id="txtFechaIngreso" class="form-control" required value="<?php echo $resultado->fechaIngreso; ?>">
                        </div>
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="txtPrecio" class="control-label">Precio</label>
                            <input type="number" name="txtPrecio" id="txtPrecio" class="form-control" required value="<?php echo $resultado->precio; ?>">
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button class="btn btn-primary modificar" type="submit"><i class="fa fa-edit"></i> Modificar</button>
                </div>
          </div>
        </form>
    </section>

  </div>