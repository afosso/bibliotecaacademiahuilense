<script src="<?php echo $url; ?>/js/biblioteca/bibliotecacabecera_consulta.js"></script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Tablero
        <small>Panel de control</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="active">Tablero</a></li>
      </ol>
    </section>

    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
            <h4 class="box-title">Lista de Libros</h4>
            </div>
            <div class="box-body">
            <table id="datos" class="table table-bordered table-striped dt-responsive tabla">
            </table>
            </div>
            <div class="box-footer">
            </div>
        </div>

        <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Agregar portada</h4>
                </div>

                <div class="modal-body">
                    <form method="POST" id="form">
                        <script src="<?php echo $url; ?>/js/biblioteca/bibliotecadetalle_guardar.js"></script>
                        <input type="hidden" name="hiddenIdCabecera" id="hiddenIdCabecera">
                        <div class="form-group col-md-12">
                            <label for="txtDescripcionPortada" class="control-label">Descripción</label>
                            <input type="text" class="form-control" name="txtDescripcionPortada" id="txtDescripcionPortada">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="txtNumeroPagina" class="control-label">Número de página</label>
                            <input type="number" class="form-control" name="txtNumeroPagina" id="txtNumeroPagina">
                        </div>
                        <div class="row">
                            <button type="submit" class="btn btn-primary pull-right guardar"><i class="fa fa-edit"></i> Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
          </div>
        </div>


        <div class="modal fade" id="modal-listado-paginas">
          <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Listado de portadas</h4>
                </div>

                <div class="modal-body">
                    <table id="datosPaginas" class="table table-bordered table-striped dt-responsive tabla">
                    </table>
                </div>
            </div>
          </div>
        </div>


        <div class="modal fade" id="modal-editar-pagina">
          <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Modificar portada</h4>
                </div>

                <div class="modal-body">
                    <form method="POST" id="formModifica">
                        <script src="<?php echo $url; ?>/js/biblioteca/bibliotecadetalle_modifica.js"></script>
                        <input type="hidden" name="hiddenIdDetalle" id="hiddenIdDetalle">
                        <div class="form-group col-md-12">
                            <label for="txtDescripcionPortadaDetalle" class="control-label">Descripción</label>
                            <input type="text" class="form-control" name="txtDescripcionPortadaDetalle" id="txtDescripcionPortadaDetalle">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="txtNumeroPaginaDetalle" class="control-label">Número de página</label>
                            <input type="number" class="form-control" name="txtNumeroPaginaDetalle" id="txtNumeroPaginaDetalle">
                        </div>
                        <div class="row">
                            <button type="submit" class="btn btn-primary pull-right modificar"><i class="fa fa-edit"></i> Modificar</button>
                        </div>
                    </form>
                </div>
            </div>
          </div>
        </div>
    </section>

  </div>