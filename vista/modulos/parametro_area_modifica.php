<?php 
    require_once str_replace("\\", "/", dirname(__DIR__,2)) . '/entorno/conexion.php';
    $idArea = $_GET["parametro"]; 
    $resultado;
    if(!filter_var($idArea, FILTER_VALIDATE_INT)){
        die("No juegues con los valores");
    }else{
        $sql = "SELECT * FROM parametro_area WHERE idArea = $idArea";
        $conexion = new Conexion();
        $stmt = $conexion->prepare($sql);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_OBJ);
    }

?>

<script src="<?php echo $url; ?>/js/parametro/area_modifica.js"></script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Tablero
        <small>Panel de control</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="active">Tablero</a></li>
      </ol>
    </section>

    <section class="content">
    <?php if($resultado == null){die("<div class='alert alert-info'>No existen datos con el valor asignado<div>");} ?>
      <form method="POST" id="form">
          <h4 class="box-title">Temas Afines</h4>
          <div class="box box-primary">
              <div class="box-header with-border">
                <button class="btn btn-primary modificar" type="submit"><i class="fa fa-edit"></i> Modificar</button>
              </div>
              <div class="box-body">
                <div class="form-horizontal">

                    <input type="hidden" name="hiddenIdArea" id="hiddenIdArea" value="<?php echo $resultado->idArea; ?>">
                  <div class="form-group">
                    <label for="txtCodigo" class="control-label col-sm-2">Código</label>
                    <div class="col-sm-4">
                      <input type="text" name="txtCodigo" id="txtCodigo" class="form-control" value="<?php echo $resultado->codigo; ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="txtDescripcion" class="control-label col-sm-2">Descripción</label>
                    <div class="col-sm-4">
                      <input type="text" name="txtDescripcion" id="txtDescripcion" class="form-control" value="<?php echo $resultado->descripcion; ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="ddlEstado" class="control-label col-sm-2">Estado</label>
                    <div class="col-sm-4">
                      <select name="ddlEstado" id="ddlEstado" class="form-control">
                        <option value="1" <?php if($resultado->estado == '1'){echo "selected=''";} ?>>Activo</option>
                        <option value="0" <?php if($resultado->estado == '0'){echo "selected=''";} ?>>Inactivo</option>
                      </select>
                    </div>
                  </div>

                </div>
              </div>
              <div class="box-footer">
                <button class="btn btn-primary modificar" type="submit"><i class="fa fa-edit"></i> Modificar</button>
              </div>
          </div>
        </form>
    </section>

  </div>