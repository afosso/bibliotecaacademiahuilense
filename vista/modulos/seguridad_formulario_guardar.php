<script src="<?php echo $url; ?>/js/seguridad/formulario_guardar.js"></script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Tablero
        <small>Panel de control</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="active">Tablero</a></li>
      </ol>
    </section>

    <section class="content">
      <form method="POST" id="form">
          <h4 class="box-title">Formularios</h4>
          <div class="box box-primary">
              <div class="box-header with-border">
                <button class="btn btn-primary guardar" type="submit"><i class="fa fa-save"></i> Guardar</button>
              </div>
              <div class="box-body">
                <div class="form-horizontal">

                  <div class="form-group">
                    <label for="ddlCarpeta" class="control-label col-sm-2">Carpeta</label>
                    <div class="col-sm-4">
                      <select type="text" name="ddlCarpeta" id="ddlCarpeta" class="form-control">
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="txtDescripcion" class="control-label col-sm-2">Descripcion</label>
                    <div class="col-sm-4">
                      <input type="text" name="txtDescripcion" id="txtDescripcion" class="form-control">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="txtRuta" class="control-label col-sm-2">Ruta</label>
                    <div class="col-sm-4">
                      <input type="text" name="txtRuta" id="txtRuta" class="form-control">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="txtIcono" class="control-label col-sm-2">Icono</label>
                    <div class="col-sm-4">
                      <input type="text" name="txtIcono" id="txtIcono" class="form-control">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="ddlEstado" class="control-label col-sm-2">Estado</label>
                    <div class="col-sm-4">
                      <select type="text" name="ddlEstado" id="ddlEstado" class="form-control">
                        <option value="1">Activo</option>
                        <option value="0">Inactivo</option>
                      </select>
                    </div>
                  </div>

                </div>
              </div>
              <div class="box-footer">
                <button class="btn btn-primary guardar" type="submit"><i class="fa fa-save"></i> Guardar</button>
              </div>
          </div>
        </form>
    </section>

  </div>