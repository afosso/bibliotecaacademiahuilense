<?php 
    require_once str_replace("\\", "/", dirname(__DIR__,2)) . '/entorno/conexion.php';

    $idFormulario = $_GET["parametro"]; 
    $resultado;
    $resultadoCarpeta;
    if(!filter_var($idFormulario, FILTER_VALIDATE_INT)){
        die("No juegues con los valores");
    }else{
        $sql = "SELECT * FROM seguridad_formulario WHERE idFormulario = $idFormulario";
        $conexion = new Conexion();
        $stmt = $conexion->prepare($sql);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_OBJ);

        $stmt = $conexion->prepare("SELECT * FROM seguridad_carpeta");
        $stmt->execute();
        $resultadoCarpeta = $stmt->fetchAll(PDO::FETCH_OBJ);
    }

?>

<script src="<?php echo $url; ?>/js/seguridad/formulario_modifica.js"></script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Tablero
        <small>Panel de control</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="active">Tablero</a></li>
      </ol>
    </section>

    <section class="content">
        <?php if($resultado == null){die("<div class='alert alert-info'>No existen datos con el valor asignado<div>");} ?>
        <form method="POST" id="form">
          <h4 class="box-title">Formularios</h4>
          <div class="box box-primary">
              <div class="box-header with-border">
                <button class="btn btn-primary modificar" type="submit"><i class="fa fa-edit"></i> Modificar</button>
              </div>
              <div class="box-body">
                <div class="form-horizontal">
                    <input type="hidden" name="hiddenIdFormulario" id="hiddenIdFormulario" value="<?php echo $resultado->idFormulario; ?>">
                  <div class="form-group">
                    <label for="ddlCarpeta" class="control-label col-sm-2">Carpeta</label>
                    <div class="col-sm-4">
                        <select type="text" name="ddlCarpeta" id="ddlCarpeta" class="form-control">
                            <option value="-1">--Seleccione una opcion--</option>
                            <?php
                                foreach ($resultadoCarpeta as $key => $carpeta) {
                                    if($resultado->idCarpeta == $carpeta->idCarpeta){
                                        echo "<option value='" . $carpeta->idCarpeta . "' selected=''>" . $carpeta->descripcion . "</option>";
                                    }else{
                                        echo "<option value='" . $carpeta->idCarpeta . "'>" . $carpeta->descripcion . "</option>";
                                    }
                                }
                            ?>
                        </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="txtDescripcion" class="control-label col-sm-2">Descripcion</label>
                    <div class="col-sm-4">
                      <input type="text" name="txtDescripcion" id="txtDescripcion" class="form-control" value="<?php echo $resultado->descripcion; ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="txtRuta" class="control-label col-sm-2">Ruta</label>
                    <div class="col-sm-4">
                      <input type="text" name="txtRuta" id="txtRuta" class="form-control" value="<?php echo $resultado->ruta; ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="txtIcono" class="control-label col-sm-2">Icono</label>
                    <div class="col-sm-4">
                      <input type="text" name="txtIcono" id="txtIcono" class="form-control" value="<?php echo $resultado->icono; ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="ddlEstado" class="control-label col-sm-2">Estado</label>
                    <div class="col-sm-4">
                      <select type="text" name="ddlEstado" id="ddlEstado" class="form-control">
                        <option value="1" <?php if($resultado->estado == '1'){echo 'selected';} ?>>Activo</option>
                        <option value="0" <?php if($resultado->estado == '0'){echo 'selected';} ?>>Inactivo</option>
                      </select>
                    </div>
                  </div>

                </div>
              </div>
              <div class="box-footer">
                <button class="btn btn-primary modificar" type="submit"><i class="fa fa-edit"></i> Modificar</button>
              </div>
          </div>
        </form>
    </section>

  </div>