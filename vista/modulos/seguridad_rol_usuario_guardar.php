<?php 
    require_once str_replace("\\", "/", dirname(__DIR__,2)) . '/entorno/conexion.php';

    $conexion = new Conexion();
    $stmt = $conexion->prepare("SELECT * FROM seguridad_usuario");
    $stmt->execute();
    $resultadoUsuario = $stmt->fetchAll(PDO::FETCH_OBJ);

?>

<script src="<?php echo $url; ?>/js/seguridad/rolusuario_guardar.js"></script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Tablero
        <small>Panel de control</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="active">Tablero</a></li>
      </ol>
    </section>

    <section class="content">
      <form method="POST" id="form">
          <h4 class="box-title">Asignar roles ausuarios</h4>
          <div class="box box-primary">
              <div class="box-body">
                <div class="form-horizontal">

                    <div class="form-group">
                        <label for="ddlUsuario" class="control-label col-sm-2">Nombre de usuario</label>
                        <div class="col-sm-4">
                        <select type="text" name="ddlUsuario" id="ddlUsuario" class="form-control" onchange="cargarInfo(this);">
                            <option value="-1">--Seleccione un usuario--</option>
                            <?php
                                foreach ($resultadoUsuario as $key => $value) {
                                    echo '<option value="' . $value->idUsuario . '">' . $value->descripcion . '</option>';
                                }
                            ?>
                        </select>
                        </div>
                    </div>

                    <table id="datos" class="table table-bordered table-striped dt-responsive tabla">
                    </table>

                </div>
              </div>
          </div>
        </form>
    </section>

  </div>