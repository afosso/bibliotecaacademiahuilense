<script src="<?php echo $url; ?>/js/biblioteca/bibliotecacabecera_guardar.js"></script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Tablero
        <small>Panel de control</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="active">Tablero</a></li>
      </ol>
    </section>

    <section class="content">
      <form method="POST" id="form">
          <h4 class="box-title">Registrar Libro</h4>
          <div class="box box-primary">
                <div class="box-header with-border">
                    <button class="btn btn-primary guardar" type="submit"><i class="fa fa-save"></i> Guardar</button>
                </div>
                <div class="box-body">

                    <div class="row">
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="ddlTipo" class="control-label">Tipo</label>
                            <select name="ddlTipo" id="ddlTipo" class="form-control">
                            </select>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="ddlArea" class="control-label">Area</label>
                            <select name="ddlArea" id="ddlArea" class="form-control" onchange="ConsultarSubAreaPorArea();">
                            </select>
                        </div>
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="ddlSubArea" class="control-label">Sub Area</label>
                            <select name="ddlSubArea" id="ddlSubArea" class="form-control">
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="ddlTemasAfines" class="control-label">Temas Afines</label>
                            <select name="ddlTemasAfines" id="ddlTemasAfines" class="form-control">
                            </select>
                        </div>
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="ddlEditorial" class="control-label">Editorial</label>
                            <select name="ddlEditorial" id="ddlEditorial" class="form-control">
                            </select>
                        </div>
                    </div> 

                    <div class="row">
                        <div class="form-group  col-sm-6 col-lg-6">
                            <label for="ddlDepartamento" class="control-label">Departamento</label>
                            <select name="ddlDepartamento" id="ddlDepartamento" class="form-control" onchange="ConsultarMunicipio();">
                            </select>
                        </div>
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="ddlMunicipio" class="control-label">Municipio</label>
                            <select name="ddlMunicipio" id="ddlMunicipio" class="form-control">
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="txtCodigo" class="control-label">Código</label>
                            <input type="text" name="txtCodigo" id="txtCodigo" class="form-control" required>
                        </div>
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="txtDescripcion" class="control-label">Descripción</label>
                            <input type="text" name="txtDescripcion" id="txtDescripcion" class="form-control" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="txtAutor" class="control-label">Autor</label>
                            <input type="text" name="txtAutor" id="txtAutor" class="form-control" required>
                        </div>
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="txtEjercicio" class="control-label">Año</label>
                            <input type="number" name="txtEjercicio" id="txtEjercicio" class="form-control">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="txtNumeroPaginas" class="control-label">Número Páginas</label>
                            <input type="number" name="txtNumeroPaginas" id="txtNumeroPaginas" class="form-control" required>
                        </div>
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="txtNumeroEjemplares" class="control-label">Número Ejemplares</label>
                            <input type="number" name="txtNumeroEjemplares" id="txtNumeroEjemplares" class="form-control" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="ddlTipoAdquisicion" class="control-label">Tipo de Adquisición</label>
                            <select name="ddlTipoAdquisicion" id="ddlTipoAdquisicion" class="form-control">
                                <option value="-1">--Seleccione una opción--</option>
                                <option value="Comprado A">Comprado A</option>
                                <option value="Donado Por">Donado Por</option>
                            </select>
                        </div>
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="txtDescripcionAdquisicion" class="control-label">Descripción Adquisición</label>
                            <input type="text" name="txtDescripcionAdquisicion" id="txtDescripcionAdquisicion" class="form-control">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="txtFechaIngreso" class="control-label">Fecha de Ingreso</label>
                            <input type="text" name="txtFechaIngreso" id="txtFechaIngreso" class="form-control" required>
                        </div>
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="txtPrecio" class="control-label">Precio</label>
                            <input type="number" name="txtPrecio" id="txtPrecio" class="form-control" required>
                        </div>
                    </div>
<!--
                    <div class="row">
                        <fieldset class="border p-2">
                            <legend>Registrar Portada</legend>
                            <div class="form-group col-md-8 col-lg-8">
                                <input type="text" name="txtTituloPortada" id="txtTituloPortada" class="form-control" placeholder="Titulo">
                            </div>
                        
                            <div class="form-group col-md-2 col-lg-2">
                                <input type="text" name="txtNumeroPaginaPortada" id="txtNumeroPaginaPortada" class="form-control" placeholder="Número de página">
                            </div>
                            <div class="form-group col-md-1 col-lg-1">
                                <button type="button" class="btn btn-info" onclick="AgregarRegistroPortada();"><i class="fa fa-plus"></i> Agregar</button>
                            </div>

                            <div class="form-group col-md-12 col-lg-12">
                                <table class="table table-striped table-bordered table-responsive">
                                    <thead>
                                        <tr>
                                            <th>Titulo</th>
                                            <th>Número de página</th>
                                        </tr>
                                    </thead>
                                    <tbody id="datos">
                                    </tbody>
                                </table>
                            </div>
                        </fieldset>
                    </div>

-->
                </div>
                <div class="box-footer">
                    <button class="btn btn-primary guardar" type="submit"><i class="fa fa-save"></i> Guardar</button>
                </div>
          </div>
        </form>
    </section>

  </div>