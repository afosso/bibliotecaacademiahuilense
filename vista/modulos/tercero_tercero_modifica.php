<?php 
    require_once str_replace("\\", "/", dirname(__DIR__,2)) . '/entorno/conexion.php';
    $idTercero = $_GET["parametro"]; 
    $resultado;
    if(!filter_var($idTercero, FILTER_VALIDATE_INT)){
        die("No juegues con los valores");
    }else{
        $sql = "SELECT t.*, m.idDepartamento, m.idMunicipio FROM tercero_tercero t 
                INNER JOIN ubicacion_municipio m ON m.idMunicipio = t.idMunicipioResidencia
                WHERE t.idTercero = $idTercero";
        $conexion = new Conexion();
        $stmt = $conexion->prepare($sql);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_OBJ);
    }

?>

<script src="<?php echo $url; ?>/js/tercero/tercero_modifica.js"></script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Tablero
        <small>Panel de control</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="active">Tablero</a></li>
      </ol>
    </section>

    <section class="content">
      <form method="POST" id="form">
          <h4 class="box-title">Terceros</h4>
          <div class="box box-primary">
              <div class="box-header with-border">
                <button class="btn btn-primary modificar" type="submit"><i class="fa fa-edit"></i> Modificar</button>
              </div>
              <div class="box-body">

                    <input type="hidden" name="hiddenIdTercero" id="hiddenIdTercero" value="<?php echo $resultado->idTercero; ?>">
                    <div class="row">
                        <div class="form-group">
                            <label for="ddlTipoDocumento" class="control-label col-sm-2">Tipo Documento</label>
                            <div class="col-sm-4">
                            <select name="ddlTipoDocumento" id="ddlTipoDocumento" class="form-control">
                                <option value="-1">--Seleccione una opción--</option>
                                <?php
                                    $conexion = new Conexion();
                                    $stmt = $conexion->prepare("SELECT * FROM tercero_tipodocumento");
                                    $stmt->execute();
                                    $tipoDocumento = $stmt->fetchAll(PDO::FETCH_OBJ);
                                    foreach ($tipoDocumento as $key => $value) {
                                        $seleccionado = "";
                                        if($value->idTipoDocumento == $resultado->idTipoDocumento){
                                            $seleccionado = "selected=''";
                                        }else{
                                            $seleccionado = "";
                                        }
                                        echo '<option value="' . $value->idTipoDocumento . '" ' . $seleccionado . '>' . $value->descripcion . '</option>';
                                    }
                                ?>
                            </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="txtNit" class="control-label col-sm-2">Número de identificación</label>
                            <div class="col-sm-4">
                            <input type="text" name="txtNit" id="txtNit" class="form-control" value="<?php echo $resultado->nit; ?>">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="txtNombres" class="control-label col-sm-2">Nombres</label>
                            <div class="col-sm-4">
                            <input type="text" name="txtNombres" id="txtNombres" class="form-control" value="<?php echo $resultado->nombres; ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="txtApellidos" class="control-label col-sm-2">Apellidos</label>
                            <div class="col-sm-4">
                            <input type="text" name="txtApellidos" id="txtApellidos" class="form-control" value="<?php echo $resultado->apellidos; ?>">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="ddlGenero" class="control-label col-sm-2">Género</label>
                            <div class="col-sm-4">
                            <select name="ddlGenero" id="ddlGenero" class="form-control">
                                <option value="M" <?php if($resultado->genero == 'M'){echo "selected=''";} ?>>Masculino</option>
                                <option value="F" <?php if($resultado->genero == 'F'){echo "selected=''";} ?>>Femenino</option>
                            </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="txtDireccion" class="control-label col-sm-2">Dirección</label>
                            <div class="col-sm-4">
                            <input type="text" name="txtDireccion" id="txtDireccion" class="form-control" value="<?php echo $resultado->direccion; ?>">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="ddlDepartamento" class="control-label col-sm-2">Departamento Residencia</label>
                            <div class="col-sm-4">
                            <select name="ddlDepartamento" id="ddlDepartamento" class="form-control" onchange="ConsultarMunicipio();">
                                <option value="-1">--Selecccione una opción--</option>
                                <?php
                                    $conexion = new Conexion();
                                    $stmt = $conexion->prepare("SELECT * FROM ubicacion_departamento");
                                    $stmt->execute();
                                    $departamento = $stmt->fetchAll(PDO::FETCH_OBJ);
                                    foreach ($departamento as $key => $value) {
                                        $seleccionado = "";
                                        if($value->idDepartamento == $resultado->idDepartamento){
                                            $seleccionado = "selected=''";
                                        }else{
                                            $seleccionado = "";
                                        }
                                        echo '<option value="' . $value->idDepartamento . '"' . $seleccionado . '>' . $value->descripcion . '</option>';
                                    }
                                ?>
                            </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="ddlMunicipio" class="control-label col-sm-2">Municipio Residencia</label>
                            <div class="col-sm-4">
                            <select name="ddlMunicipio" id="ddlMunicipio" class="form-control" >
                                <option value="-1">--Selecccione un municipio--</option>
                                <?php
                                    $conexion = new Conexion();
                                    $stmt = $conexion->prepare("SELECT * FROM ubicacion_municipio WHERE idDepartamento = :idDepartamento");
                                    $stmt->bindValue(":idDepartamento", $resultado->idDepartamento, PDO::PARAM_INT);
                                    $stmt->execute();
                                    $departamento = $stmt->fetchAll(PDO::FETCH_OBJ);
                                    foreach ($departamento as $key => $value) {
                                        $seleccionado = "";
                                        if($value->idDepartamento == $resultado->idDepartamento){
                                            $seleccionado = "selected=''";
                                        }else{
                                            $seleccionado = "";
                                        }
                                        echo '<option value="' . $value->idMunicipio . '"' . $seleccionado . '>' . $value->descripcion . '</option>';
                                    }
                                ?>
                            </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="ddlEstado" class="control-label col-sm-2">Estado</label>
                            <div class="col-sm-4">
                            <select name="ddlEstado" id="ddlEstado" class="form-control">
                                <option value="1" <?php if($resultado->estado == '1'){echo "selected=''";} ?>>Activo</option>
                                <option value="0" <?php if($resultado->estado == '0'){echo "selected=''";} ?>>Inactivo</option>
                            </select>
                            </div>
                        </div>
                    </div>

              </div>
              <div class="box-footer">
                <button class="btn btn-primary modificar" type="submit"><i class="fa fa-edit"></i> Modificar</button>
              </div>
          </div>
        </form>
    </section>

  </div>