<script src="<?php echo $url; ?>/js/parametro/subarea_guardar.js"></script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Tablero
        <small>Panel de control</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="active">Tablero</a></li>
      </ol>
    </section>

    <section class="content">
      <form method="POST" id="form">
          <h4 class="box-title">Sub area</h4>
          <div class="box box-primary">
              <div class="box-header with-border">
                <button class="btn btn-primary guardar" type="submit"><i class="fa fa-save"></i> Guardar</button>
              </div>
              <div class="box-body">
                <div class="form-horizontal">

                <div class="form-group">
                    <label for="ddlArea" class="control-label col-sm-2">Area</label>
                    <div class="col-sm-4">
                      <select name="ddlArea" id="ddlArea" class="form-control">
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="txtCodigo" class="control-label col-sm-2">Código</label>
                    <div class="col-sm-4">
                      <input type="text" name="txtCodigo" id="txtCodigo" class="form-control">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="txtDescripcion" class="control-label col-sm-2">Descripción</label>
                    <div class="col-sm-4">
                      <input type="text" name="txtDescripcion" id="txtDescripcion" class="form-control">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="ddlEstado" class="control-label col-sm-2">Estado</label>
                    <div class="col-sm-4">
                      <select name="ddlEstado" id="ddlEstado" class="form-control">
                        <option value="1">Activo</option>
                        <option value="0">Inactivo</option>
                      </select>
                    </div>
                  </div>

                </div>
              </div>
              <div class="box-footer">
                <button class="btn btn-primary guardar" type="submit"><i class="fa fa-save"></i> Guardar</button>
              </div>
          </div>
        </form>
    </section>

  </div>