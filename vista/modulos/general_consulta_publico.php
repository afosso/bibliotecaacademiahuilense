    <script src="<?php echo $url; ?>/js/biblioteca/biblioteca_consulta_publica.js"></script>
    <form method="POST" id="form">
        <h4 class="box-title">Consulta</h4>
        <div class="box box-primary">

            <div class="box-header with-border">
            <button class="btn btn-primary consultar" type="submit"><i class="fa fa-search"></i> Consultar</button><br>
            <span class="text-help">Recuerda que puedes digitar cualquier opción para la búsqueda</span>

            <div class="row">

                <div class="form-group col-md-6">
                <label for="txtAutor" class="control-label">Autor</label>
                <input type="text" name="txtAutor" id="txtAutor" class="form-control" placeholder="Digite el nombre o apellido del autor">
                </div>

                <div class="form-group col-md-6">
                <label for="txtDescripcion" class="control-label">Título</label>
                <input type="text" name="txtDescripcion" id="txtDescripcion" class="form-control" placeholder="Escriba una palabra clave del titulo">
                </div>
                
            </div>

            <div class="row">

                <div class="form-group col-md-6">
                <label for="txtPalabrasClaves" class="control-label">Palabras Claves</label>
                <input type="text" name="txtPalabrasClaves" id="txtPalabrasClaves" class="form-control" placeholder="Digite la palabra clave">
                </div>

                <div class="form-group col-md-6">
                <label for="txtAnio" class="control-label">Año de publicación</label>
                <input type="text" name="txtAnio" id="txtAnio" class="form-control" placeholder="Digite el año de publicación">
                </div>
                
            </div>

            </div>
            <div class="box-body">
                <table id="datos" class="table table-bordered table-striped dt-responsive tabla">
                </table>
            </div>
        </div>
    </form>


    <div class="modal fade modal-warning" tabindex="-1" role="dialog" id="modalTablaContenido">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Tabla de contenido</h4>
        </div>
        <div class="modal-body">
            <table id="tablaContenido" class="table table-responsive table-bordered">
            </table>
        </div>
        </div>
    </div>
    </div>