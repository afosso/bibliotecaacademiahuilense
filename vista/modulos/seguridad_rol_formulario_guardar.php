<?php 
    require_once str_replace("\\", "/", dirname(__DIR__,2)) . '/entorno/conexion.php';

    $conexion = new Conexion();
    $stmt = $conexion->prepare("SELECT * FROM seguridad_rol");
    $stmt->execute();
    $resultadoRol = $stmt->fetchAll(PDO::FETCH_OBJ);

?>

<script src="<?php echo $url; ?>/js/seguridad/rolformulario_guardar.js"></script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Tablero
        <small>Panel de control</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="active">Tablero</a></li>
      </ol>
    </section>

    <section class="content">
      <form method="POST" id="form">
          <h4 class="box-title">Asignar formularios a roles</h4>
          <div class="box box-primary">
              <div class="box-body">
                <div class="form-horizontal">

                    <div class="form-group">
                        <label for="ddlRol" class="control-label col-sm-2">Rol</label>
                        <div class="col-sm-4">
                        <select type="text" name="ddlRol" id="ddlRol" class="form-control" onchange="cargarInfo(this);">
                            <option value="-1">--Seleccione un rol--</option>
                            <?php
                                foreach ($resultadoRol as $key => $value) {
                                    echo '<option value="' . $value->idRol . '">' . $value->descripcion . '</option>';
                                }
                            ?>
                        </select>
                        </div>
                    </div>

                    <table id="datos" class="table table-bordered table-striped dt-responsive tabla">
                    </table>

                </div>
              </div>
          </div>
        </form>
    </section>

  </div>