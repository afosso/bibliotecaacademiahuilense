<?php 
    require_once str_replace("\\", "/", dirname(__DIR__,2)) . '/entorno/conexion.php';
    $idDepartamento = $_GET["parametro"]; 
    $resultado;
    if(!filter_var($idDepartamento, FILTER_VALIDATE_INT)){
        die("No juegues con los valores");
    }else{
        $sql = "SELECT * FROM ubicacion_departamento WHERE idDepartamento = $idDepartamento";
        $conexion = new Conexion();
        $stmt = $conexion->prepare($sql);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_OBJ);
    }

?>

<script src="<?php echo $url; ?>/js/ubicacion/departamento_modifica.js"></script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Tablero
        <small>Panel de control</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="active">Tablero</a></li>
      </ol>
    </section>

    <section class="content">
    <?php if($resultado == null){die("<div class='alert alert-info'>No existen datos con el valor asignado<div>");} ?>
      <form method="POST" id="form">
          <h4 class="box-title">Pais</h4>
          <div class="box box-primary">
              <div class="box-header with-border">
                <button class="btn btn-primary modificar" type="submit"><i class="fa fa-edit"></i> Modificar</button>
              </div>
              <div class="box-body">
                <div class="form-horizontal">

                    <input type="hidden" name="hiddenIdDepartamento" id="hiddenIdDepartamento" value="<?php echo $resultado->idDepartamento; ?>">

                  <div class="form-group">
                    <label for="ddlPais" class="control-label col-sm-2">Pais</label>
                    <div class="col-sm-4">
                      <select name="ddlPais" id="ddlPais" class="form-control">
                        <option value="-1">--Seleccione una opción--</option>
                        <?php
                            $stmt = $conexion->prepare("SELECT * FROM ubicacion_pais");
                            $stmt->execute();
                            $resultadoPais = $stmt->fetchAll(PDO::FETCH_OBJ);
                            if($resultadoPais != null && count($resultadoPais) > 0){
                                $seleccionado = "";
                                foreach ($resultadoPais as $key => $valuePais) {
                                  if($resultado->idPais == $valuePais->idPais){
                                      $seleccionado = "selected=''";
                                  }else{
                                      $seleccionado = "";
                                  }
                                    echo '<option value="' . $valuePais->idPais . '" ' . $seleccionado . '>' . $valuePais->descripcion . '</option>';
                                }
                            }
                        ?> 
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="txtCodigoDane" class="control-label col-sm-2">Código DANE</label>
                    <div class="col-sm-4">
                      <input type="text" name="txtCodigoDane" id="txtCodigoDane" class="form-control" value="<?php echo $resultado->codigoDane; ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="txtDescripcion" class="control-label col-sm-2">Descripcion</label>
                    <div class="col-sm-4">
                      <input type="text" name="txtDescripcion" id="txtDescripcion" class="form-control" value="<?php echo $resultado->descripcion; ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="ddlEstado" class="control-label col-sm-2">Estado</label>
                    <div class="col-sm-4">
                      <select name="ddlEstado" id="ddlEstado" class="form-control">
                        <option value="1" <?php if($resultado->estado == '1'){echo "selected=''";} ?>>Activo</option>
                        <option value="0" <?php if($resultado->estado == '0'){echo "selected=''";} ?>>Inactivo</option>
                      </select>
                    </div>
                  </div>

                </div>
              </div>
              <div class="box-footer">
                <button class="btn btn-primary modificar" type="submit"><i class="fa fa-edit"></i> Modificar</button>
              </div>
          </div>
        </form>
    </section>

  </div>