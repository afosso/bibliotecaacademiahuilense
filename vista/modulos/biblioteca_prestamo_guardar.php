<script src="<?php echo $url; ?>/js/biblioteca/prestamos_guardar.js"></script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Tablero
        <small>Panel de control</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="active">Tablero</a></li>
      </ol>
    </section>

    <section class="content">
      <form method="POST" id="form">
          <h4 class="box-title">Prestamos</h4>
          <div class="box box-primary">
              <div class="box-header with-border">
                <button class="btn btn-primary guardar" type="submit"><i class="fa fa-save"></i> Guardar</button>
              </div>
              <div class="box-body">

                  <div class="col-md-6">
                    <p class="margin text-bold">Biblioteca</p>
                    <div class="input-group">
                      <input type="hidden" name="hiddeIdBiblioteca" id="hiddeIdBiblioteca">
                      <input type="text" readonly="true" name="txtBiblioteca" id="txtBiblioteca" class="form-control" required>
                      <div class="input-group-addon" onclick="$('#modal-busqueda-biblioteca').modal('show');">
                        <i class="fa fa-search"></i>
                      </div>
                      <div class="input-group-addon" onclick="limpiarBiblioteca();">
                        <i class="fa fa-eraser"></i>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <p class="margin text-bold">Tercero</p>
                    <div class="input-group">
                      <input type="hidden" name="hiddeIdTercero" id="hiddeIdTercero">
                      <input type="text" readonly="true" name="txtTercero" id="txtTercero" class="form-control" required>
                      <div class="input-group-addon" onclick="$('#modal-busqueda-tercero').modal('show');">
                        <i class="fa fa-search"></i>
                      </div>
                      <div class="input-group-addon" onclick="limpiarTercero();">
                        <i class="fa fa-eraser"></i>
                      </div>
                    </div>
                  </div>

                  <div class="form-group col-sm-6">
                    <label for="ddlTipoServicio" class="control-label">Tipo Servicio</label>
                    <select name="ddlTipoServicio" id="ddlTipoServicio" class="form-control" required>
                    </select>
                  </div>

                  <div class="form-group col-sm-6">
                    <label for="txtFechaSalida" class="control-label">Fecha Salida</label>
                    <input type="text" name="txtFechaSalida" id="txtFechaSalida" class="form-control" required>
                  </div>

                  <div class="form-group col-sm-6">
                    <label for="txtFechaVencimiento" class="control-label">Fecha Vencimiento</label>
                    <input type="text" name="txtFechaVencimiento" id="txtFechaVencimiento" class="form-control" required>
                  </div>

                  <div class="form-group col-sm-6">
                    <label for="txtNumeroEjemplar" class="control-label">Número de ejemplares</label>
                    <input type="number" name="txtNumeroEjemplar" id="txtNumeroEjemplar" class="form-control" min="1" required>
                  </div>

              </div>
              <div class="box-footer">
                <button class="btn btn-primary guardar" type="submit"><i class="fa fa-save"></i> Guardar</button>
              </div>
          </div>
        </form>
    </section>

    <div class="modal fade" id="modal-busqueda-biblioteca">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Consulta Libro</h4>
            </div>

            <div class="modal-body">
                <form method="POST" id="formConsultaBiblioteca">
                    <div class="form-group col-md-12">
                        <label for="txtCodigoLibro" class="control-label">Código Libro</label>
                        <input type="text" class="form-control" name="txtCodigoLibro" id="txtCodigoLibro">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="txtNombres" class="control-label">Nombre Libro</label>
                        <input type="text" class="form-control" name="txtNombres" id="txtNombres">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="txtAutor" class="control-label">Autor</label>
                        <input type="number" class="form-control" name="txtAutor" id="txtAutor">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary pull-right consultarBiblioteca"><i class="fa fa-search"></i> Consultar</button>
                    </div>
                    <table id="datosBiblioteca" class="table table-bordered table-striped dt-responsive tabla">
                    </table>
                </form>
            </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modal-busqueda-tercero">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Consulta Terceros</h4>
            </div>

            <div class="modal-body">
                <form method="POST" id="formConsultaTercero">
                    <div class="form-group col-md-12">
                        <label for="txtNumeroIdentificacion" class="control-label">Número identificación</label>
                        <input type="text" class="form-control" name="txtNumeroIdentificacion" id="txtNumeroIdentificacion">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="txtNombres" class="control-label">Nombres</label>
                        <input type="text" class="form-control" name="txtNombres" id="txtNombres">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="txtApellidos" class="control-label">Apellidos</label>
                        <input type="number" class="form-control" name="txtApellidos" id="txtApellidos">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary pull-right consultarTercero"><i class="fa fa-search"></i> Consultar</button>
                    </div>
                    <table id="datosTercero" class="table table-bordered table-striped dt-responsive tabla">
                    </table>
                </form>
            </div>
        </div>
      </div>
    </div>

  </div>