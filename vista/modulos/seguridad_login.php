<script src="./js/seguridad/iniciarsesion.js"></script>
<div id="back"></div>
<div class="login-box">
  <div class="login-logo">
    <!--<a href="#"><b>Gimnasio</b> Infantil</a>-->
    <figure>
      <img src="./vista/dist/img/LogoenPNG.png" width="200" alt="Biblioteca Academia Huilense">
    </figure>
  </div>
  <!-- /.login-logo -->
  <form id="form" method="POST">
    <div class="login-box-body">
        <h2 class="login-box-msg">ACADEMIA HUILENSE DE HISTORIA</h2>  
        <p class="login-box-msg">Ingresar al sistema</p>
        <div class="form-group has-feedback">
          <input type="text" class="form-control" placeholder="Usuario" name="txtUsuario" id="txtUsuario" maxlength="20" required>
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" class="form-control" placeholder="Password" name="txtContrasenia" id="txtContrasenia" maxlength="50" required>
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
          <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button>
          </div>
        </div>
    </div>
  </form>
</div>