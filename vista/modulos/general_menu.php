<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="active">
                <a href="<?php echo $url; ?>/general_inicio">
                    <i class="fa fa-home"></i>
                    <span>Inicio</span>
                </a>
            </li>
            <!--<li class="treeview">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Examples</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview treeview-menu">
                    <li><a href="pages/examples/invoice.html"><i class="fa fa-circle"></i> Invoice</a></li>
                    <li><a href="pages/examples/profile.html"><i class="fa fa-circle"></i> Profile</a></li>
                    <li><a href="pages/examples/login.html"><i class="fa fa-circle"></i> Login</a></li>
                    <li><a href="pages/examples/register.html"><i class="fa fa-circle"></i> Register</a></li>
                    <li><a href="pages/examples/lockscreen.html"><i class="fa fa-circle"></i> Lockscreen</a></li>
                    <li><a href="pages/examples/404.html"><i class="fa fa-circle"></i> 404 Error</a></li>
                    <li><a href="pages/examples/500.html"><i class="fa fa-circle"></i> 500 Error</a></li>
                    <li><a href="pages/examples/blank.html"><i class="fa fa-circle"></i> Blank Page</a></li>
                    <li><a href="pages/examples/pace.html"><i class="fa fa-circle"></i> Pace Page</a></li>
                </ul>
            </li>-->


            <?php

                require_once str_replace("\\", "/", dirname(__DIR__,2)) . '/entorno/conexion.php';

                function ConsultarCarpetasPorId($idCarpeta, $url){
                    $sql = "SELECT DISTINCT
                        c.`descripcion` AS 'Carpeta', 
                        c.`idCarpeta` AS 'idCarpeta',
                        cp.`idCarpeta` AS 'idCarpetaPadre'
                    FROM `seguridad_rolusuario` ru
                    INNER JOIN `seguridad_rolformulario` rf ON rf.`idRol` = ru.`idRol`
                    INNER JOIN `seguridad_formulario` f ON f.`idFormulario`= rf.`idFormulario`
                    INNER JOIN `seguridad_carpeta` c ON c.`idCarpeta` = f.`idCarpeta`
                    LEFT JOIN `seguridad_carpeta` cp ON cp.`idCarpeta` = c.`idCarpetaPadre`
                    WHERE ru.`idUsuario` = 1 AND c.`idCarpetaPadre` " . $idCarpeta;
                    $conexion = new Conexion();
                    $stmt = $conexion->prepare($sql);
                    $stmt->execute();
                    $resultado = $stmt->fetchAll(PDO::FETCH_OBJ);

                    if($resultado != null && count($resultado) > 0){
                        foreach ($resultado as $key => $value) {
                            echo '<li class="treeview">';
                            echo '<a href="#">';
                            echo '<i class="fa fa-folder"></i> <span>'.$value->Carpeta.'</span>';
                            echo '<span class="pull-right-container">';
                            echo '<i class="fa fa-angle-left pull-right"></i>';
                            echo '</span>';
                            echo '</a>';
                            echo '<ul class="treeview treeview-menu">';
                            if($value->idCarpetaPadre == null){
                                ConsultarFormulariosPorId(" AND c.`idCarpetaPadre` IS NULL ", $url);
                            }else{
                                ConsultarFormulariosPorId(" AND c.`idCarpeta` = " . $value->idCarpeta, $url);
                            }

                            ConsultarCarpetasPorId(" = " . $value->idCarpeta, $url);
                            echo '</ul>';
                            echo '</li>';
                        }
                    }
                }

                function ConsultarFormulariosPorId($idCarpeta, $url){
                    $sql = "SELECT 
                        f.`descripcion`, 
                        f.`ruta`,
                        f.`icono`
                    FROM `seguridad_rolusuario` ru
                    INNER JOIN `seguridad_rolformulario` rf ON rf.`idRol` = ru.`idRol`
                    INNER JOIN `seguridad_formulario` f ON f.`idFormulario`= rf.`idFormulario`
                    INNER JOIN `seguridad_carpeta` c ON c.`idCarpeta` = f.`idCarpeta`
                    WHERE ru.`idUsuario` = 1 " . $idCarpeta;
                    $conexion = new Conexion();
                    $stmt = $conexion->prepare($sql);
                    $stmt->execute();
                    $resultado = $stmt->fetchAll(PDO::FETCH_OBJ);
                    if($resultado != null && count($resultado) > 0){
                        foreach ($resultado as $key => $value) {
                            echo '<li>
                                    <a href="' . $url . '/' . $value->ruta . '">
                                        <i class="' . $value->icono . '"></i>
                                        <span>' . $value->descripcion . '</span>
                                    </a>
                                </li>';
                        }
                    }
                }

                $conexion = new Conexion();
                $stmt = $conexion->prepare("SELECT DISTINCT
                                                cp.`idCarpeta` AS 'idCarpetaPadre',
                                                cp.`descripcion` AS 'CarpetaPadre'
                                            FROM `seguridad_rolusuario` ru
                                            INNER JOIN `seguridad_rolformulario` rf ON rf.`idRol` = ru.`idRol`
                                            INNER JOIN `seguridad_formulario` f ON f.`idFormulario`= rf.`idFormulario`
                                            INNER JOIN `seguridad_carpeta` c ON c.`idCarpeta` = f.`idCarpeta`
                                            INNER JOIN `seguridad_carpeta` cp ON cp.`idCarpeta` = c.`idCarpetaPadre`
                                            WHERE ru.`idUsuario` = 1 ");
                $stmt->execute();
                $resultado = $stmt->fetchAll(PDO::FETCH_OBJ);
                if($resultado != null && count($resultado) > 0){
                    foreach ($resultado as $key => $value) {
                        echo '<li class="treeview">';
                            echo '<a href="#">';
                            echo '<i class="fa fa-folder"></i> <span>'.$value->CarpetaPadre.'</span>';
                            echo '<span class="pull-right-container">';
                            echo '<i class="fa fa-angle-left pull-right"></i>';
                            echo '</span>';
                            echo '</a>';
                            echo '<ul class="treeview treeview-menu">';
                            ConsultarFormulariosPorId(" AND c.`idCarpeta` = " . $value->idCarpetaPadre, $url);

                            ConsultarCarpetasPorId(" = " . $value->idCarpetaPadre, $url);
                            echo '</ul>';
                            echo '</li>';
                    }
                }
            ?>
        </ul>
    </section>
</aside>