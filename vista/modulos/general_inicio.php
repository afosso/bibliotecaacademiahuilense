<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <script src="<?php echo $url; ?>/js/biblioteca/prestamos_pendientes_consulta.js"></script>
    <section class="content-header">
      <h1>
        Tablero
        <small>Panel de control</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="active">Tablero</a></li>
      </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3><?php 
                            require_once str_replace("\\", "/", dirname(__DIR__,2)) . '/entorno/conexion.php';
                            $sql = "SELECT COUNT(idPrestamo) AS 'cantidadPrestamos' FROM `biblioteca_prestamos`";
                            $conexion = new Conexion();
                            $stmt = $conexion->prepare($sql);
                            $stmt->execute();
                            $resultado = $stmt->fetch(\PDO::FETCH_OBJ);
                            echo $resultado->cantidadPrestamos;
                        ?></h3>
                        <p>Préstamos</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-book"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-green-gradient">
                    <div class="inner">
                        <h3><?php 
                            require_once str_replace("\\", "/", dirname(__DIR__,2)) . '/entorno/conexion.php';
                            $sql = "SELECT COUNT(idPrestamo) AS 'cantidadEntregados' FROM `biblioteca_prestamos` WHERE `fechaEntrada` IS NOT NULL";
                            $conexion = new Conexion();
                            $stmt = $conexion->prepare($sql);
                            $stmt->execute();
                            $resultado = $stmt->fetch(\PDO::FETCH_OBJ);
                            echo $resultado->cantidadEntregados;
                        ?></h3>
                        <p>Libros entregados</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-check"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-red-gradient">
                    <div class="inner">
                        <h3><?php 
                            require_once str_replace("\\", "/", dirname(__DIR__,2)) . '/entorno/conexion.php';
                            $sql = "SELECT COUNT(idPrestamo) AS 'cantidadPrestamo' FROM `biblioteca_prestamos` WHERE `fechaEntrada` IS NULL";
                            $conexion = new Conexion();
                            $stmt = $conexion->prepare($sql);
                            $stmt->execute();
                            $resultado = $stmt->fetch(\PDO::FETCH_OBJ);
                            echo $resultado->cantidadPrestamo;
                        ?></h3>
                        <p>Libros en préstamos</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-clock"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-green-active">
                    <div class="inner">
                        <h3><?php 
                            require_once str_replace("\\", "/", dirname(__DIR__,2)) . '/entorno/conexion.php';
                            $sql = "SELECT COUNT(idBibliotecaCabecera) AS 'cantidadLibros' FROM `biblioteca_bibliotecacabecera`";
                            $conexion = new Conexion();
                            $stmt = $conexion->prepare($sql);
                            $stmt->execute();
                            $resultado = $stmt->fetch(\PDO::FETCH_OBJ);
                            echo $resultado->cantidadLibros;
                        ?></h3>
                        <p>Cantidad de libros en la estanteria</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-list"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <table class="table table-responsive table-condensed" id="tablaPrestamos"></table>
            </div>
        </div>
    </section>

  </div>