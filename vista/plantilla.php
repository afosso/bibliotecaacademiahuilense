<?php
    session_start();
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Biblioteca Academia Huilense</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!--
        ==============================
        PLUGINS CSS
        ==============================
    -->

    <?php
        $url = "http://" . $_SERVER["SERVER_NAME"] . ":8025/bibliotecaacademiahuilense";
    ?>

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo $url; ?>/vista/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <!--<link rel="stylesheet" href="<?php echo $url; ?>/vista/bower_components/font-awesome/css/font-awesome.min.css">-->
    <link rel="stylesheet" href="<?php echo $url; ?>/vista/plugins/fontawesome/css/all.css">

    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo $url; ?>/vista/bower_components/Ionicons/css/ionicons.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $url; ?>/vista/dist/css/AdminLTE.css">

    <!-- AdminLTE Skins. -->
    <link rel="stylesheet" href="<?php echo $url; ?>/vista/dist/css/skins/_all-skins.min.css">

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="<?php echo $url; ?>/vista/plugins/DataTables/datatables.min.css"/>
    <!--<link rel="stylesheet" type="text/css" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"/>-->

    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo $url; ?>/vista/plugins/iCheck/all.css">

    <!--DatePicker-->
    <link rel="stylesheet" href="<?php echo $url; ?>/vista/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- Time Picker -->
    <link rel="stylesheet" href="<?php echo $url; ?>/vista/plugins/timepicker/bootstrap-timepicker.min.css">

    <!-- Calendar -->
    <link rel="stylesheet" href="<?php echo $url; ?>/vista/bower_components/fullcalendar/dist/fullcalendar.min.css">
    <link rel="stylesheet" href="<?php echo $url; ?>/vista/bower_components/fullcalendar/dist/fullcalendar.print.min.css" media="print">

    <!-- Color Picker -->
    <link rel="stylesheet" href="<?php echo $url; ?>/vista/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">

    <!-- SWAL -->
    <link rel="stylesheet" href="<?php echo $url; ?>/vista/plugins/sweetalert2/dist/sweetalert2.min.css">

    <!-- SELECT -->
    <link rel="stylesheet" href="<?php echo $url; ?>/vista/bower_components/select2/dist/css/select2.css">

    <!--
        ==============================
        PLUGINS DE JAVASCRIPT
        ==============================
    -->

    <!-- jQuery 3 -->
    <script src="<?php echo $url; ?>/vista/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo $url; ?>/vista/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- SlimScroll -->
    <script src="<?php echo $url; ?>/vista/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

    <!-- FastClick -->
    <script src="<?php echo $url; ?>/vista/bower_components/fastclick/lib/fastclick.js"></script>

    <!-- AdminLTE App -->
    <script src="<?php echo $url; ?>/vista/dist/js/adminlte.min.js"></script>

    <!-- DataTables -->
    <script type="text/javascript" src="<?php echo $url; ?>/vista/plugins/DataTables/datatables.min.js"></script>
    <!--<script type="text/javascript" src="<?php echo $url; ?>/vista/bower_components/datatables.net-bs/js/dataTables.bootstrap.js"></script>-->

    <!-- DAtePicker -->
    <script src="<?php echo $url; ?>/vista/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

    <!-- iCheck -->
    <script src="<?php echo $url; ?>/vista/plugins/iCheck/icheck.min.js"></script>

    <!-- Time Picker -->
    <script src="<?php echo $url; ?>/vista/plugins/timepicker/bootstrap-timepicker.min.js"></script>

    <!-- Calendar -->
    <script src="<?php echo $url; ?>/vista/bower_components/moment/moment.js"></script>
    <script src="<?php echo $url; ?>/vista/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="<?php echo $url; ?>/vista/bower_components/fullcalendar/dist/locale/es.js"></script>

    <!-- Color Picker -->
    <script src="<?php echo $url; ?>/vista/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>

    <!-- SWAL -->
    <script src="<?php echo $url; ?>/vista/plugins/sweetalert2/dist/sweetalert2.all.min.js"></script>

    <!-- SELECT -->
    <script src="<?php echo $url; ?>/vista/bower_components/select2/dist/js/select2.full.min.js"></script>    

</head>
<body class="hold-transition skin-blue sidebar-mini login-page">
    <?php
        if(isset($_SESSION["iniciarSesion"]) && 
            $_SESSION["iniciarSesion"] == "ok"){

            echo '<div class="wrapper">';
            include "modulos/general_cabezote.php";
            include "modulos/general_menu.php";
            if(isset($_GET["ruta"])){
                if($_GET["ruta"] == "general_inicio" ||
                    $_GET["ruta"] == "seguridad_usuario_guardar" ||
                    $_GET["ruta"] == "seguridad_usuario_consulta" ||
                    $_GET["ruta"] == "seguridad_usuario_modifica" ||
                    $_GET["ruta"] == "seguridad_rol_guardar" ||
                    $_GET["ruta"] == "seguridad_rol_consulta" ||
                    $_GET["ruta"] == "seguridad_rol_modifica" ||
                    $_GET["ruta"] == "seguridad_carpeta_guardar" ||
                    $_GET["ruta"] == "seguridad_carpeta_consulta" ||
                    $_GET["ruta"] == "seguridad_carpeta_modifica" ||
                    $_GET["ruta"] == "seguridad_formulario_guardar" ||
                    $_GET["ruta"] == "seguridad_formulario_consulta" ||
                    $_GET["ruta"] == "seguridad_formulario_modifica" ||
                    $_GET["ruta"] == "seguridad_rol_formulario_guardar" ||
                    $_GET["ruta"] == "seguridad_rol_usuario_guardar" ||
                    $_GET["ruta"] == "ubicacion_departamento_consulta" ||
                    $_GET["ruta"] == "ubicacion_departamento_guardar" ||
                    $_GET["ruta"] == "ubicacion_departamento_modifica" ||
                    $_GET["ruta"] == "ubicacion_municipio_consulta" ||
                    $_GET["ruta"] == "ubicacion_municipio_guardar" ||
                    $_GET["ruta"] == "ubicacion_municipio_modifica" ||
                    $_GET["ruta"] == "ubicacion_pais_consulta" ||
                    $_GET["ruta"] == "ubicacion_pais_guardar" ||
                    $_GET["ruta"] == "ubicacion_pais_modifica" ||
                    $_GET["ruta"] == "tercero_tipodocumento_consulta" ||
                    $_GET["ruta"] == "tercero_tipodocumento_guardar" ||
                    $_GET["ruta"] == "tercero_tipodocumento_modifica" ||
                    $_GET["ruta"] == "parametro_area_consulta" ||
                    $_GET["ruta"] == "parametro_area_guardar" ||
                    $_GET["ruta"] == "parametro_area_modifica" ||
                    $_GET["ruta"] == "parametro_subarea_consulta" ||
                    $_GET["ruta"] == "parametro_subarea_guardar" ||
                    $_GET["ruta"] == "parametro_subarea_modifica" ||
                    $_GET["ruta"] == "parametro_temasafines_consulta" ||
                    $_GET["ruta"] == "parametro_temasafines_guardar" ||
                    $_GET["ruta"] == "parametro_temasafines_modifica" ||
                    $_GET["ruta"] == "parametro_tipo_consulta" ||
                    $_GET["ruta"] == "parametro_tipo_guardar" ||
                    $_GET["ruta"] == "parametro_tipo_modifica" || 
                    $_GET["ruta"] == "parametro_tiposervicio_consulta" ||
                    $_GET["ruta"] == "parametro_tiposervicio_guardar" ||
                    $_GET["ruta"] == "parametro_tiposervicio_modifica" ||
                    $_GET["ruta"] == "biblioteca_editorial_consulta" ||
                    $_GET["ruta"] == "biblioteca_editorial_guardar" ||
                    $_GET["ruta"] == "biblioteca_editorial_modifica" ||
                    $_GET["ruta"] == "tercero_tercero_modifica" ||
                    $_GET["ruta"] == "tercero_tercero_guardar" ||
                    $_GET["ruta"] == "tercero_tercero_consulta" ||
                    $_GET["ruta"] == "biblioteca_bibliotecacabecera_guardar" ||
                    $_GET["ruta"] == "biblioteca_bibliotecacabecera_consulta" ||
                    $_GET["ruta"] == "biblioteca_bibliotecacabecera_modifica" ||
                    $_GET["ruta"] == "biblioteca_prestamo_consulta" ||
                    $_GET["ruta"] == "biblioteca_prestamo_guardar" ||
                    $_GET["ruta"] == "general_salir"
                    ){
                    include "modulos/" . $_GET["ruta"] . ".php";
                }
                else{
                    include "modulos/general_404.php";
                }
            }else{
                include "modulos/general_404.php";
            }
            include "modulos/general_footer.php";
            echo '</div>';

        }else{
            if(count($_GET) > 0){
                if($_GET["ruta"] == "" || $_GET["ruta"] == "seguridad_login"){
                    include "modulos/seguridad_login.php";
                }else{
                    include "modulos/general_consulta_publico.php";
                }
            }else{
                include "modulos/seguridad_login.php";
            }
        }
    ?>
    <script src="<?php echo $url; ?>/js/plantilla.js"></script>
</body>
</html>
