$(document).ready(function(){
    $.ajax({
        url: './controlador/ubicacion/consulta.departamento.controlador.php',
        type: 'post',
        dataType: 'json'
    }).done(function(response){
        var departamento = '<option value="-1">--Seleccione una opción--</option>';
        $.each(response, function(index, data){
            departamento += '<option value="' + data.idDepartamento + '">' + data.descripcion + '</option>';
        });
        $('#ddlDepartamento').html(departamento);
    });

    $('.guardar').click(function(event){
        event.preventDefault();

        $.ajax({
            url: './controlador/ubicacion/guardar.municipio.controlador.php',
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json'
        }).done(function(response){
            if(response.status == "OK"){
                mostrarMensaje('success', 'Datos guardados con éxito');
                $('#form').trigger("reset");
            }else{
                mostrarMensaje('error', response.mensaje);
            }
        })
    })
});