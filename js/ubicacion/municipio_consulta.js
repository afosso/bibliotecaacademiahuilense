$(document).ready(function(){
    $.ajax({
        url: './controlador/ubicacion/consulta.municipio.controlador.php',
        type: 'post',
        dataType: 'json'
    }).done(function(response){
        var tabla = [];
        $.each(response, function(index, data){
            var botones = "<button type='button' class='btn btn-warning' onclick='EnviarModificar(" + data.idMunicipio + ");'><i class='fa fa-edit'></i></button>";
            botones += "<button type='button' class='btn btn-danger' onclick='Eliminar(" + data.idMunicipio + ");'><i class='fa fa-trash'></i></button>";
            var obj = [
                (index+1),
                data.descripcionDepartamento,
                data.codigoDane,
                data.descripcion,
                (data.estado == "1" ? "<label class='label label-success'>ACTIVO</label>" : "<label class='label label-danger'>INACTIVO</label>"),
                botones
            ]
            tabla.push(obj);
        });
        var columnas = [
            { title : "#" },
            { title : "Departamento" },
            { title : "Código DANE" },
            { title : "Descripcion" },
            { title : "Estado"},
            { title : "Acciones" }
        ]
        agregarDataTable("datos", tabla, columnas);
    }).fail(function(response){
        console.log(response.responseText);
    });
});

function Eliminar(idMunicipio){
    $.ajax({
        url: './controlador/ubicacion/elimina.municipio.controlador.php',
        data: { "idMunicipio" : idMunicipio},
        type: 'post',
        dataType: 'json',
        success: function(response){
            if(response == "OK"){
                mostrarMensaje('succes', 'Municipio eliminado con éxito');
                window.location = 'ubicacion_municipio_consulta';
            }else{
                mostrarMensaje('error', response);
            }
        }
    });
}

function EnviarModificar(idMunicipio){
    window.location = "ubicacion_municipio_modifica/" + idMunicipio;
}