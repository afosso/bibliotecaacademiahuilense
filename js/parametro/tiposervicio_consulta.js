$(document).ready(function(){
    $.ajax({
        url: './controlador/parametro/consulta.tiposervicio.controlador.php',
        type: 'post',
        dataType: 'json'
    }).done(function(response){
        var tabla = [];
        $.each(response, function(index, data){
            var botones = "<button type='button' class='btn btn-warning' onclick='EnviarModificar(" + data.idTipoServicio + ");'><i class='fa fa-edit'></i></button>";
            botones += "<button type='button' class='btn btn-danger' onclick='Eliminar(" + data.idTipoServicio + ");'><i class='fa fa-trash'></i></button>";
            var obj = [
                (index+1),
                data.codigo,
                data.descripcion,
                (data.estado == "1" ? "<label class='label label-success'>ACTIVO</label>" : "<label class='label label-danger'>INACTIVO</label>"),
                botones
            ]
            tabla.push(obj);
        });
        var columnas = [
            { title : "#" },
            { title : "Código" },
            { title : "Descripcion" },
            { title : "Estado"},
            { title : "Acciones" }
        ]
        agregarDataTable("datos", tabla, columnas);
    }).fail(function(response){
        console.log(response.responseText);
    });
});

function Eliminar(idTipoServicio){
    $.ajax({
        url: './controlador/parametro/elimina.tiposervicio.controlador.php',
        data: { "idTipoServicio" : idTipoServicio},
        type: 'post',
        dataType: 'json',
        success: function(response){
            if(response == "OK"){
                mostrarMensaje('succes', 'Registro eliminado con éxito');
                window.location = 'parametro_tiposervicio_consulta';
            }else{
                mostrarMensaje('error', response);
            }
        }
    });
}

function EnviarModificar(idTipoServicio){
    window.location = "parametro_tiposervicio_modifica/" + idTipoServicio;
}