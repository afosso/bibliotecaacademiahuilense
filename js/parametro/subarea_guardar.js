$(document).ready(function(){
    $.ajax({
        url: './controlador/parametro/consulta.area.controlador.php',
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var area = '<option value="-1">--Seleccione una opción--</option>';
        $.each(response, function(index, data){
            area += '<option value="' + data.idArea + '">' + data.descripcion + '</option>';;
        });
        $('#ddlArea').html(area);
    });

    $('.guardar').click(function(event){
        event.preventDefault();

        $.ajax({
            url: './controlador/parametro/guardar.subarea.controlador.php',
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json'
        }).done(function(response){
            if(response.status == "OK"){
                mostrarMensaje('success', 'Datos guardados con éxito');
                $('#form').trigger("reset");
            }else{
                mostrarMensaje('error', response.mensaje);
            }
        })
    })
});