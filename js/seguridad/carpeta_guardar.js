$(document).ready(function(){
    $.ajax({
        url: './controlador/seguridad/consulta.carpeta.controlador.php',
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var datos = '<option value="-1">--Seleccione una carpeta padre--</option>';
        $.each(response, function(index, data){
            datos += '<option value="' + data.idCarpeta + '">' + data.descripcion + '</option>';
        });
        $('#ddlCarpetaPadre').html(datos);
    });

    $('.guardar').click(function(event){
        event.preventDefault();

        $.ajax({
            url: './controlador/seguridad/guardar.carpeta.controlador.php',
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json'
        }).done(function(response){
            if(response.status == "OK"){
                mostrarMensaje('success', 'Datos guardados con éxito');
                $('#form').trigger("reset");
            }else{
                mostrarMensaje('error', response.mensaje);
            }
        })
    })
});