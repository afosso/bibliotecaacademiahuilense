$(document).ready(function(){
    $.ajax({
        url: './controlador/seguridad/consulta.formulario.controlador.php',
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var tabla = [];
        $.each(response, function(index, data){
            var obj = [
                (index+1),
                '<label class="label-control checkbox"><input type="checkbox" name="' + data.idFormulario + '" id="' + data.idFormulario + '" onchange="guardarCheckBox(this);"> ' + data.descripcion + '</label>'
            ]
            tabla.push(obj);
        });
        var columnas = [
            { title : "#" },
            { title : "Descripcion" }
        ]
        agregarDataTable("datos", tabla, columnas);
    });
});

function guardarCheckBox(chk){
    if($('#ddlRol').val() != "-1"){
        $.ajax({
            url: './controlador/seguridad/guardar.rolformulario.controlador.php',
            data: { 
                "idRol": $('#ddlRol').val(),
                "idFormulario": chk.id,
                "valor": chk.checked
            },
            type: 'POST',
            dataType: 'json'
        }).done(function(response){
            if(response == "OK"){
                mostrarMensaje('success', 'Datos actualizados con éxito');
            }else{
                mostrarMensaje('error', 'Error al guardar los datos');
            }
        })
    }else{
        mostrarMensaje('error', 'Debe seleccionar una opción valida');
    }
}

function cargarInfo(selectForm){
    $('input[type=checkbox]').each(function(index, data){
        data.checked = false;
    });

    if(selectForm.value != "-1"){
        $.ajax({
            url: './controlador/seguridad/consulta.rolfomulario.controlador.php',
            data: {"idRol": selectForm.value},
            type: 'POST',
            dataType: 'json'
        }).done(function(response){
            $.each(response, function(index, data){
                document.getElementById(data.idFormulario).checked = true;
            });
        });
    }
}