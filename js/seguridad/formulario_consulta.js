$(document).ready(function(){
    $.ajax({
        url: './controlador/seguridad/consulta.formulario.controlador.php',
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var tabla = [];
        $.each(response, function(index, data){
            var botones = "<button type='button' class='btn btn-warning' onclick='EnviarModificar(" + data.idFormulario + ");'><i class='fa fa-edit'></i></button>";
            botones += "<button type='button' class='btn btn-danger' onclick='Eliminar(" + data.idFormulario + ");'><i class='fa fa-trash'></i></button>";
            var obj = [
                (index+1),
                data.descripcionCarpeta,
                data.descripcion,
                data.ruta,
                data.icono,
                (data.estado == "1" ? "<label class='label label-success'>ACTIVO</label>" : "<label class='label label-danger'>INACTIVO</label>"),
                botones
            ]
            tabla.push(obj);
        });
        var columnas = [
            { title : "#" },
            { title : "Carpeta" },
            { title : "Descripcion" },
            { title : "Ruta" },
            { title : "Icono" },
            { title : "Estado"},
            { title : "Acciones" }
        ]
        agregarDataTable("datos", tabla, columnas);
    });
});

function Eliminar(idFormulario){
    $.ajax({
        url: './controlador/seguridad/elimina.formulario.controlador.php',
        data: { "idFormulario" : idFormulario},
        type: 'post',
        dataType: 'json',
        success: function(response){
            if(response == "OK"){
                mostrarMensaje('succes', 'Formulario eliminado con éxito');
                window.location = 'seguridad_formulario_consulta';
            }else{
                mostrarMensaje('error', response);
            }
        }
    });
}

function EnviarModificar(idFormulario){
    window.location = "seguridad_formulario_modifica/" + idFormulario;
}