$(document).ready(function(){
    $.ajax({
        url: './controlador/seguridad/consulta.usuario.controlador.php',
        type: 'post',
        dataType: 'json',
        success: function(response){
            var tabla = [];
            $.each(response, function(index, data){
                var botones = "<button type='button' class='btn btn-warning' onclick='EnviarModificar(" + data.idUsuario + ");'><i class='fa fa-edit'></i></button>";
                botones += "<button type='button' class='btn btn-danger' onclick='Eliminar(" + data.idUsuario + ");'><i class='fa fa-trash'></i></button>";
                var obj = [
                    (index+1),
                    data.codigo,
                    data.descripcion,
                    data.fechaActivacion,
                    data.fechaExpiracion,
                    (data.administrador == "1" ? "<label class='label label-success'>SI</label>" : "<label class='label label-danger'>NO</label>"),
                    "<img src='" + data.fotoUrl + "' alt='' class='img-circle' style='width:50px;height:50px;'></img>",
                    botones
                ]
                tabla.push(obj);
            })
            var columnas = [
                { title : "#" },
                { title : "Nombre de usuario" },
                { title : "Descripcion de usuario" },
                { title : "Fecha de Activación"},
                { title : "Fecha de Expiración" },
                { title : "Administrador" },
                { title : "Foto" },
                { title : "Acciones" },
            ]
            agregarDataTable("datos", tabla, columnas);
        }
    });
});

function Eliminar(idUsuario){
    $.ajax({
        url: './controlador/seguridad/elimina.usuario.controlador.php',
        data: { "idUsuario" : idUsuario},
        type: 'post',
        dataType: 'json',
        success: function(response){
            if(response == "OK"){
                mostrarMensaje('succes', 'Usuario eliminado con éxito');
                window.location = 'seguridad_usuario_consulta';
            }else{
                mostrarMensaje('error', response);
            }
        }
    });
}

function EnviarModificar(idUsuario){
    window.location = "seguridad_usuario_modifica/" + idUsuario;
}