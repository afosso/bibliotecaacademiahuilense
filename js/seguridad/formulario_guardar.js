$(document).ready(function(){
    $.ajax({
        url: './controlador/seguridad/consulta.carpeta.controlador.php',
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var carpeta = '<option value="-1">--Seleccione una opción--</option>';
        $.each(response, function(index, data){
            carpeta += '<option value="' + data.idCarpeta + '">' + data.descripcion + '</option>';
        });
        $('#ddlCarpeta').html(carpeta);
    });

    $('.guardar').click(function(event){
        event.preventDefault();

        $.ajax({
            url: './controlador/seguridad/guardar.formulario.controlador.php',
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json'
        }).done(function(response){
            if(response.status == "OK"){
                mostrarMensaje('success', 'Datos guardados con éxito');
                $('#form').trigger("reset");
            }else{
                mostrarMensaje('error', response.mensaje);
            }
        });
    });
});