$(document).ready(function(){

    $('.guardar').click(function(event){
        event.preventDefault();

        $.ajax({
            url: './controlador/seguridad/guardar.rol.controlador.php',
            data: $('#form').serialize(),
            type: 'post',
            dataType: 'json',
            success: function(response){
                if(response.status == "OK"){
                    mostrarMensaje('success', 'Datos guardados con éxito');
                    $('#form').trigger("reset");
                }else{
                    mostrarMensaje('error', response.mensaje);
                }
            }
        });
    });

});

function nuevo(){
    $('#form').clear();
}