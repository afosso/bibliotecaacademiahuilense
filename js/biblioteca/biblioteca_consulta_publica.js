$(document).ready(function(){
    $.ajax({
        url: './controlador/parametro/consulta.tipo.controlador.php',
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var tipo = '<option value="-1">--Seleccione una opción--</option>';
        $.each(response, function(index, data){
            tipo += '<option value="' + data.idTipo + '">' + data.descripcion + '</option>';
        });
        $('#ddlTipo').html(tipo);
    });

    $.ajax({
        url: './controlador/parametro/consulta.temasafines.controlador.php',
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var TemasAfines = '<option value="-1">--Seleccione una opción--</option>';
        $.each(response, function(index, data){
            TemasAfines += '<option value="' + data.idTemasAfines + '">' + data.descripcion + '</option>';
        });
        $('#ddlTemasAfines').html(TemasAfines);
    });

    $.ajax({
        url: './controlador/parametro/consulta.tipo.controlador.php',
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var tipo = '<option value="-1">--Seleccione una opción--</option>';
        $.each(response, function(index, data){
            tipo += '<option value="' + data.idTipo + '">' + data.descripcion + '</option>';
        });
        $('#ddlTipo').html(tipo);
    });

    $('.consultar').click(function(event){
        event.preventDefault();
        
        var tabla = document.getElementById("datos").getElementsByTagName('tbody')[0];
        if(tabla !== undefined){
            if(tabla.rows.length > 0){
                $('#datos').dataTable().fnDestroy();
            }
        }
        
        $.ajax({
            url: './controlador/biblioteca/consultapublico.biblioteca.controlador.php',
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json'
        }).done(function(response){
            var tabla = [];
        $.each(response, function(index, data){
            var obj = [
                (index+1),
                data.codigo,
                data.descripcion,
                data.autor,
                data.ejercicio,
                data.numeroEjemplar,
                "<button type='button' onclick='ConsultaTablaContenido(" + data.idBibliotecaCabecera + ")' class='btn btn-warning'><i class='fa fa-eye'></i></button>"
            ]
            tabla.push(obj);
            });
            var columnas = [
                { title : "#" },
                { title : "Código Libro" },
                { title : "Descripción Libro" },
                { title : "Autor"},
                { title : "Año de publicación"},
                { title : "Número de ejemplares"},
                { title : "Ver Tabla de Contenido"}
            ]
            agregarDataTable("datos", tabla, columnas);
        }).fail(function(response){
            console.log(response);
        });
    });
    
});

function ConsultaTablaContenido(id){
    $.ajax({
        url: './controlador/biblioteca/consultapublicoportada.biblioteca.controlador.php',
        data: {"idCabecera": id},
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var datos = "<thead><tr><th>Titulo</th><th>Página</th></tr></thead><tbody>";
        $.each(response, function(index, data){
            datos += "<tr><td>" + data.descripcion + "</td><td>" + data.numeroPagina + "</td></tr>";
        });
        datos += "</tbody>";

        $('#tablaContenido').html(datos);
        $('#modalTablaContenido').modal("show");
    });
}