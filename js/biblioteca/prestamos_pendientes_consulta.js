var fecha = '';

$(document).ready(function(){
    $.ajax({
        url: './controlador/biblioteca/consulta.prestamospendientes.controlador.php',
        type: 'post',
        dataType: 'json'
    }).done(function(response){
        var tabla = [];
        $.each(response, function(index, data){
            var botones = "<button type='button' class='btn btn-warning' onclick='EnviarModificar(" + data.idPrestamo + "," + (data.fechaEntrada == null ? "true" : "false") + ");'><i class='fa fa-calendar-check'></i></button>";
            var obj = [
                data.codigoLibro,
                data.libro,
                data.tercero,
                data.telefono,
                data.celular,
                data.correoElectronico,
                data.tipoServicio,
                data.numeroEjemplar,
                data.numeroPaginas,
                data.fechaSalida,
                data.fechaVencimiento,
                botones
            ]
            tabla.push(obj);
        });
        var columnas = [
            { title : "Código" },
            { title : "Libro" },
            { title : "Persona" },
            { title : "Teléfono"},
            { title : "Celular"},
            { title : "Correo Eléctronico"},
            { title : "Tipo Servicio"},
            { title : "N°. Ejemplar"},
            { title : "N°. Páginas"},
            { title : "Fecha Salida"},
            { title : "Fecha Vencimiento"},
            { title : "Acciones" }
        ]
        agregarDataTable("tablaPrestamos", tabla, columnas);
    }).fail(function(response){
        console.log(response.responseText);
    });
});



function EnviarModificar(idPrestamo, modificar){
    if(modificar){
    Swal.fire({
        title: '¿Estás seguro de marcar la entrada de este préstamo?',
        text: "Esta acción no se puede revertir.",
        type: 'warning',
        input: 'text',
        inputAttributes: {
            placeholder: 'yyyy-mm-dd'
        },
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Cerrar el préstamo!',
        cancelButtonText: "Cancelar",
        preConfirm: (login) => {
            fecha = `${login}`
        }
      }).then((result) => {
        if (result.value) {
            $.ajax({
                url: './controlador/biblioteca/modifica.prestamos.controlador.php',
                data: { "idPrestamo" : idPrestamo, "fecha" : fecha},
                type: 'post',
                dataType: 'json',
                success: function(response){
                    if(response.status == "OK"){
                        mostrarMensaje('success', 'Entrada marcada con éxito');
                        window.location = 'general_inicio';
                    }else{
                        mostrarMensaje('error', response);
                    }
                }
            });
        }
      })
    }else{
        mostrarMensaje('warning', 'Este préstamo ya fue cerrado');
    }
    
}