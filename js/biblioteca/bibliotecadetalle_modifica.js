$(document).ready(function(){
    $('.modificar').click(function(event){
        event.preventDefault();

        $.ajax({
            url: './controlador/biblioteca/modifica.bibliotecadetalle.controlador.php',
            data: $('#formModifica').serialize(),
            type: 'POST',
            dataType: 'json',
            success: function(response){
                if(response.status == "OK"){
                    mostrarMensaje('success', 'Datos modificados con éxito');
                }else{
                    mostrarMensaje('error', response.mensaje);
                }
            },
            error: function(response){
                console.log(response.responseText);
            }
        });
    });
});