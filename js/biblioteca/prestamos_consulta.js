$(document).ready(function(){
    $.ajax({
        url: './controlador/biblioteca/consulta.prestamos.controlador.php',
        type: 'post',
        dataType: 'json'
    }).done(function(response){
        var tabla = [];
        $.each(response, function(index, data){
            var botones = "<button type='button' class='btn btn-warning' onclick='EnviarModificar(" + data.idPrestamo + "," + (data.fechaEntrada == null ? "true" : "false") + ");'><i class='fa fa-calendar-check'></i></button>";
            botones += "<button type='button' class='btn btn-danger' onclick='Eliminar(" + data.idPrestamo + ");'><i class='fa fa-trash'></i></button>";
            var obj = [
                (index+1),
                data.libro,
                data.tercero,
                data.tipoServicio,
                data.fechaSalida,
                data.fechaVencimiento,
                data.fechaEntrada,
                data.numeroEjemplar,
                botones
            ]
            tabla.push(obj);
        });
        var columnas = [
            { title : "#" },
            { title : "Libro" },
            { title : "Persona" },
            { title : "Tipo Servicio"},
            { title : "Fecha Salida"},
            { title : "Fecha Vencimiento"},
            { title : "Fecha Entrada"},
            { title : "Número Ejemplar"},
            { title : "Acciones" }
        ]
        agregarDataTable("datos", tabla, columnas);
    }).fail(function(response){
        console.log(response.responseText);
    });
});

function Eliminar(idPrestamo){
    $.ajax({
        url: './controlador/biblioteca/elimina.prestamos.controlador.php',
        data: { "idPrestamo" : idPrestamo},
        type: 'post',
        dataType: 'json',
        success: function(response){
            if(response == "OK"){
                mostrarMensaje('succes', 'Registro eliminado con éxito');
                window.location = 'biblioteca_prestamo_consulta';
            }else{
                mostrarMensaje('error', response);
            }
        }
    });
}

function EnviarModificar(idPrestamo, modificar){
    if(modificar){
    Swal.fire({
        title: '¿Estás seguro de marcar la entrada de este préstamo?',
        text: "Esta acción no se puede revertir.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Cerrar el préstamo!',
        cancelButtonText: "Cancelar"
      }).then((result) => {
        if (result.value) {
            $.ajax({
                url: './controlador/biblioteca/modifica.prestamos.controlador.php',
                data: { "idPrestamo" : idPrestamo},
                type: 'post',
                dataType: 'json',
                success: function(response){
                    if(response.status == "OK"){
                        mostrarMensaje('success', 'Entrada marcada con éxito');
                        window.location = 'biblioteca_prestamo_consulta';
                    }else{
                        mostrarMensaje('error', response);
                    }
                }
            });
        }
      })
    }else{
        mostrarMensaje('warning', 'Este préstamo ya fue cerrado');
    }
    
}