$(document).ready(function(){

    $('#txtFechaIngreso').datepicker({
        format: 'yyyy-mm-dd'
    })

    $.ajax({
        url: './controlador/parametro/consulta.tipo.controlador.php',
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var tipo = '<option value="-1">--Seleccione un tipo--</option>';
        $.each(response, function(index, data){
            tipo += '<option value="' + data.idTipo + '">' + data.descripcion + '</option>';
        });
        $('#ddlTipo').html(tipo);
    });

    $.ajax({
        url: './controlador/parametro/consulta.area.controlador.php',
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var area = '<option value="-1">--Seleccione un area--</option>';
        $.each(response, function(index, data){
            area += '<option value="' + data.idArea + '">' + data.descripcion + '</option>';
        });
        $('#ddlArea').html(area);
    });

    $.ajax({
        url: './controlador/parametro/consulta.temasafines.controlador.php',
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var temasAfines = '<option value="-1">--Seleccione un temas afines--</option>';
        $.each(response, function(index, data){
            temasAfines += '<option value="' + data.idTemasAfines + '">' + data.descripcion + '</option>';
        });
        $('#ddlTemasAfines').html(temasAfines);
    });

    $.ajax({
        url: './controlador/biblioteca/consulta.editorial.controlador.php',
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var editorial = '<option value="-1">--Seleccione un editorial--</option>';
        $.each(response, function(index, data){
            editorial += '<option value="' + data.idEditorial + '">' + data.descripcion + '</option>';
        });
        $('#ddlEditorial').html(editorial);
    });

    $.ajax({
        url: './controlador/ubicacion/consulta.departamento.controlador.php',
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var departamento = '<option value="-1">--Seleccione un departamento--</option>';
        $.each(response, function(index, data){
            departamento += '<option value="' + data.idDepartamento + '">' + data.descripcion + '</option>';
        });
        $('#ddlDepartamento').html(departamento);
    });
    

    $('.guardar').click(function(event){
        event.preventDefault();

        $.ajax({
            url: './controlador/biblioteca/guardar.bibliotecacabecera.controlador.php',
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json'
        }).done(function(response){
            if(response.status == "OK"){
                mostrarMensaje('success', 'Datos guardados con éxito');
                $('#form').trigger("reset");
            }else{
                mostrarMensaje('error', response.mensaje);
            }
        })
    })
});

function ConsultarSubAreaPorArea(){
    $.ajax({
        url: './controlador/parametro/consulta.subarea.controlador.php',
        data: {"idArea": $('#ddlArea').val()},
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var subarea = '<option value="-1">--Seleccione un SubArea--</option>';
        $.each(response, function(index, data){
            subarea += '<option value="' + data.idSubArea + '">' + data.descripcion + '</option>';
        });
        $('#ddlSubArea').html(subarea);
    });
}

function ConsultarMunicipio(){
    $.ajax({
        url: './controlador/ubicacion/consulta.municipio.controlador.php',
        data: {"idDepartamento": $('#ddlDepartamento').val()},
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var municipio = '<option value="-1">--Seleccione un municipio--</option>';
        $.each(response, function(index, data){
            municipio += '<option value="' + data.idMunicipio + '">' + data.descripcion + '</option>';
        });
        $('#ddlMunicipio').html(municipio);
    });
}

function AgregarRegistroPortada(){
    var titulo, pagina;
    titulo = $('#txtTituloPortada').val();
    pagina = $('#txtNumeroPaginaPortada').val();
    if(titulo == "" || pagina == ""){
        mostrarMensaje('error', "Debe llenar todos los campos");
        return;
    }

    var fila = "<tr><td>" + titulo + "</td><td>" + pagina + "</td></tr>";

    var btn = document.createElement("TR");
    btn.innerHTML = fila;

    document.getElementById("datos").appendChild(btn);
}