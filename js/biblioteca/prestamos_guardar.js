$(document).ready(function(){
    $('#txtFechaSalida').datepicker({
        format: 'yyyy-mm-dd'
    });
    $('#txtFechaVencimiento').datepicker({
        format: 'yyyy-mm-dd'
    });

    $.ajax({
        url: './controlador/parametro/consulta.tiposervicio.controlador.php',
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var tipoServicio = '<option value="-1">--Seleccione una opción--</option>';
        $.each(response,function(index, data){
            tipoServicio += '<option value="' + data.idTipoServicio  + '">' + data.descripcion + '</option>';
        });
        $('#ddlTipoServicio').html(tipoServicio);
    });

    $('.guardar').click(function(event){
        event.preventDefault();

        $.ajax({
            url: './controlador/biblioteca/guardar.prestamos.controlador.php',
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json'
        }).done(function(response){
            if(response.status == "OK"){
                mostrarMensaje('success', 'Datos guardados con éxito');
                $('#form').trigger("reset");
            }else{
                mostrarMensaje('error', response.mensaje);
            }
        })
    });

    $('.consultarBiblioteca').click(function(event){
        event.preventDefault();

        $.ajax({
            url: './controlador/biblioteca/consultaavanzada.bibliotecacabecera.controlador.php',
            data: $('#formConsultaBiblioteca').serialize(),
            type: 'POST',
            dataType: 'json'
        }).done(function(response){
            var tabla = [];
            $.each(response, function(index, data){
                var botones = "<button type='button' class='btn btn-primary' onclick='seleccionarBiblioteca(" + data.idBibliotecaCabecera + ");'><i class='fa fa-hand-pointer'></i></button>";
                var obj = [
                    (index+1),
                    data.codigo,
                    data.descripcion,
                    data.autor,
                    botones
                ]
                tabla.push(obj);
            });
            var columnas = [
                { title : "#" },
                { title : "Código" },
                { title : "Descripción" },
                { title : "Autor" },
                { title : "Acciones" }
            ]
            agregarDataTable("datosBiblioteca", tabla, columnas);
            $('#datosBiblioteca').dataTable().fnDestroy();
        });
    });

    $('.consultarTercero').click(function(event){
        event.preventDefault();
        $.ajax({
            url: './controlador/tercero/consultaavanzada.tercero.controlador.php',
            data: $('#formConsultaTercero').serialize(),
            type: 'POST',
            dataType: 'json'
        }).done(function(response){
            var tabla = [];
            $.each(response, function(index, data){
                var botones = "<button type='button' class='btn btn-primary' onclick='seleccionarTercero(" + data.idTercero + ");'><i class='fa fa-hand-pointer'></i></button>";
                var obj = [
                    (index+1),
                    data.nit,
                    data.nombres,
                    data.apellidos,
                    botones
                ]
                tabla.push(obj);
            });
            var columnas = [
                { title : "#" },
                { title : "Número identificación" },
                { title : "Nombres" },
                { title : "Apellidos" },
                { title : "Acciones" }
            ]
            agregarDataTable("datosTercero", tabla, columnas);
            $('#datosTercero').dataTable().fnDestroy();
        });
    })
});

function seleccionarBiblioteca(idBibliotecaCabecera){
    $.ajax({
        url: './controlador/biblioteca/consultalibro.bibliotecacabecera.controlador.php',
        data: {"idCabecera": idBibliotecaCabecera},
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        $('#txtBiblioteca').val(response.descripcion);
        $('#hiddeIdBiblioteca').val(response.idBibliotecaCabecera);
        $('#modal-busqueda-biblioteca').modal("hide");
    });
}

function seleccionarTercero(idTercero){
    $.ajax({
        url: './controlador/tercero/consultatercero.tercero.controlador.php',
        data: {"idTercero": idTercero},
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        $('#txtTercero').val(response.nombres + ' ' + response.apellidos);
        $('#hiddeIdTercero').val(response.idTercero);
        $('#modal-busqueda-tercero').modal("hide");
    });
}

function limpiarTercero(){
    $('#txtTercero').val("");
    $('#hiddeIdTercero').val("");
}

function limpiarBiblioteca(){
    $('#txtBiblioteca').val("");
    $('#hiddeIdBiblioteca').val("");
}