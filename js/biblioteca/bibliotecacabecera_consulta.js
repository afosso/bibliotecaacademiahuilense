$(document).ready(function(){
    $.ajax({
        url: './controlador/biblioteca/consulta.bibliotecacabecera.controlador.php',
        type: 'post',
        dataType: 'json'
    }).done(function(response){
        var tabla = [];
        $.each(response, function(index, data){
            var botones = "<button type='button' class='btn btn-warning' onclick='EnviarModificar(" + data.idBibliotecaCabecera + ");'><i class='fa fa-edit'></i></button>";
            botones += "<button type='button' class='btn btn-danger' onclick='Eliminar(" + data.idBibliotecaCabecera + ");'><i class='fa fa-trash'></i></button>";
            var obj = [
                (index+1),
                data.codigo,
                data.descripcion,
                data.autor,
                data.fechaIngreso,
                data.numeroPaginas,
                data.numeroEjemplar,
                "<button type='button' class='btn btn-warning' onclick='AgregarPortada(" + data.idBibliotecaCabecera + ");'><i class='fa fa-plus'></i></button>" +
                "<button type='button' class='btn btn-info' onclick='ListarPortada(" + data.idBibliotecaCabecera + ");'><i class='fa fa-eye'></i></button>",
                botones
            ]
            tabla.push(obj);
        });
        var columnas = [
            { title : "#" },
            { title : "Código" },
            { title : "Descripcion" },
            { title : "Autor"},
            { title : "Fecha Ingreso"},
            { title : "Número de página"},
            { title : "Número de ejemplares"},
            { title : "Acciones Portada"},
            { title : "Acciones" }
        ]
        agregarDataTable("datos", tabla, columnas);
    }).fail(function(response){
        console.log(response.responseText);
    });
});

function Eliminar(idBibliotecaCabecera){
    $.ajax({
        url: './controlador/biblioteca/elimina.bibliotecacabecera.controlador.php',
        data: { "idBibliotecaCabecera" : idBibliotecaCabecera},
        type: 'post',
        dataType: 'json',
        success: function(response){
            if(response == "OK"){
                mostrarMensaje('succes', 'Registro eliminado con éxito');
                window.location = 'biblioteca_bibliotecacabecera_consulta';
            }else{
                mostrarMensaje('error', response);
            }
        }
    });
}

function EnviarModificar(idBibliotecaCabecera){
    window.location = "biblioteca_bibliotecacabecera_modifica/" + idBibliotecaCabecera;
}


function AgregarPortada(idCabecera){
    $('#hiddenIdCabecera').val(idCabecera);

    $('#modal-default').modal("show");
}

function ListarPortada(id){
    
    $.ajax({
        url: './controlador/biblioteca/consulta.bibliotecadetalle.controlador.php',
        data: {"idCabecera": id},
        type: 'post',
        dataType: 'json'
    }).done(function(response){
        var tabla = [];
        $.each(response, function(index, data){
            var botones = "<button type='button' class='btn btn-warning' onclick='ModificarPortada(" + data.idBibliotecaDetalle + ");'><i class='fa fa-edit'></i></button>";
            botones += "<button type='button' class='btn btn-danger' onclick='EliminarPortada(" + data.idBibliotecaDetalle + ");'><i class='fa fa-trash'></i></button>";
            var obj = [
                (index+1),
                data.descripcion,
                data.numeroPagina,
                botones
            ]
            tabla.push(obj);
        });
        var columnas = [
            { title : "#" },
            { title : "Descripcion" },
            { title : "Número Página"},
            { title : "Acciones" }
        ]
        agregarDataTable("datosPaginas", tabla, columnas);
        $('#modal-listado-paginas').modal("show");
        $("#datosPaginas").dataTable().fnDestroy();
    }).fail(function(response){
        console.log(response.responseText);
    });
}

function ModificarPortada(idBibliotecaDetalle){
    $.ajax({
        url: './controlador/biblioteca/consulta.bibliotecadetalle.controlador.php',
        data: { "idDetalle" : idBibliotecaDetalle},
        type: 'post',
        dataType: 'json',
        success: function(response){
            $('#hiddenIdDetalle').val(response.idBibliotecaDetalle);
            $('#txtDescripcionPortadaDetalle').val(response.descripcion);
            $('#txtNumeroPaginaDetalle').val(response.numeroPagina);
            $('#modal-editar-pagina').modal("show")
        }
    });
}

function EliminarPortada(idBibliotecaDetalle){
    $.ajax({
        url: './controlador/biblioteca/elimina.bibliotecadetalle.controlador.php',
        data: { "idBibliotecaDetalle" : idBibliotecaDetalle},
        type: 'post',
        dataType: 'json',
        success: function(response){
            if(response == "OK"){
                mostrarMensaje('succes', 'Registro eliminado con éxito');
                window.location = 'biblioteca_bibliotecacabecera_consulta';
            }else{
                mostrarMensaje('error', response);
            }
        }
    });
}