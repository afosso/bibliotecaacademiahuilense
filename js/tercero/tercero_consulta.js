$(document).ready(function(){
    $.ajax({
        url: './controlador/tercero/consulta.tercero.controlador.php',
        type: 'post',
        dataType: 'json'
    }).done(function(response){
        var tabla = [];
        $.each(response, function(index, data){
            var botones = "<button type='button' class='btn btn-warning' onclick='EnviarModificar(" + data.idTercero + ");'><i class='fa fa-edit'></i></button>";
            botones += "<button type='button' class='btn btn-danger' onclick='Eliminar(" + data.idTercero + ");'><i class='fa fa-trash'></i></button>";
            var obj = [
                (index+1),
                data.nit,
                data.nombres,
                data.apellidos,
                data.direccion,
                (data.estado == "1" ? "<label class='label label-success'>ACTIVO</label>" : "<label class='label label-danger'>INACTIVO</label>"),
                botones
            ]
            tabla.push(obj);
        });
        var columnas = [
            { title : "#" },
            { title : "Número de identificación" },
            { title : "Nombres" },
            { title : "Apellidos" },
            { title : "Dirección" },
            { title : "Estado"},
            { title : "Acciones" }
        ]
        agregarDataTable("datos", tabla, columnas);
    }).fail(function(response){
        console.log(response.responseText);
    });
});

function Eliminar(idTercero){
    $.ajax({
        url: './controlador/tercero/elimina.tercero.controlador.php',
        data: { "idTercero" : idTercero},
        type: 'post',
        dataType: 'json',
        success: function(response){
            if(response == "OK"){
                mostrarMensaje('succes', 'Registro eliminado con éxito');
                window.location = 'tercero_tercero_consulta';
            }else{
                mostrarMensaje('error', response);
            }
        }
    });
}

function EnviarModificar(idTercero){
    window.location = "tercero_tercero_modifica/" + idTercero;
}