$(document).ready(function(){

    $.ajax({
        url: './controlador/tercero/consulta.tipodocumento.controlador.php',
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var tipoDocumento = '<option value="-1">--Seleccione un Tipo de Documento--</option>';
        $.each(response, function(index, data){
            tipoDocumento += '<option value="' + data.idTipoDocumento + '">' + data.descripcion + '</option>';
        });
        $('#ddlTipoDocumento').html(tipoDocumento);
    });

    $.ajax({
        url: './controlador/ubicacion/consulta.departamento.controlador.php',
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var departamento = '<option value="-1">--Seleccione un Departamento--</option>';
        $.each(response, function(index, data){
            departamento += '<option value="' + data.idDepartamento + '">' + data.descripcion + '</option>';
        });
        $('#ddlDepartamento').html(departamento);
    });

    $('.guardar').click(function(event){
        event.preventDefault();

        $.ajax({
            url: './controlador/tercero/guardar.tercero.controlador.php',
            data: $('#form').serialize(),
            type: 'POST',
            dataType: 'json'
        }).done(function(response){
            if(response.status == "OK"){
                mostrarMensaje('success', 'Datos guardados con éxito');
                $('#form').trigger("reset");
            }else{
                mostrarMensaje('error', response.mensaje);
            }
        })
    })
});


function ConsultarMunicipio(){
    $.ajax({
        url: './controlador/ubicacion/consulta.municipio.controlador.php',
        data: { "idDepartamento" : $('#ddlDepartamento').val()},
        type: 'POST',
        dataType: 'json'
    }).done(function(response){
        var municipio = '<option value="-1">--Seleccione un municipio--</option>';
        $.each(response, function(index, data){
            municipio += '<option value="' + data.idMunicipio + '">' + data.descripcion + '</option>';
        });
        $('#ddlMunicipio').html(municipio);
    });
}