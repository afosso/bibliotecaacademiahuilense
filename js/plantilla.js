function mostrarMensaje(tipo, mensaje){
    Swal.fire({
        type: tipo,
        title: 'Mensaje del sistema',
        text: mensaje
    });
}

function agregarDataTable(id, data, columns){

	$('#' + id).DataTable({
		'paging'      : true,
		'lengthChange': true,
		'searching'   : true,
		'ordering'    : true,
		'info'        : true,
		'autoWidth'   : false,
		'iDisplayLength': 15,
		"language": {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
			"sFirst":    "Primero",
			"sLast":     "Último",
			"sNext":     "Siguiente",
			"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		},
		data: data,
		columns: columns,
		"dom": "Bfrtip",
		"buttons": [
			{
				extend: 'pdfHtml5',
				titleAttr: "Descargar PDF",
				text: "<i class='fa fa-file-pdf' aria-hidden='true'></i>",
				exportOptions: {
					modifier: {
						page: 'all'
					}
				}
			},
			{
				extend: 'excelHtml5',
				titleAttr: "Descargar Excel",
				text: "<i class='fa fa-file-excel' aria-hidden='true'></i>",
				exportOptions: {
					modifier: {
						page: 'all'
					}
				}
			},
			{
				extend: 'csvHtml5',
				titleAttr: "Descargar CSV",
				text: "<i class='fa fa-file' aria-hidden='true'></i>",
				exportOptions: {
					modifier: {
						page: 'all'
					}
				}
			}
		]
	})
}